import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DatabaseService {
  final String? uid;
  DatabaseService({this.uid});
  CollectionReference users = FirebaseFirestore.instance.collection('Users');
  CollectionReference hosts = FirebaseFirestore.instance.collection('Hosts');
  CollectionReference groups = FirebaseFirestore.instance.collection('Groups');
  CollectionReference requests =
      FirebaseFirestore.instance.collection('Requests');
  CollectionReference supports =
      FirebaseFirestore.instance.collection('Supports');
  CollectionReference situations =
      FirebaseFirestore.instance.collection('Situations');

  Future<String> addHostAccount(
      {required String fullName,
      required String email,
      required String phoneNumber}) async {
    String uid = '';

    QuerySnapshot<Object?> results = await hosts
        .where('email', isEqualTo: email)
        .limit(1)
        .get()
        .catchError((error) {
      print(error.toString());
    });
    if (results.docs.isEmpty) {
      await hosts.add({
        'fullName': fullName,
        'phoneNumber': phoneNumber,
        'email': email,
        'password': '',
        'groupName': fullName,
        'profilePhotoURL': null,
        'bannerPhotoURL': null,
        'partyAssociated': null,
        'gender': null,
        'isActive': true,
        'dateOfBirth': null,
        'themeColorCode': 0xFF0CA404,
        'numUnits': 0,
        'supports': [],
        'users': [],
        'groups': [],
        'nextElectionDate': null,
        'timeCreated': DateTime.now(),
      }).then((value) async {
        print("Host successfully added to firestore");
        uid = value.id;
      }).catchError((error) {
        print("Failed to add host: $error");
      });
    } else {
      uid =
          'Email already used. Login if you have access to that account or use another email.';
    }
    return uid;
  }

  Future<bool> sendSupportRequest({
    required String phoneNumber,
    required String email,
    required String message,
    required String userType,
    required String uid,
  }) async {
    bool response = true;
    await requests.add({
      'phoneNumber': phoneNumber,
      'userType': userType,
      'uid': uid,
      'email': email,
      'message': message,
    }).catchError((error) {
      response = false;
    });
    return response;
  }

  Future<String> addUserAccount(
      {required String phoneNumber,
      required String authCode,
      String? fullName,
      required String hostAssociatedUID,
      bool shouldUpdateGroupInfo = false}) async {
    String uid = '';
    QuerySnapshot<Object?> results = await users
        .where('hostAssociated', isEqualTo: hostAssociatedUID)
        .where('phoneNumber', isEqualTo: phoneNumber)
        .limit(1)
        .get()
        .catchError((error) {
      print(error.toString());
    });
    if (results.docs.isEmpty) {
      await users.add({
        'fullName': fullName,
        'phoneNumber': phoneNumber,
        'shouldUpdateGroupInfo': shouldUpdateGroupInfo,
        'state': null,
        'gender': null,
        'dateOfBirth': null,
        'localGovernmentArea': null,
        'votersCardNumber': null,
        'bank': null,
        'accountNumber': null,
        'address': null,
        'password': authCode,
        'hostAssociated': hostAssociatedUID,
        'supports': [],
        'email': null,
        'profilePhotoURL': null,
        'isActive': true,
        'timeCreated': DateTime.now(),
      }).then((value) async {
        print("Host successfully added to firestore");
        uid = value.id;
        updateHostData({
          "users": FieldValue.arrayUnion([uid])
        }, hostUID: hostAssociatedUID);
      }).catchError((error) {
        print("Failed to add host: $error");
      });
    } else {
      DocumentSnapshot userSnapshot = results.docs[0];
      // if user already exists but it's a new group being registered to the user,
      // then just update the user's account to say that there's group data to be updated
      if (shouldUpdateGroupInfo) {
        uid = userSnapshot.id;
        await updateUserData(
          {'shouldUpdateGroupInfo': true},
          userID: uid,
        );
      }
    }
    return uid;
  }

  Future<String> addSupportsAccount(
      {required String fullName,
      String? profilePhotoURL,
      required String phoneNumber,
      required String? state,
      required DateTime dateOfBirth,
      required String? localGovernmentArea,
      required String votersCardNumber,
      required String address,
      required String? gender,
      String? authCode,
      required String email,
      required String occupation,
      required String hostAssociatedUID,
      required String ward,
      required String unit,
      required String employmentStatus,
      required String bank,
      required String accountNumber,
      String? userAssociatedUID,
      required String? educationalQualification,
      String? groupAssociated}) async {
    String uid = '';
    QuerySnapshot<Object?> results = await hosts
        .where('hostAssociated', isEqualTo: hostAssociatedUID)
        .where('votersCardNumber', isEqualTo: votersCardNumber)
        .limit(1)
        .get()
        .catchError((error) {
      print(error.toString());
    });
    if (results.docs.isEmpty) {
      await supports.add({
        'fullName': fullName,
        'phoneNumber': phoneNumber,
        'state': state,
        'dateOfBirth': dateOfBirth,
        'gender': gender,
        'occupation': occupation,
        'localGovernmentArea': localGovernmentArea,
        'votersCardNumber': votersCardNumber,
        'address': address,
        'ward': ward,
        'unit': unit,
        'password': authCode,
        'employmentStatus': employmentStatus,
        'bank': bank,
        'accountNumber': accountNumber,
        'hostAssociated': hostAssociatedUID,
        'userAssociated': userAssociatedUID,
        'groupAssociated': groupAssociated,
        'email': email,
        'profilePhotoURL': profilePhotoURL,
        'educationalQualification': educationalQualification,
        'timeCreated': DateTime.now(),
      }).then((value) async {
        print("Host successfully added to firestore");
        uid = value.id;
        updateHostData({
          "supports": FieldValue.arrayUnion([uid])
        }, hostUID: hostAssociatedUID);
        if (userAssociatedUID != null) {
          updateUserData({
            "supports": FieldValue.arrayUnion([uid])
          }, userID: userAssociatedUID);
        }
        if (groupAssociated != null) {
          updateGroupData({
            "supports": FieldValue.arrayUnion([uid])
          }, groupID: groupAssociated);
        }
      }).catchError((error) {
        print("Failed to add host: $error");
      });
    } else {
      uid =
          'A supporter with this voter card number has already been registered on our system.';
    }
    return uid;
  }

  Future<String> addGroup({
    required String groupName,
    required String groupLeader,
    required String phoneNumber,
    required String activationCode,
    required String email,
    required String leaderUID,
    required String hostAssociatedUID,
  }) async {
    String groupID = '';
    await groups.add({
      'groupName': groupName,
      'groupLeader': groupLeader,
      'phoneNumber': phoneNumber,
      'state': null,
      'localGovernmentArea': null,
      'hostAssociated': hostAssociatedUID,
      'userAssociated': leaderUID,
      'password': activationCode,
      'email': email,
      'profilePhotoURL': null,
      'candidate': null,
      'category': null,
      'senatorialCoordinators': [],
      'lgaCoordinators': [],
      'description': null,
      'supports': [],
      'executives': [],
      'timeCreated': DateTime.now(),
      'groupActivated': false
    }).then((value) async {
      print("Host successfully added to firestore");
      groupID = value.id;
      updateHostData({
        "groups": FieldValue.arrayUnion([groupID])
      }, hostUID: hostAssociatedUID);
    }).catchError((error) {
      print("Failed to add group: $error");
    });
    return groupID;
  }

  Future<bool> updateHostData(Map<String, Object?> data,
      {String? hostUID}) async {
    bool successful = true;
    await hosts.doc(hostUID ?? uid).update(data).catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future<bool> addSituation(
      {required String hostAssociated,
      required String userAssociated,
      String? description,
      List<String>? imageURLs,
      List<String>? videoURLs}) async {
    bool isSuccessful = false;
    await situations.add({
      'hostAssociated': hostAssociated,
      'userAssociated': userAssociated,
      'description': description,
      'imageURLs': imageURLs,
      'videoURLs': videoURLs,
      'time': DateTime.now()
    }).then((value) => isSuccessful = true);
    return isSuccessful;
  }

  Stream<QuerySnapshot> getUserSituations(
      {required String userAssociated, required String hostAssociated}) {
    return situations
        .where('hostAssociated', isEqualTo: hostAssociated)
        .where('userAssociated', isEqualTo: userAssociated)
        .snapshots();
  }

  Stream<QuerySnapshot> getHostSituations({required String hostAssociated}) {
    return situations
        .where('hostAssociated', isEqualTo: hostAssociated)
        .orderBy('time')
        .snapshots();
  }

  Future<bool> checkIfHostIsActive(String hostUID) async {
    bool isActive = true;
    try {
      isActive = await getSpecificHostData('isActive', hostUID: hostUID);
    } catch (e) {
      print(e.toString());
      updateHostData({'isActive': true}, hostUID: hostUID);
    }
    return isActive;
  }

  Future<bool> updateUserData(Map<String, Object?> data,
      {required String userID}) async {
    bool successful = true;
    await users.doc(userID).update(data).catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future<bool> updateSupportData(Map<String, Object?> data,
      {required String supporterID}) async {
    bool successful = true;
    await supports.doc(supporterID).update(data).catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future<bool> updateGroupData(Map<String, Object?> data,
      {required String groupID}) async {
    bool successful = true;
    await groups.doc(groupID).update(data).catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future<bool> suspendUser({required String uid}) async {
    bool successful = true;
    await users.doc(uid).update({'isActive': false}).catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future<bool> deleteUser(
      {required String hostUID, required String uid}) async {
    bool successful = true;
    List supportsRegistered =
        await getSpecificUserData('supports', userID: uid);
    await users
        .doc(uid)
        .delete()
        .then((value) => updateHostData({
              "users": FieldValue.arrayRemove([uid]),
              "supports": FieldValue.arrayRemove(supportsRegistered),
            }, hostUID: hostUID))
        .catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future<bool> deleteSupporter(
      {required String hostUID,
      required String userID,
      required bool isUserRegistered,
      required String uid}) async {
    bool successful = true;
    await supports.doc(uid).delete().then((value) {
      updateHostData({
        "supports": FieldValue.arrayRemove([uid])
      }, hostUID: hostUID);
      if (isUserRegistered) {
        updateUserData({
          "supports": FieldValue.arrayRemove([uid])
        }, userID: userID);
      } else {
        updateGroupData({
          "supports": FieldValue.arrayRemove([uid])
        }, groupID: userID);
      }
    }).catchError((error) {
      print(error.toString());
      successful = false;
    });
    return successful;
  }

  Future getSpecificGroupData(String key, {required String groupID}) async {
    dynamic data;
    await groups.doc(groupID).get().then((value) async {
      data = await value[key];
    });
    return data;
  }

  Future getSpecificUserData(String key, {required String userID}) async {
    dynamic data;
    await users.doc(userID).get().then((value) async {
      data = await value[key];
    });
    return data;
  }

  Future getSpecificSupportData(String key, {required String supportID}) async {
    dynamic data;
    await supports.doc(supportID).get().then((value) async {
      data = await value[key];
    });
    return data;
  }

  Future getSpecificHostData(String key, {required String hostUID}) async {
    dynamic data;
    await hosts.doc(hostUID).get().then((value) async {
      data = await value[key];
    });
    return data;
  }

  Stream<DocumentSnapshot> hostSnapshots({required String hostUID}) {
    return hosts.doc(hostUID).snapshots();
  }

  Stream<DocumentSnapshot> userSnapshots({required String userID}) {
    return users.doc(userID).snapshots();
  }

  Stream<DocumentSnapshot> groupSnapshots({required String groupID}) {
    return groups.doc(groupID).snapshots();
  }

  Stream<DocumentSnapshot> supportSnapshots({required String supportID}) {
    return supports.doc(supportID).snapshots();
  }

  Future<DocumentSnapshot> getHostData({required String hostUID}) {
    return hosts.doc(hostUID).get();
  }

  Future<DocumentSnapshot> getUserData({required String userID}) {
    return users.doc(userID).get();
  }

  Future<DocumentSnapshot> getSupporterData({required String supporterUID}) {
    return supports.doc(supporterUID).get();
  }

  Future<DocumentSnapshot> getGroupData({required String groupID}) {
    return groups.doc(groupID).get();
  }

  Future<List<String>> getSupportsPhoneNumbers(
      {required List supportsList}) async {
    List<String> phoneNumberList = [];
    for (var element in supportsList) {
      String phoneNumber =
          await getSpecificSupportData('phoneNumber', supportID: element);
      if (phoneNumber.isNotEmpty) {
        phoneNumberList.add(phoneNumber);
      }
    }
    return phoneNumberList;
  }

  Stream<QuerySnapshot> hostUsersSnapshots({required String userID}) {
    return users.where('hostAssociated', isEqualTo: userID).snapshots();
  }

  Stream<QuerySnapshot> hostSupportsSnapshots(
      {required String hostUID,
      String? userID,
      bool showAll = false,
      String fieldToOrderBy = 'fullName'}) {
    if (showAll) {
      return supports
          .where('hostAssociated', isEqualTo: hostUID)
          .where('userAssociated', isNotEqualTo: null)
          .orderBy(fieldToOrderBy)
          .snapshots();
    } else {
      return supports
          .where('hostAssociated', isEqualTo: hostUID)
          .where('userAssociated', isEqualTo: userID)
          .orderBy(fieldToOrderBy)
          .snapshots();
    }
  }

  Stream<QuerySnapshot> groupSupportsSnapshots(
      {String? groupID, String? hostUID, bool showAll = false}) {
    if (showAll) {
      return supports
          .where('userAssociated', isNull: true)
          .where('hostAssociated', isEqualTo: hostUID)
          .orderBy('fullName')
          .snapshots();
    } else {
      return supports.where('groupAssociated', isEqualTo: groupID).snapshots();
    }
  }

  Stream<QuerySnapshot> hostGroupsSnapshots({required String hostUID}) {
    return groups.where('hostAssociated', isEqualTo: hostUID).snapshots();
  }

  Future<Map<String, String>> getUsersToNameMap(
      {required List userList}) async {
    // List<String> userIDs = await getSpecificHostData('users', hostUID: hostUID);
    // print(userIDs);
    Map<String, String> usersToName = {};
    for (String element in userList) {
      DocumentSnapshot snapshot = await getUserData(userID: element);
      if (snapshot.exists) {
        usersToName[snapshot.get('fullName') ??
            '${snapshot.get('phoneNumber') as String} (No name)' ??
            'No name/phone number'] = element;
      }
    }
    return usersToName;
  }

  Future<Map<String, String>> getUsersToNumberMap(
      {required List userList}) async {
    // List<String> userIDs = await getSpecificHostData('users', hostUID: hostUID);
    // print(userIDs);
    Map<String, String> usersToNumber = {};
    for (String element in userList) {
      DocumentSnapshot snapshot = await getUserData(userID: element);
      usersToNumber[snapshot.get('fullName') ??
          '${snapshot.get('phoneNumber') as String} (No name)' ??
          'No name/phone number'] = snapshot.get('phoneNumber');
    }
    return usersToNumber;
  }

  Future<DocumentSnapshot<Object?>> userWithHighestSupportRegistrations(
      {required List userList}) async {
    List<DocumentSnapshot> userSnapshots = [];
    for (String element in userList) {
      DocumentSnapshot snapshot = await getUserData(userID: element);
      userSnapshots.add(snapshot);
    }
    userSnapshots.sort(
        (a, b) => b.get('supports').length.compareTo(a.get('supports').length));
    DocumentSnapshot snapshot = userSnapshots[0];
    return snapshot;
  }

  Future<DocumentSnapshot<Object?>> groupWithHighestSupportRegistrations(
      {required List groupsList}) async {
    List<DocumentSnapshot> groupSnapshots = [];
    for (String element in groupsList) {
      DocumentSnapshot snapshot = await getGroupData(groupID: element);
      groupSnapshots.add(snapshot);
    }
    groupSnapshots.sort(
        (a, b) => b.get('supports').length.compareTo(a.get('supports').length));
    DocumentSnapshot snapshot = groupSnapshots[0];
    return snapshot;
  }

  saveDeviceToken({required String uid}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? deviceToken = prefs.getString('${uid}Token');
    if (deviceToken == null || deviceToken == '') {
      String userType = prefs.getString('userType')!;
      CollectionReference collectionReference = users;
      if (userType == 'user') {
        collectionReference = users;
      }
      if (userType == 'host') {
        collectionReference = hosts;
      }
      if (userType == 'supports') {
        collectionReference = supports;
      }
      String? fcmToken = await FirebaseMessaging.instance.getToken();
      if (fcmToken != null) {
        DocumentReference tokenRef =
            collectionReference.doc(uid).collection('tokens').doc(fcmToken);

        await tokenRef.set({
          'token': fcmToken,
          'createdAt': DateTime.now(),
          'platform': Platform.operatingSystem
        });
        await prefs.setString('${uid}Token', fcmToken);
        // resubscribeToTopics();
      }
    }
  }

  checkAndRemovePastTokens() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userFirestoreId = prefs.getString('uid');
    if (userFirestoreId != null) {
      String? deviceToken = prefs.getString('${userFirestoreId}Token');
      if (deviceToken != null && deviceToken != '') {
        String userType = prefs.getString('userType')!;
        CollectionReference collectionReference = users;
        if (userType == 'user') {
          collectionReference = users;
        }
        if (userType == 'host') {
          collectionReference = hosts;
        }
        if (userType == 'supports') {
          collectionReference = supports;
        }
        DocumentReference tokenRef = collectionReference
            .doc(userFirestoreId)
            .collection('tokens')
            .doc(deviceToken);
        tokenRef.delete();
        await prefs.remove('${userFirestoreId}Token');
        await prefs.setString('uid', '');
      }
      // unsubscribeFromTopics(userFirestoreId);
    }
  }
}
