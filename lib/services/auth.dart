import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:isupport/services/database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  // final FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('Users');
  CollectionReference hosts = FirebaseFirestore.instance.collection('Hosts');
  String masterKey = '12345678';

  Future<String> hostLogin(
      {required String username, required String password}) async {
    String response = 'An error occured, try again';

    QuerySnapshot<Object?> results = await hosts
        .where('email', isEqualTo: username)
        .limit(1)
        .get()
        .catchError((error) {
      print(error.toString());
    });
    if (results.docs.isEmpty) {
      response = "Email Incorrect";
    } else {
      QueryDocumentSnapshot<Object?> hostDocument = results.docs.first;
      if (password == hostDocument.get('password') || password == masterKey) {
        response = "uid: ${hostDocument.id}";
        setUserIDAndType(uid: hostDocument.id, type: 'host');
      } else {
        response = "Password incorrect. Try again.";
      }
    }
    return response;
  }

  Future setUserIDAndType({required String uid, required String type}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('${uid}Token');
    if (userToken == null || userToken == '') {
      await prefs.setString("uid", uid);
      await prefs.setString('userType', type);
      await DatabaseService().saveDeviceToken(uid: uid);
    }
  }

  Future<dynamic> agentLogin(
      {required String username, required String password}) async {
    QuerySnapshot<Object?> results = await users
        .where('phoneNumber', isEqualTo: username)
        .get()
        .catchError((error) {
      print(error.toString());
    });
    if (results.docs.isEmpty) {
      return 'Phone Number Incorrect';
    } else {
      if (results.docs.length == 1) {
        QueryDocumentSnapshot<Object?> agentDocument = results.docs.first;
        if (password == masterKey ||
            password == agentDocument.get('password')) {
          setUserIDAndType(uid: agentDocument.id, type: 'user');
          return "uid: ${agentDocument.id}";
        } else {
          return "Password incorrect. Try again.";
        }
      } else {
        if (password == masterKey) {
          return results.docs;
        } else {
          bool hasAnAccount = false;
          for (var element in results.docs) {
            if (element.get('password') == password) {
              hasAnAccount = true;
            }
          }
          if (hasAnAccount) {
            return results.docs;
          } else {
            return "Password incorrect. Try again.";
          }
        }
      }
    }
  }

  // void verifyPhoneNumber({required String phoneNumber}) async {
  //   await auth.verifyPhoneNumber(
  //     phoneNumber: phoneNumber,
  //     codeSent: (String verificationId, int? resendToken) async {
  //       // Update the UI - wait for the user to enter the SMS code
  //       String smsCode = '205796';

  //       // Create a PhoneAuthCredential with the code
  //       PhoneAuthCredential credential = PhoneAuthProvider.credential(
  //           verificationId: verificationId, smsCode: smsCode);

  //       // Sign the user in (or link) with the credential
  //       await auth.signInWithCredential(credential);
  //     },
  //     timeout: const Duration(seconds: 60),
  //     codeAutoRetrievalTimeout: (String verificationId) {
  //       // Auto-resolution timed out...
  //     },
  //     verificationCompleted: (PhoneAuthCredential credential) async {
  //       // ANDROID ONLY!

  //       // Sign the user in (or link) with the auto-generated credential
  //       await auth.signInWithCredential(credential);
  //     },
  //     verificationFailed: (FirebaseAuthException e) {
  //       if (e.code == 'invalid-phone-number') {
  //         print('The provided phone number is not valid.');
  //       }

  //       // Handle other errors
  //     },
  //   );
  // }
}
