import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:isupport/utils/wards.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'components.dart';
import 'constants.dart';

String integars = '1234567890';
String alphaNumerals =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
Random rnd = Random();

String getRandomString() => String.fromCharCodes(Iterable.generate(
    11, (_) => integars.codeUnitAt(rnd.nextInt(integars.length))));

String getRandomAlphaNumerals() => String.fromCharCodes(Iterable.generate(
    20, (_) => alphaNumerals.codeUnitAt(rnd.nextInt(alphaNumerals.length))));

bool isValidEmail(String val) {
  final emailRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  return emailRegExp.hasMatch(val);
}

String numberListToLinkedString({required List<String> phoneNumberList}) {
  return phoneNumberList.join('&');
}

Future<bool> sendGroupLeaderSMS({
  required bool isNewUser,
  required String leaderName,
  required String hostName,
  required String groupName,
  required String receiverNumber,
  required String authCode,
}) async {
  String callToAction = '';
  if (isNewUser) {
    callToAction =
        "Your iSupport username is: $receiverNumber Password: $authCode";
  } else {
    callToAction = "Login to the iSupport app to activate group.";
  }
  String message =
      "Hey, $leaderName. You have been invited by $hostName to head the $groupName group. $callToAction";
  bool sendMessage = await sendSMS(
    receiverNumbers: receiverNumber,
    sender: 'iSupport',
    message: message,
  );
  return sendMessage;
}

Future<bool> sendAgentSMS({
  required String hostName,
  required String receiverNumber,
  required String authCode,
}) async {
  String message =
      "$hostName has added you as a campaign agent. Your iSupport username is: $receiverNumber Password: $authCode";
  bool sendMessage = await sendSMS(
    receiverNumbers: receiverNumber,
    sender: 'iSupport',
    message: message,
  );
  return sendMessage;
}

Future<bool> sendSMS({
  required String receiverNumbers,
  required String sender,
  required String message,
}) async {
  String username = "victorpee40@gmail.com";
  String password = "Darren123\$#@";
  String url = "http://portal.nigeriabulksms.com/api/";
  var uri = Uri.parse(url);
  var response = await http.post(uri, body: {
    'username': username,
    'password': password,
    'message': message,
    'sender': sender,
    'mobiles': receiverNumbers
  });
  print(url);
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
  return response.body.contains("OK");
}

String formattedNumber(int num) {
  return NumberFormat.compact().format(num);
}

Future<bool> checkInternetConnection() {
  /// We use a mix of IPV4 and IPV6 here in case some networks only accept one of the types.
  /// Only tested with an IPV4 only network so far (I don't have access to an IPV6 network).
  final List<InternetAddress> dnss = [
    InternetAddress('8.8.8.8', type: InternetAddressType.IPv4), // Google
    InternetAddress('2001:4860:4860::8888',
        type: InternetAddressType.IPv6), // Google
    InternetAddress('1.1.1.1', type: InternetAddressType.IPv4), // CloudFlare
    InternetAddress('2606:4700:4700::1111',
        type: InternetAddressType.IPv6), // CloudFlare
    InternetAddress('208.67.222.222',
        type: InternetAddressType.IPv4), // OpenDNS
    InternetAddress('2620:0:ccc::2', type: InternetAddressType.IPv6), // OpenDNS
    InternetAddress('180.76.76.76', type: InternetAddressType.IPv4), // Baidu
    InternetAddress('2400:da00::6666', type: InternetAddressType.IPv6), // Baidu
  ];

  final Completer<bool> completer = Completer<bool>();

  int callsReturned = 0;
  void onCallReturned(bool isAlive) {
    if (completer.isCompleted) return;

    if (isAlive) {
      completer.complete(true);
    } else {
      callsReturned++;
      if (callsReturned >= dnss.length) {
        completer.complete(false);
      }
    }
  }

  dnss.forEach((dns) => _pingDns(dns).then(onCallReturned));

  return completer.future;
}

Future<bool> _pingDns(InternetAddress dnsAddress) async {
  const int dnsPort = 53;
  const Duration timeout = Duration(seconds: 3);

  Socket? socket;
  try {
    socket = await Socket.connect(dnsAddress, dnsPort, timeout: timeout);
    socket.destroy();
    return true;
  } on SocketException {
    socket?.destroy();
  }
  return false;
}

Future<String> saveFileToAppStorage(File file) async {
  final directory = await getApplicationDocumentsDirectory();
  String filename = file.path.split('/').last;
  File newFile = File('${directory.path}/$filename');
  var bytes = file.readAsBytesSync();
  newFile.writeAsBytesSync(bytes);
  return newFile.path;
}

void bulkSMSConfirmationDialog(BuildContext context,
    {required List<String> phoneNumbers,
    required String senderName,
    required String message}) {
  int numNumbers = phoneNumbers.length;
  customShowDialog(
      context,
      Padding(
        padding:
            EdgeInsets.only(left: 20.w, right: 20.w, top: 30.h, bottom: 20.h),
        child: Text(
          'You are about to send a bulk message to a total of ' +
              numNumbers.toString() +
              ' numbers.',
          textAlign: TextAlign.center,
        ),
      ),
      actions: [
        InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Padding(
            padding: EdgeInsets.only(bottom: 20.h, right: 30.w),
            child: Text(
              'Cancel',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        InkWell(
          onTap: () async {
            Navigator.of(context).pop();
            customScaffoldMessage('Dispatching message...', context,
                duration: const Duration(hours: 1));
            message = "Message from $senderName: \n" + message;
            bool messagesSent = await sendSMS(
              receiverNumbers:
                  numberListToLinkedString(phoneNumberList: phoneNumbers),
              sender: 'iSupport',
              message: message,
            );
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            if (messagesSent) {
              customScaffoldMessage(
                "Messages sent successfully!",
                context,
                duration: const Duration(seconds: 4),
              );
              Navigator.of(context).pop();
            } else {
              customScaffoldMessage(
                "Temporary error sending message. Please try again later.",
                context,
                duration: const Duration(seconds: 3),
              );
            }
          },
          child: Padding(
            padding: EdgeInsets.only(bottom: 20.h, right: 30.w),
            child: Text(
              'Proceed',
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
        )
      ]);
}

void printPage(
  Widget body, {
  required String documentName,
  required BuildContext context,
  Duration delay = const Duration(seconds: 1),
}) async {
  customScaffoldMessage('Preparing document...', context,
      duration: const Duration(hours: 1));
  final controller = ScreenshotController();
  final bytes = await controller.captureFromWidget(
    Material(
      color: Colors.white,
      child: body,
    ),
    delay: delay,
  );
  final doc = pw.Document();
  doc.addPage(
    pw.Page(
      build: (pw.Context context) {
        return pw.Center(
          child: pw.Container(
            decoration: pw.BoxDecoration(
              image: pw.DecorationImage(
                image: pw.MemoryImage(bytes),
              ),
            ),
          ),
        ); // Center
      },
    ),
  ); // Page// Page
  final output = await getTemporaryDirectory();
  final file = File('${output.path}/$documentName');
  await file.writeAsBytes(await doc.save());
  print(file.path);
  // await Printing.sharePdf(bytes: await doc.save(), filename: documentName);
  await Printing.layoutPdf(onLayout: (_) => file.readAsBytesSync())
      .then((value) => ScaffoldMessenger.of(context).hideCurrentSnackBar());
  // PdfPreview(
  //   build: (format) => file.readAsBytes(),
  // );
}

class CustomDropdownWidget extends StatefulWidget {
  const CustomDropdownWidget({
    Key? key,
    required this.initialDropdownValue,
    required this.hint,
    required this.items,
    required this.onChanged,
  }) : super(key: key);
  final void Function(String? newValue) onChanged;
  final String hint;
  final String? initialDropdownValue;
  final List<String> items;

  @override
  State<CustomDropdownWidget> createState() => _CustomDropdownWidgetState();
}

class _CustomDropdownWidgetState extends State<CustomDropdownWidget> {
  String? dropdownValue;
  bool isLoaded = false;
  @override
  void initState() {
    if (!isLoaded) {
      dropdownValue = widget.initialDropdownValue;
      isLoaded = true;
    }
    super.initState();
  }

  Widget customDropdownButton() {
    return Center(
      child: roundedContainer(
        buttonColor: Colors.black54,
        radius: 0,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.h),
          width: double.infinity,
          child: DropdownButton<String>(
            isDense: true,
            // remove underline
            underline: Container(),

            // Initial Value
            value: dropdownValue,

            hint: Container(
              width: 320.w,
              padding: EdgeInsets.only(left: 15.w),
              child: Text(widget.hint,
                  style: TextStyle(fontSize: 18.sp, color: Colors.black45)),
            ),

            // Down Arrow Icon
            icon: Icon(
              CupertinoIcons.chevron_down,
              size: 25.w,
            ),

            // Array list of items
            items: widget.items.map((String item) {
              return DropdownMenuItem(
                value: item,
                child: Container(
                  width: 320.w,
                  padding: EdgeInsets.only(left: 15.w),
                  child: Text(item,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black)),
                ),
              );
            }).toList(),
            // After selecting the desired option,it will
            // change button value to selected value
            onChanged: (newValue) {
              setState(() {
                dropdownValue = newValue!;
              });
              widget.onChanged(newValue);
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return customDropdownButton();
  }
}

Future<String> uploadFileToFirebaseStorage(File file) async {
  //Set File Name
  String fileName = file.path.split('/').last;

  //Create Reference
  Reference reference =
      FirebaseStorage.instance.ref().child('uploads').child(fileName);

  //Now We have to check the status of UploadTask
  UploadTask uploadTask = reference.putFile(file);

  late String url;
  await uploadTask.whenComplete(() async {
    url = await uploadTask.snapshot.ref.getDownloadURL();
  });

  return url;
}

Future<CroppedFile?> getImageCropped(File image,
    {CropAspectRatio? aspectRatio}) async {
  int maxImageHeight = 250;
  CroppedFile? croppedFile = await ImageCropper().cropImage(
    maxHeight: maxImageHeight,
    maxWidth: maxImageHeight,
    compressQuality: 100,
    sourcePath: image.path,
    aspectRatio: aspectRatio ?? const CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
    uiSettings: [
      AndroidUiSettings(
          toolbarTitle: 'Crop Image',
          toolbarColor: primaryColor,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original),
      IOSUiSettings(
        aspectRatioLockEnabled: true,
        minimumAspectRatio: 1.0,
      )
    ],
  );
  return croppedFile;
}
