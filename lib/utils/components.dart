import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:isupport/pages/main/chat/chat.dart';
import 'package:isupport/pages/main/hosts/manage_agents/send_activation_code.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:uuid/uuid.dart';
import 'package:isupport/utils/constants.dart';

import '../services/database.dart';

Widget customScaffold(
    {required Widget child,
    PreferredSizeWidget? appBar,
    Drawer? drawer,
    Color backgroundColor = Colors.white}) {
  return Scaffold(
      drawer: drawer,
      appBar: appBar,
      backgroundColor: backgroundColor,
      body: Center(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w), child: child)));
}

void customShowDialog(BuildContext context, Widget content,
    {List<Widget>? actions, bool isDismissible = true, Function()? then}) {
  showDialog(
    barrierDismissible: isDismissible,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        actions: actions,
        insetPadding: const EdgeInsets.all(2),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        contentPadding: EdgeInsets.zero,
        content: content,
      );
    },
  ).then((value) => then);
}

Widget roundedContainer(
    {required Widget child,
    Color? buttonColor,
    double? radius,
    Color? backgroundColor}) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(radius ?? 40.w),
      border: Border.all(color: buttonColor ?? primaryColor, width: 1.h),
      color: backgroundColor,
    ),
    child: child,
  );
}

Widget filledInContainer(
    {required Widget child, Color? buttonColor, double? radius}) {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius ?? 40.w),
        color: buttonColor ?? primaryColor),
    child: child,
  );
}

Widget filledInButton(
    {required String text,
    void Function()? onTap,
    double? padding,
    double? fontSize,
    double? radius,
    Color? buttonColor}) {
  return InkWell(
    onTap: onTap,
    child: filledInContainer(
      radius: radius ?? 40.w,
      buttonColor: buttonColor ?? primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(padding ?? 15.w),
            child: Text(
              text,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: fontSize ?? 31.sp),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget borderedInButton(
    {required String text, void Function()? onTap, Color? buttonColor}) {
  return InkWell(
    onTap: onTap,
    child: roundedContainer(
      buttonColor: buttonColor ?? primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(15.w),
            child: Text(
              text,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 31.sp),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget textFieldPadding(
    {required Widget child,
    double? radius,
    EdgeInsetsGeometry? padding,
    Color buttonColor = Colors.black}) {
  return roundedContainer(
    radius: radius ?? 40.w,
    buttonColor: buttonColor,
    child: Center(
      child: Padding(
          padding:
              padding ?? EdgeInsets.only(left: 25.w, top: 8.h, bottom: 8.h),
          child: child),
    ),
  );
}

InputDecoration customInputDecoration(
    {String hintText = '', Widget? suffixIcon, bool isDense = false}) {
  return InputDecoration(
      isDense: isDense,
      contentPadding: isDense ? EdgeInsets.zero : null,
      suffix: suffixIcon,
      border: InputBorder.none,
      hintText: hintText,
      hintStyle: TextStyle(color: Colors.black45, fontSize: 18.sp));
}

PreferredSize roundedAppbar(BuildContext context, {String? uid, bool? isHost}) {
  // double appBarHeight = (MediaQuery.of(context).size.height * 0.05) +
  //     MediaQuery.of(context).padding.top;
  return PreferredSize(
    preferredSize: Size(MediaQuery.of(context).size.width, 100.0),
    child: Container(
      // height: appBarHeight,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50.w),
          bottomRight: Radius.circular(50.w),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 40.0, bottom: 20),
        child: Stack(children: [
          Builder(builder: (context) {
            return Positioned(
              top: 0,
              left: (MediaQuery.of(context).size.width * 0.08).w,
              bottom: 0,
              child: InkWell(
                child: InkWell(
                    onTap: () => Scaffold.of(context).openDrawer(),
                    child: Icon(Icons.menu, color: Colors.white, size: 27.w)),
              ),
            );
          }),
          Align(
            alignment: Alignment.center,
            child: uid == null
                ? Text(
                    "",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24.sp,
                        fontWeight: FontWeight.bold),
                  )
                : SizedBox(
                    width: 290.w,
                    child: FutureBuilder(
                        future: isHost!
                            ? DatabaseService()
                                .getSpecificHostData('groupName', hostUID: uid)
                            : DatabaseService().getSpecificUserData(
                                'hostAssociated',
                                userID: uid),
                        builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) {
                          if (!snapshot.hasData) {
                            return Container();
                          }
                          return isHost
                              ? Text(
                                  snapshot.data ?? '',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 24.sp,
                                      fontWeight: FontWeight.bold),
                                )
                              : FutureBuilder(
                                  future: DatabaseService().getSpecificHostData(
                                      'groupName',
                                      hostUID: snapshot.data),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<dynamic> snapshot) {
                                    if (!snapshot.hasData) {
                                      return Container();
                                    }
                                    return Text(
                                      snapshot.data ?? '',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 24.sp,
                                          fontWeight: FontWeight.bold),
                                    );
                                  });
                        }),
                  ),
          ),
        ]),
      ),
    ),
  );
}

AppBar customAppbar(context,
    {String title = '', Color? backgroundColor, List<Widget>? actions}) {
  bool isWhiteBackground = backgroundColor == Colors.white;
  return AppBar(
    elevation: 1.5,
    leading: InkWell(
        onTap: () => Navigator.of(context).pop(),
        child: Icon(
          Icons.arrow_back_ios_new,
          color: isWhiteBackground ? Colors.black54 : Colors.white,
        )),
    backgroundColor: backgroundColor ?? primaryColor,
    title: Text(
      title,
      style:
          TextStyle(color: isWhiteBackground ? Colors.grey[600] : Colors.white),
    ),
    actions: actions,
  );
}

void customScaffoldMessage(String message, BuildContext context,
    {Duration duration = const Duration(milliseconds: 1500),
    bool devoidScaffold = false,
    Function()? action}) {
  SnackBar snackBar = SnackBar(
      backgroundColor: primaryColor,
      content: Text(
        message,
        textAlign: TextAlign.center,
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      duration: duration,
      action: action != null
          ? SnackBarAction(
              label: "See All",
              textColor: Colors.white,
              onPressed: () => action)
          : null);
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void removeCurrentSnackbar(BuildContext context) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
}

class InvitationWidget extends StatefulWidget {
  const InvitationWidget(
      {Key? key,
      required this.hostName,
      this.isFirstUser = false,
      this.withScaffold = false})
      : super(key: key);
  final String hostName;
  final bool isFirstUser;
  final bool withScaffold;

  @override
  State<InvitationWidget> createState() => _InvitationWidgetState();
}

class _InvitationWidgetState extends State<InvitationWidget> {
  String activationCode = '';

  void generateRandomCode() {
    setState(() {
      activationCode = getRandomString();
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        widget.isFirstUser
            ? Column(
                children: [
                  Text(
                    "You do not have any user yet, invite users by first generating an activation code",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20.sp, color: Colors.black54),
                  ),
                  SizedBox(height: 30.h),
                ],
              )
            : Column(
                children: [
                  Text(
                    "Invite users by first generating an activation code",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20.sp, color: Colors.black54),
                  ),
                  SizedBox(height: 50.h),
                ],
              ),
        roundedContainer(
          radius: 20.w,
          child: Padding(
            padding: EdgeInsets.all(15.w),
            child: Icon(Icons.person, size: 80.w, color: Colors.black54),
          ),
        ),
        SizedBox(height: 30.h),
        Text(
          "Generate User Activation Code",
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 15.h),
        filledInButton(
            text: 'Generate',
            padding: 10.w,
            fontSize: 22.sp,
            onTap: () => generateRandomCode()),
        SizedBox(height: 20.h),
        roundedContainer(
          buttonColor: Colors.grey,
          child: SizedBox(
            height: 45.h,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    activationCode,
                    style: TextStyle(fontSize: 18.sp, color: Colors.grey),
                  ),
                ),
                if (activationCode.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: InkWell(
                      onTap: () => setState(() {
                        activationCode = '';
                      }),
                      child: const Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.cancel),
                      ),
                    ),
                  )
              ],
            ),
          ),
        ),
        SizedBox(height: 20.h),
        filledInButton(
            text: 'Send Activation Code',
            padding: 10.w,
            fontSize: 22.sp,
            buttonColor: activationCode.isEmpty ? Colors.grey : primaryColor,
            onTap: () {
              if (activationCode.isEmpty) {
                customScaffoldMessage(
                    "You need to generate an activation code first", context);
              } else {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (BuildContext context) => SendActivationCodePage(
                      hostName: widget.hostName,
                      activationCode: activationCode,
                    ),
                  ),
                );
              }
            }),
      ],
    );
    return widget.withScaffold
        ? customScaffold(
            child: body, appBar: customAppbar(context, title: 'User'))
        : Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: body,
          );
  }
}

class ChatInputWidget extends StatefulWidget {
  const ChatInputWidget(
      {Key? key,
      required this.id,
      required this.peerId,
      required this.roomId,
      required this.senderName,
      required this.senderIsHost,
      required this.focusNode,
      this.isDropbox = false,
      this.isGroup = false,
      this.listScrollController})
      : super(key: key);
  final String id;
  final String peerId;
  final String roomId;
  final String senderName;
  final bool senderIsHost;
  final bool isDropbox;
  final bool isGroup;
  final FocusNode focusNode;
  final ScrollController? listScrollController;

  @override
  State<ChatInputWidget> createState() => _ChatInputWidgetState();
}

class _ChatInputWidgetState extends State<ChatInputWidget> {
  final TextEditingController textEditingController = TextEditingController();
  List<File> filesPicked = <File>[];

  bool uploadingImage = false;

  void onSendMessage({String content = '', String msgPhoto = ''}) {
    // type: 0 = text, 1 = image, 2 = sticker
    if (content.trim().isEmpty && msgPhoto.isEmpty) {
      Fluttertoast.showToast(
          msg: 'Nothing to send',
          backgroundColor: Colors.black,
          textColor: Colors.red);
    } else {
      textEditingController.clear();
      DateTime now = DateTime.now();
      String messageId = now.millisecondsSinceEpoch.toString();
      String timeString = now.toString();
      String collectionName = !widget.isDropbox
          ? 'Rooms'
          : widget.isGroup
              ? 'GroupDropboxes'
              : 'Dropboxes';

      DocumentReference docRef = FirebaseFirestore.instance
          .collection(collectionName)
          .doc(widget.roomId);
      docRef.get().then((value) {
        if (!value.exists) {
          docRef.set({
            'roomID': widget.roomId,
            'members': [widget.id, widget.peerId],
            'hostUID': widget.senderIsHost ? widget.id : widget.peerId,
            'userID': widget.senderIsHost ? widget.peerId : widget.id,
            'timeInitiated': DateTime.now()
          });
        }
      });

      var collectionReference = docRef.collection("messages");

      collectionReference.add(
        {
          'msgPhoto': msgPhoto,
          'read': "true",
          'sender_id': widget.id,
          'receiver_id': widget.peerId,
          'sender_name': widget.senderName,
          'text': content,
          'timestamp': now
        },
      ).then(
        (value) => FirebaseFirestore.instance
            .collection(collectionName)
            .doc(widget.roomId)
            .update({
          "lastMessageTime": now,
          "lastMessage": content,
          "lastMessageHasImage": msgPhoto.isNotEmpty,
          "lastMessageSender": widget.id,
          "${widget.peerId}_unseen": FieldValue.increment(1)
        }),
      );

      if (widget.listScrollController != null) {
        widget.listScrollController!.animateTo(0.0,
            duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
      } else {
        Navigator.of(context).pop();
      }
    }
  }

  Future<List<File>> getFile({bool allowMultiple = false}) async {
    List<File> imagesAssociated = <File>[];
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: allowMultiple,
        type: FileType.custom,
        allowedExtensions: ['jpg', 'jpeg', 'png', 'svg']);

    if (result != null) {
      imagesAssociated = result.paths.map((path) => File(path!)).toList();
      print(imagesAssociated);
    }
    return imagesAssociated;
  }

  Future<String> uploadFileToStorage(File image) async {
    //Set File Name
    String fileName = image.path.split('/').last;

    //Create Reference
    Reference reference =
        FirebaseStorage.instance.ref().child('images').child(fileName);

    //Now We have to check the status of UploadTask
    UploadTask uploadTask = reference.putFile(image);

    late String url;
    await uploadTask.whenComplete(() async {
      url = await uploadTask.snapshot.ref.getDownloadURL();
    });

    return url;
  }

  Widget inputWidget() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          if (filesPicked.isNotEmpty)
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.w),
                    topRight: Radius.circular(15.w)),
                color: primaryColor.withOpacity(0.8),
              ),
              padding: EdgeInsets.all(8.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (!uploadingImage)
                    Image.file(filesPicked.first, height: 200.h),
                  if (uploadingImage)
                    Text("Uploading image   ",
                        style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                  if (uploadingImage)
                    SizedBox(
                        height: 15.h,
                        width: 15.w,
                        child: const CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 3,
                        )),
                ],
              ),
            ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(40.w)),
            padding: EdgeInsets.only(bottom: 10.h),
            child: roundedContainer(
              buttonColor: Colors.black54,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5.h),
                child: Row(
                  children: <Widget>[
                    // Button send image
                    IconButton(
                      icon: Icon(Icons.attach_file, color: primaryColor),
                      onPressed: () async {
                        filesPicked = await getFile();
                        setState(() {});
                      },
                      color: primaryColor,
                    ),

                    // Edit text
                    Flexible(
                      child: TextField(
                        onSubmitted: (value) {
                          onSendMessage(content: textEditingController.text);
                        },
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        style: TextStyle(fontSize: 16.0.sp),
                        controller: textEditingController,
                        decoration: const InputDecoration.collapsed(
                          hintText: 'Text Message',
                          // hintStyle: TextStyle(color: Colors.grey),
                        ),
                        focusNode: widget.focusNode,
                      ),
                    ),

                    // Button send message
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 8.0.w),
                      child: InkWell(
                        child: Container(
                            decoration: const BoxDecoration(
                                color: Color(0xff48DF3B),
                                shape: BoxShape.circle),
                            child: const Padding(
                              padding: EdgeInsets.all(1),
                              child:
                                  Icon(Icons.arrow_upward, color: Colors.white),
                            )),
                        onTap: () async {
                          if (filesPicked.isEmpty) {
                            onSendMessage(content: textEditingController.text);
                          } else {
                            setState(() {
                              uploadingImage = true;
                            });
                            String msgPhotoLink =
                                await uploadFileToStorage(filesPicked.first);
                            onSendMessage(
                                content: textEditingController.text,
                                msgPhoto: msgPhotoLink);
                            setState(() {
                              filesPicked = [];
                              uploadingImage = false;
                            });
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return inputWidget();
  }
}
