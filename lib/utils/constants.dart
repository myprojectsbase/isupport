import 'package:flutter/material.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:shared_preferences/shared_preferences.dart';

Color primaryColor = const Color(0xFF0CA404);
Color lightGreen = const Color(0xff48DF3B);
const Color textFieldBlue = Color(0xff003FCA);

void setPrimaryColor({required int colorCode}) async {
  primaryColor = Color(colorCode);
  lightGreen = primaryColor;
  print("New Primary Color: $primaryColor");
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt('themeColorCode', colorCode);
}

const darkColor = Color(0xFF545454);
final List<String> groupCategories = [
  'National',
  'State',
  'Senatorial',
  'House of Representatives',
  'State House of Assembly',
  'Local Government Chairman',
];

List<int> themeColorCodes = [
  0xFF0CA404,
  0xFF003FCA,
  0xFFFA4848,
  0xFFF9B402,
  0xFF943DEA,
  0xFFFF7A1A,
  0xFF0F8984,
  0xFF727272,
];
