import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/registry/pick_color.dart';
import 'package:url_launcher/url_launcher.dart';
import '../registry/register.dart';
import '../main/hosts/home.dart';
import 'package:isupport/utils/components.dart';

class HostActivationPage extends StatefulWidget {
  const HostActivationPage({Key? key, required this.uid}) : super(key: key);
  final String uid;

  @override
  State<HostActivationPage> createState() => _HostActivationPageState();
}

class _HostActivationPageState extends State<HostActivationPage> {
  TextEditingController activationKeyController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return customScaffold(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Activate Account',
              style: TextStyle(fontSize: 43.sp),
            ),
            SizedBox(height: 100.h),
            Wrap(
              alignment: WrapAlignment.center,
              children: [
                Text("Go to ",
                    style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
                InkWell(
                  onTap: () =>
                      launchUrl(Uri.parse("https://isupport.com.ng/payment")),
                  child: Text("iSupport.com.ng",
                      style: TextStyle(fontSize: 22.sp, color: Colors.blue)),
                ),
                Text(" to get your activation key",
                    style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
              ],
            ),
            SizedBox(height: 50.h),
            textFieldPadding(
              child: TextFormField(
                controller: activationKeyController,
                textAlign: TextAlign.center,
                decoration: customInputDecoration(hintText: "Activation Key"),
              ),
            ),
            SizedBox(height: 60.h),
            filledInButton(
              text: "Submit",
              onTap: () {
                if (activationKeyController.text.isNotEmpty) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          PickColorPage(isOnboarding: true, uid: widget.uid),
                    ),
                  );
                } else {
                  customScaffoldMessage("Invalid activation key!", context);
                }
              },
            ),
            SizedBox(height: 60.h),
            Text("Not ready yet?",
                style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
            SizedBox(height: 5.h),
            InkWell(
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      HostHomePage(uid: widget.uid),
                ),
              ),
              child: Text("Start 14 days free trial",
                  style: TextStyle(
                      fontSize: 22.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.bold)),
            ),
            SizedBox(height: 30.h),
            Text("Not registered yet?",
                style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
            SizedBox(height: 5.h),
            InkWell(
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const RegisterPage(),
                ),
              ),
              child: Text("Register now",
                  style: TextStyle(
                      fontSize: 22.sp,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold)),
            ),
          ],
        ),
      ),
    );
  }
}
