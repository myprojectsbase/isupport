import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/registry/host_activation.dart';
import 'package:isupport/pages/registry/login.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/utilities.dart';
import 'login_type.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return customScaffold(
        child: SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Register",
              style: TextStyle(fontSize: 43.sp),
            ),
            SizedBox(height: 30.h),
            Text("Registration is quick and simple.",
                style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
            SizedBox(height: 5.h),
            Text("Fill in the form to get started.",
                style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
            SizedBox(height: 40.h),
            textFieldPadding(
              child: TextFormField(
                controller: fullNameController,
                keyboardType: TextInputType.name,
                validator: (val) {
                  if (val!.isEmpty) {
                    return "Name is required";
                  }
                },
                decoration: customInputDecoration(
                    hintText: "Full name (Surname first)"),
              ),
            ),
            SizedBox(height: 20.h),
            textFieldPadding(
              child: TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                validator: (val) {
                  if (val!.isEmpty) {
                    return "Email is required";
                  }
                  if (!isValidEmail(val)) {
                    return 'Enter valid email';
                  }
                },
                decoration: customInputDecoration(hintText: "Email"),
              ),
            ),
            SizedBox(height: 20.h),
            textFieldPadding(
              child: TextFormField(
                controller: phoneController,
                keyboardType: TextInputType.phone,
                validator: (val) {
                  if (val!.isEmpty) {
                    return "Phone number is required";
                  }
                },
                decoration: customInputDecoration(hintText: "Phone"),
              ),
            ),
            SizedBox(height: 40.h),
            filledInButton(
              text: "Register",
              onTap: () async {
                await SystemChannels.textInput.invokeMethod('TextInput.hide');
                if (_formKey.currentState!.validate()) {
                  customScaffoldMessage('Registering host...', context,
                      duration: const Duration(hours: 2));
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  String registrationID =
                      await DatabaseService().addHostAccount(
                    fullName: fullNameController.text,
                    email: emailController.text,
                    phoneNumber: phoneController.text,
                  );
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  if (registrationID.isNotEmpty) {
                    if (registrationID.contains('already used')) {
                      customScaffoldMessage(registrationID, context,
                          duration: const Duration(seconds: 5));
                    } else {
                      customScaffoldMessage(
                          'Registration successful!', context);
                      prefs.setString("uid", registrationID);
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              HostActivationPage(uid: registrationID),
                        ),
                      );
                    }
                  } else {
                    customScaffoldMessage(
                        'An error occured, please try again', context);
                  }
                }
              },
            ),
            SizedBox(height: 40.h),
            Text("Already registered?",
                style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
            SizedBox(height: 5.h),
            InkWell(
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const LoginTypePage(),
                ),
              ),
              child: Text("Login",
                  style: TextStyle(
                      fontSize: 22.sp,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold)),
            ),
          ],
        ),
      ),
    ));
  }
}
