import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/registry/register.dart';
import '../registry/login.dart';
import 'package:isupport/utils/components.dart';

class RegistrationTypePage extends StatefulWidget {
  const RegistrationTypePage({Key? key}) : super(key: key);

  @override
  State<RegistrationTypePage> createState() => _RegistrationTypePageState();
}

class _RegistrationTypePageState extends State<RegistrationTypePage> {
  @override
  Widget build(BuildContext context) {
    return customScaffold(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Register',
            style: TextStyle(fontSize: 43.sp),
          ),
          SizedBox(height: 30.h),
          Text("Select your registration type",
              style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
          SizedBox(height: 35.h),
          const Text(
            "Host is the political party or aspoirant who owns super privilege to the account",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black54),
          ),
          SizedBox(height: 20.h),
          borderedInButton(
            text: "Host",
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (BuildContext context) => const RegisterPage()),
            ),
          ),
          SizedBox(height: 43.h),
          const Text(
            "Agent & Group are users assigned by the host to campaign and manage accounts",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black54),
          ),
          SizedBox(height: 20.h),
          borderedInButton(
            text: "Agent/Group",
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    const LoginPage(isHost: false),
              ),
            ),
            buttonColor: Colors.black,
          ),
          SizedBox(height: 43.h),
          const Text(
            "Support is an electorate ready to keep up with the host to succeed. Remember, your vote counts.",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black54),
          ),
          SizedBox(height: 20.h),
          borderedInButton(
            text: "Support",
            onTap: () => customScaffoldMessage(
              'Supports coming soon!',
              context,
              duration: const Duration(seconds: 3),
            ),
            buttonColor: Colors.black,
          )
        ],
      ),
    );
  }
}
