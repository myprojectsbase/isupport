import 'package:flutter/material.dart';
import 'package:isupport/pages/registry/registration_type.dart';
import 'register.dart';
import 'login_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../utils/components.dart';

class RegisterOrLogin extends StatefulWidget {
  const RegisterOrLogin({Key? key}) : super(key: key);

  @override
  State<RegisterOrLogin> createState() => _RegisterOrLoginState();
}

class _RegisterOrLoginState extends State<RegisterOrLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Get Started",
              style: TextStyle(fontSize: 43.sp),
            ),
            SizedBox(height: 45.h),
            Text(
              "Register to get started",
              style: TextStyle(fontSize: 24.sp, color: Colors.black54),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 15.h),
            filledInButton(
              text: "Register",
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        const RegistrationTypePage()),
              ),
            ),
            SizedBox(height: 45.h),
            Text("Already registered?",
                style: TextStyle(fontSize: 24.sp, color: Colors.black54)),
            SizedBox(height: 15.h),
            borderedInButton(
                text: "Login",
                onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => const LoginTypePage())))
          ],
        ),
      ),
    ));
  }
}
