// ignore_for_file: use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/users/home.dart';
import 'package:isupport/services/auth.dart';
import 'package:isupport/utils/constants.dart';
import '../../services/database.dart';
import '../../utils/utilities.dart';
import '../registry/register.dart';
import '../main/hosts/home.dart';
import 'package:isupport/utils/components.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key, required this.isHost, this.justRegistered = false})
      : super(key: key);
  final bool isHost;
  final bool justRegistered;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool obscurePassword = true;

  Widget hostChooseWidget(List<DocumentSnapshot> userSnapshots) {
    Widget hostItem(DocumentSnapshot userSnapshot) {
      return FutureBuilder(
        future: DatabaseService()
            .getHostData(hostUID: userSnapshot.get('hostAssociated')),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> hostSnapshot) {
          if (!hostSnapshot.hasData) {
            return Container();
          }
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 10.h),
            child: InkWell(
              onTap: () {
                customScaffoldMessage('Login Successful. Welcome!', context);
                AuthService()
                    .setUserIDAndType(uid: userSnapshot.id, type: 'user');
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        UserHomePage(uid: userSnapshot.id),
                  ),
                );
              },
              child: roundedContainer(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 8.h, horizontal: 10.w),
                  child: Row(
                    children: [
                      Container(
                        height: 55.w,
                        width: 55.w,
                        margin: EdgeInsets.only(right: 10.w),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: hostSnapshot.data!.get('profilePhotoURL') ==
                                    null
                                ? Colors.grey[200]
                                : Colors.white),
                        child: Container(
                          padding: EdgeInsets.all(8.w),
                          child: hostSnapshot.data!.get('profilePhotoURL') ==
                                  null
                              ? Icon(
                                  Icons.person,
                                  size: 30.w,
                                  color: Colors.black54,
                                )
                              : FittedBox(
                                  fit: BoxFit.fill,
                                  child: Image.network(
                                    hostSnapshot.data!.get('profilePhotoURL'),
                                  ),
                                ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 10.w),
                        child: Center(
                          child: Text(
                            hostSnapshot.data!.get('fullName'),
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    }

    return Padding(
      padding: EdgeInsets.all(20.w),
      child: IntrinsicHeight(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.h),
              child: Text(
                'Select a host account',
                style: TextStyle(
                  fontSize: 20.sp,
                  color: Colors.black54,
                ),
              ),
            ),
            ...userSnapshots.map((e) => hostItem(e)).toList()
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Login',
                style: TextStyle(fontSize: 43.sp),
              ),
              SizedBox(height: 30.h),
              Text("Login to access your account",
                  style: TextStyle(fontSize: 22.sp, color: Colors.black54)),
              SizedBox(height: 40.h),
              textFieldPadding(
                child: widget.isHost
                    ? TextFormField(
                        style: TextStyle(fontSize: 22.sp),
                        controller: usernameController,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Field is empty";
                          }
                          if (!isValidEmail(val)) {
                            return 'Enter valid email';
                          }
                        },
                        decoration: customInputDecoration(hintText: "Email"),
                      )
                    : TextFormField(
                        style: TextStyle(fontSize: 22.sp),
                        keyboardType: TextInputType.phone,
                        controller: usernameController,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Field is empty";
                          }
                        },
                        decoration:
                            customInputDecoration(hintText: "Phone number"),
                      ),
              ),
              SizedBox(height: 30.h),
              textFieldPadding(
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        style: TextStyle(fontSize: 22.sp),
                        controller: passwordController,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Field is empty";
                          }
                        },
                        obscureText: obscurePassword,
                        decoration: customInputDecoration(
                            hintText:
                                widget.justRegistered || widget.isHost == false
                                    ? "Access Key"
                                    : "Password"),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.w),
                      child: InkWell(
                        onTap: () => setState(() {
                          obscurePassword = !obscurePassword;
                        }),
                        child: Icon(
                          obscurePassword
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: darkColor,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 40.h),
              filledInButton(
                text: "Login",
                onTap: () async {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                  if (_formKey.currentState!.validate()) {
                    customScaffoldMessage(
                      "Authenticating...",
                      context,
                      duration: const Duration(hours: 1),
                    );
                    var response;
                    if (widget.isHost) {
                      response = await AuthService().hostLogin(
                        username: usernameController.text,
                        password: passwordController.text,
                      );
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      if (response.contains('uid')) {
                        String uid = response.replaceAll('uid: ', '');
                        customScaffoldMessage(
                            'Login Successful. Welcome!', context);
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                HostHomePage(uid: uid),
                          ),
                        );
                      } else {
                        customScaffoldMessage(response, context,
                            duration: const Duration(seconds: 3));
                      }
                    } else {
                      response = await AuthService().agentLogin(
                        username: usernameController.text,
                        password: passwordController.text,
                      );
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      if (response.runtimeType == String) {
                        if (response.contains('uid')) {
                          String uid = response.replaceAll('uid: ', '');
                          customScaffoldMessage(
                              'Login Successful. Welcome!', context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  UserHomePage(uid: uid),
                            ),
                          );
                        } else {
                          customScaffoldMessage(response, context,
                              duration: const Duration(seconds: 3));
                        }
                      } else {
                        List<DocumentSnapshot> userSnapshots = response;
                        print(userSnapshots);
                        customShowDialog(
                            context, hostChooseWidget(userSnapshots));
                      }
                    }
                  }
                },
              ),
              if (widget.isHost)
                Column(
                  children: [
                    SizedBox(height: 40.h),
                    Text("Not registered yet?",
                        style:
                            TextStyle(fontSize: 22.sp, color: Colors.black54)),
                    SizedBox(height: 5.h),
                    InkWell(
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const RegisterPage(),
                        ),
                      ),
                      child: Text(
                        "Register now",
                        style: TextStyle(
                            fontSize: 22.sp,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
