// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/database.dart';
import '../main/hosts/home.dart';

class PickColorPage extends StatefulWidget {
  const PickColorPage({
    super.key,
    this.changeHomepageThemeColor,
    this.isOnboarding = false,
    required this.uid,
  });
  final Function(int colorCode)? changeHomepageThemeColor;
  final String uid;
  final bool isOnboarding;

  @override
  State<PickColorPage> createState() => _PickColorPageState();
}

class _PickColorPageState extends State<PickColorPage> {
  String appearanceOptionChosen = 'Default';
  int colorCodeChosen = 0xFF0CA404;
  File? image;

  Future<List<File>> getImage({bool allowMultiple = false}) async {
    List<File> imagesAssociated = <File>[];
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: allowMultiple,
        type: FileType.custom,
        allowedExtensions: ['jpg', 'jpeg', 'png', 'svg']);

    if (result != null) {
      imagesAssociated = result.paths.map((path) => File(path!)).toList();
      print(imagesAssociated);
    }
    return imagesAssociated;
  }

  selectImage({bool usingCamera = false}) async {
    await Permission.storage.request();
    File? imageSelected;
    var permissionStatus = await Permission.storage.status;

    if (permissionStatus.isGranted) {
      if (usingCamera) {
        final ImagePicker _picker = ImagePicker();
        // Capture a photo
        final XFile? photo =
            await _picker.pickImage(source: ImageSource.camera);
        imageSelected = File(photo!.path);
      } else {
        //Select Image from galery
        List<File> results = await getImage();
        imageSelected = results.first;
      }
      CroppedFile? croppedImage = await getImageCropped(imageSelected,
          aspectRatio: const CropAspectRatio(ratioX: 2.5, ratioY: 1.0));
      setState(() {
        image = File(croppedImage!.path);
      });
    } else {
      customScaffoldMessage(
          'Permission not granted. Try Again with permission access', context);
    }
  }

  Widget appearanceOptionItem(String choice) {
    Widget child = Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 38.w),
      child: Text(
        choice,
        style: TextStyle(
            fontSize: 18.sp,
            color: choice == appearanceOptionChosen ? Colors.white : null),
      ),
    );
    return InkWell(
      onTap: () {
        if (appearanceOptionChosen != choice) {
          setState(() {
            appearanceOptionChosen = choice;
          });
        }
      },
      child: choice == appearanceOptionChosen
          ? filledInContainer(
              child: child,
              buttonColor: const Color(0xFF0F8984),
            )
          : roundedContainer(
              child: child,
              buttonColor: Colors.grey.shade400,
              backgroundColor: Colors.white,
            ),
    );
  }

  Widget colorSchemeChoiceItem(int colorCode) {
    return InkWell(
      onTap: () {
        if (colorCodeChosen != colorCode) {
          setState(() {
            colorCodeChosen = colorCode;
            if (widget.changeHomepageThemeColor != null) {
              widget.changeHomepageThemeColor!(colorCode);
            }
          });
          DatabaseService().updateHostData(
            {'themeColorCode': colorCodeChosen},
            hostUID: widget.uid,
          );
        }
      },
      child: Container(
        height: 56.w,
        width: 56.w,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Color(colorCode),
        ),
        child: colorCodeChosen == colorCode
            ? const Icon(
                Icons.done,
                color: Colors.white,
              )
            : null,
      ),
    );
  }

  Future uploadBannerImage() async {
    if (image != null) {
      String downloadURL = await uploadFileToFirebaseStorage(image!);
      await DatabaseService()
          .updateHostData({'bannerPhotoURL': downloadURL}, hostUID: widget.uid);
    }
  }

  @override
  void initState() {
    Future.delayed(const Duration(seconds: 0), () async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int? colorCode = prefs.getInt('themeColorCode');
      if (colorCode != null) {
        setState(() {
          colorCodeChosen = colorCode;
        });
      }
    });
    super.initState();
  }

  Widget bannerUploadButton() {
    return SizedBox(
      width: 170.w,
      child: filledInButton(
          onTap: () async {
            if (image != null) {
              customScaffoldMessage('Setting banner image...', context,
                  duration: const Duration(hours: 1));
              await uploadBannerImage().then((value) {
                removeCurrentSnackbar(context);
                customScaffoldMessage('Banner set successfully', context);
                if (widget.isOnboarding) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          HostHomePage(uid: widget.uid),
                    ),
                  );
                }
              });
            } else {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      HostHomePage(uid: widget.uid),
                ),
              );
            }
          },
          text: widget.isOnboarding ? 'Done' : 'Upload Banner',
          fontSize: 18.sp,
          radius: 20.w,
          padding: 10.w),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0XFFF0F0F0),
      appBar: customAppbar(context, title: 'Appearance'),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 11.w),
        child: Column(
          children: [
            SizedBox(height: 25.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 60.w),
              child: Text(
                "Take a moment to customize the appearance of your app",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24.sp,
                  color: darkColor,
                ),
              ),
            ),
            SizedBox(height: 17.h),
            image == null
                ? Column(
                    children: [
                      SizedBox(height: 20.h),
                      Text(
                        "Upload your header banner",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18.sp, color: darkColor),
                      ),
                      Text(
                        "Header banner appears in your home page",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 14.sp, color: darkColor),
                      ),
                      SizedBox(height: 13.h),
                      InkWell(
                        onTap: () => selectImage(),
                        child: Icon(
                          Icons.cloud_upload,
                          color: const Color(0xFF0F8984),
                          size: 60.w,
                        ),
                      ),
                      SizedBox(height: 20.h),
                    ],
                  )
                : Stack(
                    children: [
                      SizedBox(
                        height: 145.h,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.w),
                          clipBehavior: Clip.hardEdge,
                          child: Image.file(image!, fit: BoxFit.fill),
                        ),
                      ),
                      Positioned(
                        bottom: 20.h,
                        child: Padding(
                          padding: EdgeInsets.only(left: 20.w),
                          child: roundedContainer(
                            backgroundColor: Colors.white54,
                            buttonColor: Colors.white,
                            child: InkWell(
                              onTap: () => selectImage(),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.w, vertical: 5.h),
                                child: const Text(
                                  'Change Image',
                                  style: TextStyle(
                                    color: darkColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
            SizedBox(height: 15.h),
            Text(
              "Appearance",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 26.sp,
                color: darkColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 17.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                appearanceOptionItem("Dark"),
                appearanceOptionItem("Light"),
                appearanceOptionItem("Default"),
              ],
            ),
            SizedBox(height: 15.h),
            Align(
              alignment: Alignment.centerLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Theme",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24.sp,
                      color: darkColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 5.h),
                  Text(
                    "Pick a color scheme",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.sp,
                      color: Colors.grey.shade400,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 14.h),
            SizedBox(
              width: double.infinity,
              child: filledInContainer(
                radius: 20.w,
                buttonColor: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 30.h,
                    horizontal: 30.w,
                  ),
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    spacing: 30.w,
                    runSpacing: 20.w,
                    children: themeColorCodes
                        .map((e) => colorSchemeChoiceItem(e))
                        .toList(),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20.h),
            widget.isOnboarding
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                          onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      HostHomePage(uid: widget.uid),
                                ),
                              ),
                          child: Text(
                            "Skip",
                            style: TextStyle(
                                fontSize: 18.sp, fontWeight: FontWeight.bold),
                          )),
                      bannerUploadButton(),
                    ],
                  )
                : image == null
                    ? Container()
                    : Align(
                        alignment: Alignment.centerRight,
                        child: bannerUploadButton(),
                      )
          ],
        ),
      ),
    );
  }
}
