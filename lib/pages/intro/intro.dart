// import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/constants.dart';
import '../registry/choose.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  int _currentPage = 0;
  // late Timer _timer;
  final PageController _pageController = PageController(
    initialPage: 0,
  );

  void pageChangeControl() {
    if (_currentPage < 2) {
      _pageController.animateToPage(
        _currentPage + 1,
        duration: const Duration(milliseconds: 350),
        curve: Curves.easeInExpo,
      );
      setState(() {
        _currentPage++;
      });
    } else {
      // _timer.cancel();
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => const RegisterOrLogin()));
    }
  }

  Map<String, Widget> infographicsMap = {
    "Data is Power": Image.asset('assets/images/infographics/01-data.png'),
    "Build Strategy": Image.asset('assets/images/infographics/02-Strategy.png'),
    "Stay in Control": Image.asset('assets/images/infographics/03-Control.png'),
  };

  Widget introWidget(
      {required String header,
      required String substring1,
      required String substring2}) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 300.w,
            width: 300.w,
            child: FittedBox(
              fit: BoxFit.contain,
              child: infographicsMap[header],
            ),
          ),
          SizedBox(height: 59.h),
          Text(header,
              style: TextStyle(
                  fontSize: 36.sp,
                  fontWeight: FontWeight.bold,
                  color: primaryColor)),
          SizedBox(height: 51.h),
          Text(substring1, style: TextStyle(fontSize: 23.sp)),
          const SizedBox(height: 5),
          Text(substring2, style: TextStyle(fontSize: 23.sp))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            controller: _pageController,
            onPageChanged: (pageNumber) {
              setState(() {
                _currentPage = pageNumber;
              });
            },
            children: [
              introWidget(
                  header: "Data is Power",
                  substring1: "We collate them for you",
                  substring2: "You know who votes for you"),
              introWidget(
                  header: "Build Strategy",
                  substring1: "With data in your possesion,",
                  substring2: "you lead"),
              introWidget(
                  header: "Stay in Control",
                  substring1: "Win the hearts of the people",
                  substring2: "and be in control"),
            ],
          ),
          Container(
            padding: EdgeInsets.only(bottom: 20.h, left: 30.w),
            alignment: Alignment.bottomLeft,
            child: InkWell(
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const RegisterOrLogin()));
                },
                child: Text(
                  "Skip",
                  style: TextStyle(fontSize: 21.sp),
                )),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 25.h),
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 10.w,
                  width: 10.w,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentPage == 0 ? primaryColor : Colors.grey,
                  ),
                ),
                SizedBox(width: 12.w),
                Container(
                  height: 10.w,
                  width: 10.w,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentPage == 1 ? primaryColor : Colors.grey,
                  ),
                ),
                SizedBox(width: 12.w),
                Container(
                  height: 10.w,
                  width: 10.w,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentPage == 2 ? primaryColor : Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 20.h, right: 30.w),
            alignment: Alignment.bottomRight,
            child: InkWell(
                onTap: () {
                  // _timer.cancel();
                  pageChangeControl();
                },
                child: Text(
                  "Next",
                  style: TextStyle(fontSize: 21.sp),
                )),
          ),
        ],
      ),
    );
  }
}
