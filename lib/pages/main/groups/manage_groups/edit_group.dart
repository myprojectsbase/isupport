// ignore_for_file: use_build_context_synchronously

import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/work_station/dropbox/dropbox.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/wards.dart';
import 'package:uuid/uuid.dart';

import '../../../../utils/constants.dart';
import '../../../../utils/utilities.dart';

class EditGroupPage extends StatefulWidget {
  const EditGroupPage({Key? key, required this.groupSnapshot})
      : super(key: key);
  final DocumentSnapshot groupSnapshot;

  @override
  State<EditGroupPage> createState() => _EditGroupPageState();
}

class _EditGroupPageState extends State<EditGroupPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController groupnameController = TextEditingController();
  TextEditingController groupLeaderController = TextEditingController();
  TextEditingController candidateController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  Map<int, Map<String, dynamic>> senetorialDistrictCoordinators = {};
  Map<int, Map<String, dynamic>> localGovernmentCoordinators = {};
  DateTime? dateOfBirth;
  String activationCode = '';
  bool pageLoaded = false;
  String? state;
  String? category;
  String lga = 'Select LGA';
  String ward = 'Select Ward';
  bool stateSelected = false;

  void getGroupData() async {
    groupnameController.text = widget.groupSnapshot.get('groupName');
    groupLeaderController.text = widget.groupSnapshot.get('groupLeader');
    descriptionController.text = widget.groupSnapshot.get('description') ?? '';
    phoneController.text = widget.groupSnapshot.get('phoneNumber');
    emailController.text = widget.groupSnapshot.get('email');
    candidateController.text = widget.groupSnapshot.get('candidate');
    category = widget.groupSnapshot.get('category');
    List remoteSenatorialCoordinators = [];
    List remoteLGACoordinators = [];
    try {
      remoteSenatorialCoordinators =
          widget.groupSnapshot.get('senatorialCoordinators');
    } catch (e) {
      print(e);
    }
    try {
      remoteLGACoordinators = widget.groupSnapshot.get('lgaCoordinators');
    } catch (e) {
      print(e);
    }
    for (var element in remoteSenatorialCoordinators) {
      int currentLength = senetorialDistrictCoordinators.length;
      senetorialDistrictCoordinators[currentLength] = {};
      senetorialDistrictCoordinators[currentLength]!['name'] =
          TextEditingController(text: element['name']);
      senetorialDistrictCoordinators[currentLength]!['phoneNumber'] =
          TextEditingController(text: element['phoneNumber']);
      senetorialDistrictCoordinators[currentLength]!['isDeleted'] = false;
    }
    for (var element in remoteLGACoordinators) {
      int currentLength = localGovernmentCoordinators.length;
      localGovernmentCoordinators[currentLength] = {};
      localGovernmentCoordinators[currentLength]!['name'] =
          TextEditingController(text: element['name']);
      localGovernmentCoordinators[currentLength]!['phoneNumber'] =
          TextEditingController(text: element['phoneNumber']);
      localGovernmentCoordinators[currentLength]!['isDeleted'] = false;
    }
    try {
      state = widget.groupSnapshot.get('state');
      if (state != null) {
        if (!statesToLGAsToWardsMap.keys.contains(state) || state == '') {
          state = null;
        }
      }
    } catch (e) {
      null;
    }
    setState(() {
      pageLoaded = true;
    });
  }

  Widget coordinatorFields(Map<int, Map<String, dynamic>> coordinatorMap) {
    Widget textField(TextEditingController nameController,
        TextEditingController phoneController) {
      return Column(
        children: [
          textFieldPadding(
            padding: EdgeInsets.only(left: 20.w),
            buttonColor: Colors.black54,
            radius: 0,
            child: TextFormField(
              keyboardType: TextInputType.name,
              controller: nameController,
              validator: (val) {
                if (val!.isEmpty) {
                  return "Field cannot be empty";
                }
              },
              decoration: customInputDecoration(
                  hintText: 'District name & Full name of Coordinator'),
            ),
          ),
          SizedBox(height: 3.h),
          textFieldPadding(
            padding: EdgeInsets.only(left: 20.w),
            buttonColor: Colors.black54,
            radius: 0,
            child: TextFormField(
              keyboardType: TextInputType.phone,
              controller: phoneController,
              validator: (val) {
                if (val!.isEmpty) {
                  return "Field cannot be empty";
                }
              },
              decoration: customInputDecoration(
                  hintText: 'Phone number of Coordinator'),
            ),
          ),
        ],
      );
    }

    return Column(
      children: [
        ...coordinatorMap.keys
            .where((element) => coordinatorMap[element]!['isDeleted'] == false)
            .map(
              (e) => Padding(
                padding: EdgeInsets.only(bottom: 8.h),
                child: Row(
                  children: [
                    Expanded(
                      child: textField(coordinatorMap[e]!['name'],
                          coordinatorMap[e]!['phoneNumber']),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.w),
                      child: InkWell(
                        onTap: () {
                          coordinatorMap.remove(e);
                          setState(() {});
                        },
                        child: roundedContainer(
                          buttonColor: Colors.black,
                          radius: 0,
                          child: Padding(
                            padding: EdgeInsets.all(3.w),
                            child: Icon(
                              Icons.remove,
                              size: 20.w,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
            .toList(),
        SizedBox(height: 5.h),
        Padding(
          padding: EdgeInsets.only(left: 5.w),
          child: InkWell(
            onTap: () {
              TextEditingController nameController = TextEditingController();
              TextEditingController phoneController = TextEditingController();
              int currentLength = coordinatorMap.length;
              coordinatorMap[currentLength] = {};
              coordinatorMap[currentLength]!['name'] = nameController;
              coordinatorMap[currentLength]!['phoneNumber'] = phoneController;
              coordinatorMap[currentLength]!['isDeleted'] = false;
              print(coordinatorMap);
              setState(() {});
            },
            child: roundedContainer(
              buttonColor: Colors.black,
              radius: 0,
              child: Padding(
                padding: EdgeInsets.all(8.w),
                child: IntrinsicWidth(
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 5.w),
                        child: Icon(
                          Icons.add,
                          size: 20.w,
                        ),
                      ),
                      const Text('Add Coordinator')
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  void initState() {
    if (!pageLoaded) {
      getGroupData();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Group Registration"),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 40.h),
                Text(
                  'Please complete Group Registration',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20.sp, color: Colors.black),
                ),
                SizedBox(height: 30.h),
                Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: Column(
                      children: [
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 0,
                          child: TextFormField(
                            controller: groupnameController,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field cannot be empty";
                              }
                            },
                            decoration:
                                customInputDecoration(hintText: 'Group name'),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        CustomDropdownWidget(
                            initialDropdownValue: category,
                            hint: 'Group Category',
                            items: groupCategories,
                            onChanged: (newValue) {
                              category = newValue;
                            }),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 0,
                          child: TextFormField(
                            controller: groupLeaderController,
                            keyboardType: TextInputType.name,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field is empty";
                              }
                            },
                            decoration: customInputDecoration(
                                hintText: 'Supporting which candidate?'),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 0,
                          child: TextFormField(
                            controller: candidateController,
                            keyboardType: TextInputType.name,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field is empty";
                              }
                            },
                            decoration: customInputDecoration(
                                hintText: 'Full Name of Group Leader'),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 0,
                          child: TextFormField(
                            keyboardType: TextInputType.phone,
                            controller: phoneController,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field is empty";
                              }
                            },
                            decoration:
                                customInputDecoration(hintText: "Phone"),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 0,
                          child: TextFormField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            validator: (val) {
                              if (val!.isNotEmpty) {
                                if (!isValidEmail(val)) {
                                  return 'Enter valid email';
                                }
                              }
                            },
                            decoration:
                                customInputDecoration(hintText: "Email"),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        Stack(
                          children: [
                            CustomDropdownWidget(
                              initialDropdownValue: state,
                              hint: 'Select State',
                              items: statesToLGAsToWardsMap.keys.toList(),
                              onChanged: ((newValue) {
                                setState(() {
                                  state = newValue!;
                                });
                              }),
                            ),
                            if (stateSelected)
                              Positioned.fill(
                                  child: Container(
                                color: Colors.white54,
                              ))
                          ],
                        ),
                        SizedBox(height: 25.h),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Senetorial District Coordinators',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18.sp,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        coordinatorFields(senetorialDistrictCoordinators),
                        SizedBox(height: 25.h),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'L.G.A. Coordinators',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18.sp,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        coordinatorFields(localGovernmentCoordinators),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 0,
                          child: TextFormField(
                            controller: descriptionController,
                            maxLines: 4,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field is empty";
                              }
                            },
                            decoration: customInputDecoration(
                                hintText:
                                    'Give us a short description of your group'),
                          ),
                        ),
                        SizedBox(height: 30.h),
                        Container(
                          margin: EdgeInsets.only(bottom: 30.h),
                          // padding: EdgeInsets.symmetric(horizontal: 10.h),
                          alignment: Alignment.bottomCenter,
                          child: filledInButton(
                            text: 'Submit',
                            padding: 10.w,
                            fontSize: 26.sp,
                            onTap: () async {
                              SystemChannels.textInput
                                  .invokeMethod('TextInput.hide');
                              if (_formKey.currentState!.validate()) {
                                customScaffoldMessage(
                                  "Updating group details...",
                                  context,
                                  duration: const Duration(hours: 1),
                                );
                                List<Map<String, String>>
                                    senatorialCoordinators = [];
                                senetorialDistrictCoordinators
                                    .forEach((key, value) {
                                  if (value['name'].text.trim() != '' &&
                                      value['isDeleted'] == false) {
                                    Map<String, String> map = {};
                                    map['name'] = value['name'].text.trim();
                                    map['phoneNumber'] =
                                        value['phoneNumber'].text.trim();
                                    senatorialCoordinators.add(map);
                                  }
                                });
                                List<Map<String, String>> lgaCoordinators = [];
                                localGovernmentCoordinators
                                    .forEach((key, value) {
                                  if (value['name'].text.trim() != '' &&
                                      value['isDeleted'] == false) {
                                    Map<String, String> map = {};
                                    map['name'] = value['name'].text.trim();
                                    map['phoneNumber'] =
                                        value['phoneNumber'].text.trim();
                                    lgaCoordinators.add(map);
                                  }
                                });
                                Map<String, Object?> data = {
                                  'groupName': groupnameController.text,
                                  'groupLeader': groupLeaderController.text,
                                  'candidate': candidateController.text,
                                  'email': emailController.text,
                                  'phoneNumber': phoneController.text,
                                  'description': descriptionController.text,
                                  'state': state,
                                  'senatorialCoordinators':
                                      senatorialCoordinators,
                                  'lgaCoordinators': lgaCoordinators,
                                  'category': category,
                                  'groupActivated': true
                                };
                                print(lgaCoordinators);
                                print(senatorialCoordinators);
                                bool successful = await DatabaseService()
                                    .updateGroupData(data,
                                        groupID: widget.groupSnapshot.id)
                                    .then(
                                      (value) =>
                                          DatabaseService().updateUserData(
                                        {
                                          'shouldUpdateGroupInfo': false,
                                        },
                                        userID: widget.groupSnapshot
                                            .get('userAssociated'),
                                      ),
                                    );
                                ScaffoldMessenger.of(context)
                                    .hideCurrentSnackBar();
                                if (successful) {
                                  customScaffoldMessage(
                                      'Group update successful!', context,
                                      duration: const Duration(seconds: 3));
                                  Navigator.of(context).pop();
                                } else {
                                  customScaffoldMessage(
                                    'Group update failed. Try again!',
                                    context,
                                    duration: const Duration(seconds: 3),
                                  );
                                }
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
