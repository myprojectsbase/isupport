// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:isupport/pages/main/help.dart';
import 'package:isupport/pages/main/groups/manage_groups/edit_group.dart';
import 'package:isupport/services/database.dart';
import 'package:jiffy/jiffy.dart';
import 'package:localstore/localstore.dart';
import '../../../utils/utilities.dart';
import '../../registry/choose.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import '../users/manage_supports/register_support.dart';
import 'dropbox/dropbox.dart';
import 'manage_supports/supports.dart';

class GroupHomePage extends StatefulWidget {
  const GroupHomePage({Key? key, required this.uid}) : super(key: key);
  final String uid;

  @override
  State<GroupHomePage> createState() => _GroupHomePageState();
}

class _GroupHomePageState extends State<GroupHomePage>
    with WidgetsBindingObserver {
  bool pageLoaded = false;
  Widget dummyHeader() {
    return Scaffold(
      appBar: customAppbar(context),
      body: const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void uploadOfflineSupportsData() async {
    bool isConnected = await checkInternetConnection();
    if (isConnected) {
      String hostID = await DatabaseService()
          .getSpecificGroupData('hostAssociated', groupID: widget.uid);
      CollectionRef collectionRef =
          Localstore.instance.collection('${hostID}OfflineGroupSupports');
      final items = await collectionRef.get();
      if (items != null && items != {}) {
        for (var value in items.keys) {
          String? imagePath;
          print(value);
          var doc = items[value];
          if (doc != null) {
            print('Uploading data...');
            String? profilePhotoURL = doc['profilePhotoURL'];
            if (profilePhotoURL != null) {
              imagePath = profilePhotoURL;
              var imageFile = File(profilePhotoURL);
              profilePhotoURL = await uploadFileToFirebaseStorage(imageFile);
            }
            DatabaseService().addSupportsAccount(
              bank: doc['bank'],
              accountNumber: doc['accountNumber'],
              employmentStatus: doc['employmentStatus'],
              ward: doc['ward'],
              unit: doc['unit'],
              address: doc['address'],
              profilePhotoURL: profilePhotoURL,
              fullName: doc['fullName'],
              dateOfBirth: DateTime.parse(doc['dateOfBirth']),
              state: doc['state'],
              gender: doc['gender'],
              localGovernmentArea: doc['localGovernmentArea'],
              votersCardNumber: doc['votersCardNumber'],
              email: doc['email'],
              occupation: doc['occupation'],
              educationalQualification: doc['educationalQualification'],
              phoneNumber: doc['phoneNumber'],
              groupAssociated: widget.uid,
              hostAssociatedUID: hostID,
            );
            print('Upload successful. Deleting data...');
            print(doc.values);
            collectionRef
                .doc(value.replaceAll('/${hostID}OfflineGroupSupports/', ''))
                .delete()
                .then((value) {
              if (imagePath != null) {
                final dir = File(imagePath);
                dir.deleteSync();
              }
              print('Done');
            });
          }
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    if (pageLoaded == false) {
      uploadOfflineSupportsData();
      DatabaseService()
          .updateGroupData({'isOnline': true}, groupID: widget.uid);
      pageLoaded = true;
    }
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      DatabaseService()
          .updateGroupData({'isOnline': true}, groupID: widget.uid);
    } else {
      DatabaseService()
          .updateGroupData({'isOnline': false}, groupID: widget.uid);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: DatabaseService().groupSnapshots(groupID: widget.uid),
      builder: (BuildContext context,
          AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
        if (!snapshot.hasData) {
          return dummyHeader();
        }
        if (!snapshot.data!.exists) {
          DatabaseService().checkAndRemovePastTokens();
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: Text(
                      "Your account has been deleted and your access revoked. Kindly contact your host if you feel this was in error. Thank you.",
                      style: TextStyle(
                        fontSize: 22.sp,
                        color: darkColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 45.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    child: filledInButton(
                      text: 'Exit',
                      padding: 8.w,
                      onTap: () => Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const RegisterOrLogin(),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }
        return WillPopScope(
          onWillPop: () async {
            DatabaseService()
                .updateGroupData({'isOnline': false}, groupID: widget.uid);
            return true;
          },
          child: customScaffold(
            drawer: customDrawer(context, snapshot),
            appBar: roundedAppbar(context),
            child: SafeArea(
              child: Column(
                children: [
                  SizedBox(height: 30.h),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 75.w,
                        width: 75.w,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey.withOpacity(0.6),
                          image: snapshot.data!.get('profilePhotoURL') == null
                              ? null
                              : DecorationImage(
                                  image: CachedNetworkImageProvider(
                                    snapshot.data!.get('profilePhotoURL'),
                                  ),
                                ),
                        ),
                        child: snapshot.data!.get('profilePhotoURL') == null
                            ? Padding(
                                padding: EdgeInsets.all(20.w),
                                child: Icon(
                                  Icons.person,
                                  color: Colors.black54,
                                  size: 35.w,
                                ),
                              )
                            : null,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 15.w),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Hi",
                                style: TextStyle(
                                    fontSize: 22.sp, color: Colors.black54)),
                            Text(
                                snapshot.data!.get('groupName') ??
                                    'No name given',
                                style: TextStyle(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 1.h,
                    margin: EdgeInsets.only(top: 20.h, bottom: 7.h),
                    color: primaryColor,
                  ),
                  Row(
                    children: [
                      Text(
                        "Election Date: ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14.sp),
                      ),
                      FutureBuilder(
                          future: DatabaseService().getHostData(
                              hostUID: snapshot.data!.get('hostAssociated')),
                          builder: (BuildContext context,
                              AsyncSnapshot<DocumentSnapshot<Object?>>
                                  hostSnapshot) {
                            if (!hostSnapshot.hasData) {
                              return Container();
                            }
                            return Text(
                                hostSnapshot.data!.get('nextElectionDate') !=
                                        null
                                    ? Jiffy((hostSnapshot.data!
                                                    .get('nextElectionDate')
                                                as Timestamp?)!
                                            .toDate())
                                        .format("MMM d yyyy")
                                        .toUpperCase()
                                    : 'Set Election Date',
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 14.sp));
                          }),
                      const Spacer(),
                      Text(
                          Jiffy(DateTime.now())
                              .format("MMM d yyyy")
                              .toUpperCase(),
                          style:
                              TextStyle(color: Colors.black54, fontSize: 14.sp))
                    ],
                  ),
                  SizedBox(height: 73.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    child: Column(
                      children: [
                        Text(
                            "Support are electorates, to bring them on board you need to get their data by registering them.",
                            style: TextStyle(
                                fontSize: 16.sp, color: Colors.black54),
                            textAlign: TextAlign.center),
                        SizedBox(height: 14.h),
                        InkWell(
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  RegisterSupportPage(
                                      groupID: widget.uid,
                                      hostUID:
                                          snapshot.data!.get('hostAssociated')),
                            ),
                          ),
                          child: roundedContainer(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8.h, horizontal: 10.w),
                              child: Stack(
                                children: [
                                  Container(
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Color(0XFFC4C4C4)),
                                    child: Padding(
                                      padding: EdgeInsets.all(8.w),
                                      child: Icon(
                                        Icons.people,
                                        size: 21.w,
                                        color: Colors.black54,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 10.h, left: 10.w),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Register Support",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 51.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                roundedContainer(
                                  radius: 30.w,
                                  child: InkWell(
                                    onTap: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            GroupsSupportsPage(
                                                groupID: snapshot.data!.id,
                                                hostUID: snapshot.data!
                                                    .get('hostAssociated')),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 40.w, vertical: 15.h),
                                      child: Column(
                                        children: [
                                          Icon(Icons.people, size: 50.w),
                                          Text(
                                              snapshot.data!
                                                  .get('supports')
                                                  .length
                                                  .toString(),
                                              style: TextStyle(
                                                  fontSize: 40.sp,
                                                  fontWeight: FontWeight.bold))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                Text("Total Supports",
                                    style: TextStyle(
                                        fontSize: 16.sp, color: Colors.black54))
                              ],
                            ),
                            SizedBox(width: 30.w),
                            Column(
                              children: [
                                roundedContainer(
                                  radius: 30.w,
                                  child: InkWell(
                                    onTap: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            DropboxPage(
                                                snapshot: snapshot.data!),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 35.h, vertical: 25.w),
                                      child: SizedBox(
                                        height: 75.h,
                                        width: 60.h,
                                        child: Stack(
                                          children: [
                                            Positioned.fill(
                                              child: Icon(
                                                FontAwesomeIcons.message,
                                                size: 50.w,
                                                color: primaryColor,
                                              ),
                                            ),
                                            Positioned(
                                              right: 0,
                                              top: 0,
                                              child: StreamBuilder(
                                                  stream: FirebaseFirestore
                                                      .instance
                                                      .collection(
                                                          'GroupDropboxes')
                                                      .where('members',
                                                          arrayContains:
                                                              widget.uid)
                                                      .snapshots(),
                                                  builder:
                                                      (BuildContext context,
                                                          AsyncSnapshot<
                                                                  QuerySnapshot<
                                                                      Object?>>
                                                              snapshot) {
                                                    if (snapshot.hasData) {
                                                      if (snapshot
                                                          .data!.docs.isEmpty) {
                                                        return Container();
                                                      }
                                                      int numRoomsWithUnreadMessages =
                                                          0;
                                                      for (var element
                                                          in snapshot
                                                              .data!.docs) {
                                                        try {
                                                          if (element.get(widget
                                                                      .uid +
                                                                  "_unseen") >
                                                              0) {
                                                            numRoomsWithUnreadMessages++;
                                                          }
                                                        } catch (error) {
                                                          // print('');
                                                        }
                                                      }
                                                      return numRoomsWithUnreadMessages >
                                                              0
                                                          ? Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              child:
                                                                  IntrinsicWidth(
                                                                child:
                                                                    Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      bottom:
                                                                          25),
                                                                  padding:
                                                                      const EdgeInsets
                                                                          .all(4),
                                                                  decoration: const BoxDecoration(
                                                                      color: Colors
                                                                          .red,
                                                                      shape: BoxShape
                                                                          .circle),
                                                                  child: Center(
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsets
                                                                          .all(2
                                                                              .w),
                                                                      child:
                                                                          Text(
                                                                        numRoomsWithUnreadMessages
                                                                            .toString(),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                        style:
                                                                            const TextStyle(
                                                                          color:
                                                                              Colors.white,
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          : Container();
                                                    }
                                                    return Container();
                                                  }),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                Text("iChat",
                                    style: TextStyle(
                                        fontSize: 16.sp, color: Colors.black54))
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

Drawer customDrawer(
    BuildContext context, AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
  return Drawer(
    backgroundColor: const Color(0xFF444444),
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top + 40.h),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 50.w,
                width: 50.w,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: const Color(0XFFE5E5E5),
                  image: snapshot.data!.get('profilePhotoURL') == null
                      ? null
                      : DecorationImage(
                          image: CachedNetworkImageProvider(
                            snapshot.data!.get('profilePhotoURL'),
                          ),
                        ),
                ),
                child: snapshot.data!.get('profilePhotoURL') == null
                    ? Padding(
                        padding: EdgeInsets.all(10.w),
                        child: Icon(
                          Icons.person,
                          color: Colors.black54,
                          size: 25.w,
                        ),
                      )
                    : null,
              ),
              Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Hi", style: TextStyle(color: Colors.white)),
                    SizedBox(height: 3.h),
                    Text(snapshot.data!.get('groupName') ?? 'No name given',
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)),
                  ],
                ),
              ),
              const Spacer(),
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        EditGroupPage(groupSnapshot: snapshot.data!),
                  ),
                ),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Icon(Icons.edit, color: Colors.white, size: 16.sp),
                ),
              )
            ],
          ),
          Container(
            height: 1.h,
            margin: EdgeInsets.only(top: 20.h, bottom: 25.h),
            color: Colors.white,
          ),
          drawerItem(
              icon: const Icon(
                Icons.home,
                color: Colors.white,
              ),
              title: 'Dashboard',
              onTap: () => Navigator.of(context).pop()),
          drawerItem(
              icon: const Icon(
                Icons.person,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => GroupsSupportsPage(
                          groupID: snapshot.data!.id,
                          hostUID: snapshot.data!.get('hostAssociated')),
                    ),
                  ),
              title: 'Supports'),
          drawerItem(
              icon: const Icon(
                Icons.forum,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => DropboxPage(
                        snapshot: snapshot.data!,
                      ),
                    ),
                  ),
              title: 'iChats'),
          drawerItem(
              icon: const Icon(
                Icons.phone,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => const HelpPage(),
                    ),
                  ),
              title: 'Help'),
          drawerItem(
              icon: const Icon(
                Icons.edit,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          EditGroupPage(groupSnapshot: snapshot.data!),
                    ),
                  ),
              title: 'Edit Group Details'),
          SizedBox(height: 15.h),
          drawerItem(
            icon: const Icon(
              Icons.logout,
              color: Colors.white,
            ),
            title: 'Exit Group',
            onTap: () {
              DatabaseService().updateGroupData({'isOnline': false},
                  groupID: snapshot.data!.id);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    ),
  );
}

Widget drawerItem(
    {required Icon icon, required String title, void Function()? onTap}) {
  return InkWell(
    onTap: onTap,
    child: Padding(
      padding: EdgeInsets.symmetric(vertical: 19.h),
      child: Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 15.w), child: icon),
          Text(title, style: TextStyle(color: Colors.white, fontSize: 18.sp))
        ],
      ),
    ),
  );
}
