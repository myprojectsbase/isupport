import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'supports.dart';
import 'package:isupport/utils/components.dart';

class RegistrationCompletePage extends StatefulWidget {
  const RegistrationCompletePage(
      {Key? key, required this.groupID, required this.hostUID})
      : super(key: key);
  final String groupID;
  final String hostUID;

  @override
  State<RegistrationCompletePage> createState() =>
      _RegistrationCompletePageState();
}

class _RegistrationCompletePageState extends State<RegistrationCompletePage> {
  @override
  Widget build(BuildContext context) {
    return customScaffold(
        child: Column(
          children: [
            SizedBox(height: 86.h),
            Text(
              "Support has been successfully registered",
              style: TextStyle(fontSize: 28.sp, color: Colors.black54),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 102.h),
            filledInButton(
                onTap: () => Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (BuildContext context) => GroupsSupportsPage(
                          hostUID: widget.hostUID,
                          groupID: widget.groupID,
                        ),
                      ),
                    ),
                radius: 0,
                padding: 15.w,
                fontSize: 28.sp,
                text: 'See list of all supports')
          ],
        ),
        appBar: customAppbar(context, title: 'Registration Complete'));
  }
}
