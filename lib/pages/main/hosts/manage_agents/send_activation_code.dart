import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_agents/users.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../utils/utilities.dart';

class SendActivationCodePage extends StatefulWidget {
  const SendActivationCodePage(
      {Key? key, required this.activationCode, required this.hostName})
      : super(key: key);
  final String activationCode;
  final String hostName;

  @override
  State<SendActivationCodePage> createState() => _SendActivationCodePageState();
}

class _SendActivationCodePageState extends State<SendActivationCodePage> {
  bool showSMS = true;
  final _formKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Send Activation Code"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 55.h,
              color: Colors.grey[600],
              child: Row(
                children: [
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      onTap: () => setState(() {
                        showSMS = true;
                      }),
                      child: Column(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 5.h),
                              alignment: Alignment.bottomCenter,
                              child: Text(
                                "SMS",
                                style: TextStyle(
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold,
                                    color: showSMS
                                        ? Colors.white
                                        : Colors.white60),
                              ),
                            ),
                          ),
                          Container(
                              height: 8.h,
                              color:
                                  showSMS ? Colors.grey[200] : Colors.grey[600])
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      onTap: () => setState(() {
                        showSMS = false;
                      }),
                      child: Column(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 5.h),
                              alignment: Alignment.bottomCenter,
                              child: Text(
                                "Email",
                                style: TextStyle(
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold,
                                    color: showSMS
                                        ? Colors.white60
                                        : Colors.white),
                              ),
                            ),
                          ),
                          Container(
                              height: 8.h,
                              color:
                                  showSMS ? Colors.grey[600] : Colors.grey[200])
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 50.h),
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Column(
                  children: [
                    textFieldPadding(
                      padding: EdgeInsets.only(left: 10.w),
                      buttonColor: Colors.grey,
                      radius: 0,
                      child: showSMS
                          ? TextFormField(
                              controller: phoneController,
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Field is empty";
                                }
                              },
                              decoration: customInputDecoration(
                                  hintText: 'Phone Number'),
                            )
                          : TextFormField(
                              controller: emailController,
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Field is empty";
                                }
                                if (!isValidEmail(val)) {
                                  return 'Enter valid email';
                                }
                              },
                              decoration:
                                  customInputDecoration(hintText: 'Email'),
                            ),
                    ),
                    SizedBox(height: 30.h),
                    roundedContainer(
                      radius: 0,
                      buttonColor: Colors.grey,
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(15.w),
                            child: Text(
                              widget.activationCode,
                              style: TextStyle(
                                  fontSize: 18.sp, color: Colors.grey),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 40.h),
                    filledInButton(
                      text: 'Send',
                      radius: 0,
                      padding: 10.w,
                      fontSize: 22.sp,
                      onTap: () async {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        if (_formKey.currentState!.validate()) {
                          if (showSMS) {
                            customScaffoldMessage(
                              "Sending...",
                              context,
                              duration: const Duration(hours: 1),
                            );
                            bool messageSent = await sendAgentSMS(
                              hostName: widget.hostName,
                              receiverNumber: phoneController.text,
                              authCode: widget.activationCode,
                            );
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            if (messageSent) {
                              customScaffoldMessage(
                                "Message sent successfully. Adding new user...",
                                context,
                                duration: const Duration(hours: 1),
                              );
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              await DatabaseService().addUserAccount(
                                  phoneNumber: phoneController.text,
                                  authCode: widget.activationCode,
                                  hostAssociatedUID: prefs.getString('uid')!);
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                "User successfully created!",
                                context,
                                duration: const Duration(seconds: 3),
                              );
                              Navigator.of(context).pop();
                            } else {
                              customScaffoldMessage(
                                "Temporary error sending message. Please try again later.",
                                context,
                                duration: const Duration(seconds: 3),
                              );
                            }
                          } else {
                            customScaffoldMessage(
                              "Email sending yet to be supported",
                              context,
                              duration: const Duration(seconds: 3),
                            );
                          }
                        }
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
