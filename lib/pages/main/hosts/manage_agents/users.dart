import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_agents/delete_user.dart';
import 'package:isupport/pages/main/user_details.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../services/database.dart';

class UsersPage extends StatefulWidget {
  const UsersPage(
      {Key? key, this.uid, required this.hostName, this.isFirstUser = false})
      : super(key: key);
  final bool isFirstUser;
  final String hostName;
  final String? uid;

  @override
  State<UsersPage> createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  Widget userRow(String userUID) {
    return FutureBuilder(
        future: DatabaseService().getUserData(userID: userUID),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          if (snapshot.data!.exists == false) {
            return Container();
          }
          return Column(
            children: [
              const SizedBox(height: 15),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                child: Row(
                  children: [
                    Text(
                      snapshot.data!.get('fullName') ?? 'No name given',
                      style: TextStyle(fontSize: 18.sp, color: Colors.black54),
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => UserDetails(
                            userData: snapshot.data!,
                            isSupport: false,
                          ),
                        ),
                      ),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.grey, shape: BoxShape.circle),
                        child: Padding(
                          padding: EdgeInsets.all(5.w),
                          child: Icon(
                            Icons.visibility,
                            size: 16.w,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 15.w),
                    InkWell(
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => DeleteUserPage(
                              fullName: snapshot.data!.get('fullName') ??
                                  'No name given',
                              uid: userUID,
                              hostUID: widget.uid!),
                        ),
                      ),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.grey, shape: BoxShape.circle),
                        child: Padding(
                          padding: EdgeInsets.all(5.w),
                          child: Icon(
                            Icons.delete,
                            size: 16.w,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 1.h,
                color: Colors.grey,
                margin: EdgeInsets.symmetric(vertical: 10.h),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Agents"),
      body: StreamBuilder(
          stream: DatabaseService().hostUsersSnapshots(userID: widget.uid!),
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            }
            return snapshot.data!.docs.isEmpty
                ? InvitationWidget(isFirstUser: true, hostName: widget.hostName)
                : Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 20.w, top: 20.h),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Total agents (${snapshot.data!.docs.length})",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20.h),
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        child: InkWell(
                          onTap: () => Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  InvitationWidget(
                                      withScaffold: true,
                                      hostName: widget.hostName),
                            ),
                          ),
                          child: filledInContainer(
                            buttonColor: const Color(0XFFC4C4C4),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 12.w, horizontal: 25.w),
                              child: Row(
                                children: [
                                  Text(
                                    "Generate agent activation code",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const Spacer(),
                                  Container(
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.grey),
                                    child: Padding(
                                      padding: EdgeInsets.all(8.w),
                                      child: Icon(
                                        Icons.person_add,
                                        size: 21.w,
                                        color: Colors.white,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      ...snapshot.data!.docs.map((e) => userRow(e.id))
                    ],
                  );
          }),
    );
  }
}
