import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../services/database.dart';

class DeleteUserPage extends StatefulWidget {
  const DeleteUserPage(
      {Key? key,
      required this.fullName,
      required this.uid,
      required this.hostUID})
      : super(key: key);
  final String fullName;
  final String hostUID;
  final String uid;

  @override
  State<DeleteUserPage> createState() => _DeleteUserPageState();
}

class _DeleteUserPageState extends State<DeleteUserPage> {
  bool suspensionComplete = false;
  bool isDeletion = false;

  Widget actionCompleteWidget() {
    String deletedOrSuspendeString = isDeletion ? "deleted!" : "suspended!";
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          widget.fullName,
          style: TextStyle(
              color: primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 25.sp),
        ),
        SizedBox(height: 35.h),
        Text(
          "Has been " + deletedOrSuspendeString,
          style: TextStyle(color: Colors.grey, fontSize: 28.sp),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Delete User"),
      body: suspensionComplete
          ? Center(child: actionCompleteWidget())
          : Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      SizedBox(height: 30.h),
                      Text(
                        "You are about to delete or suspend",
                        style:
                            TextStyle(color: Colors.black54, fontSize: 18.sp),
                      ),
                      Text(
                        widget.fullName,
                        style: TextStyle(
                            color: primaryColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                      SizedBox(height: 40.h),
                      Text(
                        "You can either suspend or delete a user",
                        style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                      SizedBox(height: 30.h),
                      const Text(
                          "Suspending a user will not temporarily deactivate the user. In this case the user and its records can be recovered.",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black54)),
                      SizedBox(height: 30.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 45.w),
                        child: filledInButton(
                            text: "Suspend User",
                            fontSize: 18.sp,
                            padding: 10.w,
                            onTap: () async {
                              customScaffoldMessage('Hold on...', context,
                                  duration: const Duration(hours: 1));
                              await DatabaseService()
                                  .suspendUser(uid: widget.uid);
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              setState(() {
                                suspensionComplete = true;
                              });
                            }),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 1.h,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(vertical: 50.h),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      const Text(
                          "If you delete a user, you will loose the user and all its records which includes the supports registered by that user.",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black54)),
                      SizedBox(height: 30.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 45.w),
                        child: filledInButton(
                          text: "Delete User",
                          fontSize: 18.sp,
                          padding: 10.w,
                          buttonColor: Colors.grey,
                          onTap: () async {
                            customScaffoldMessage('Hold on...', context,
                                duration: const Duration(hours: 1));
                            await DatabaseService().deleteUser(
                                hostUID: widget.hostUID, uid: widget.uid);
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            setState(() {
                              isDeletion = true;
                              suspensionComplete = true;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
    );
  }
}
