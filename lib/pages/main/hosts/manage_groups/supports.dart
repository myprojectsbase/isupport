import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';

import '../../../../services/database.dart';
import '../../../../utils/constants.dart';
import '../../user_details.dart';

class GroupSupportsPage extends StatefulWidget {
  const GroupSupportsPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<GroupSupportsPage> createState() => _GroupSupportsPageState();
}

class _GroupSupportsPageState extends State<GroupSupportsPage> {
  Stream<QuerySnapshot<Object?>>? stream;
  TextEditingController searchController = TextEditingController();
  bool pageLoaded = false;
  bool isSearching = false;
  Widget supporterRow(String supporterUID) {
    return FutureBuilder(
        future: DatabaseService().getSupporterData(supporterUID: supporterUID),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          return Column(
            children: [
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        UserDetails(userData: snapshot.data!, isSupport: true),
                  ),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
                  child: Row(
                    children: [
                      Text(
                        snapshot.data!.get('fullName'),
                        style:
                            TextStyle(fontSize: 16.sp, color: Colors.black54),
                      ),
                      const Spacer(),
                      const Icon(
                        Icons.arrow_forward_ios,
                        // size: 16,
                        color: Colors.black54,
                      ),
                    ],
                  ),
                ),
              ),
              Container(height: 1.h, color: Colors.grey)
            ],
          );
        });
  }

  Widget searchResultsWidget(
      {required List<QueryDocumentSnapshot<Object?>> docs}) {
    Iterable<QueryDocumentSnapshot<Object?>> searchResults =
        docs.where((element) {
      if (element.get('fullName') == null) {
        return false;
      } else {
        if ((element.get('fullName') as String)
            .toLowerCase()
            .contains(searchController.text.trim().toLowerCase())) {
          return true;
        } else {
          return false;
        }
      }
    });
    return Container(
      color: Colors.white,
      child: searchResults.isNotEmpty
          ? Column(
              children: searchResults
                  .map(
                    (e) => supporterRow(e.id),
                  )
                  .toList(),
            )
          : const Center(child: Text('No results')),
    );
  }

  @override
  void initState() {
    if (!pageLoaded) {
      stream =
          DatabaseService().groupSupportsSnapshots(groupID: widget.snapshot.id);
      pageLoaded = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: customAppbar(context, title: 'Group'),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h),
            child: Text(
              'You are viewing supports registered by',
              style: TextStyle(fontSize: 14.sp),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.w),
            child: Text(
              widget.snapshot.get('groupName'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 30.w),
            child: Row(
              children: [
                Container(
                  height: 50.h,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(width: 1.h, color: primaryColor),
                      left: BorderSide(width: 1.h, color: primaryColor),
                      right: BorderSide(width: 1.h, color: primaryColor),
                      bottom: BorderSide(width: 1.h, color: primaryColor),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(8.w),
                      topLeft: Radius.circular(8.w),
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40.w),
                      child: Text(
                        'Total number of supports',
                        style: TextStyle(color: Colors.black, fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 50.h,
                    decoration: BoxDecoration(
                      color: Colors.black54,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(8.w),
                        topRight: Radius.circular(8.w),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        widget.snapshot.get('supports').length.toString(),
                        style: TextStyle(color: Colors.white, fontSize: 18.sp),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          StreamBuilder(
              stream: stream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
                if (!snapshot.hasData) {
                  return Container();
                }
                return Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 15.h),
                      color: Colors.white,
                      child: Column(
                        children: [
                          Container(
                              padding:
                                  EdgeInsets.only(left: 20.w, bottom: 10.h),
                              alignment: Alignment.center,
                              child: Text(
                                'Use search to find supports',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14.sp),
                              )),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.w),
                            child: roundedContainer(
                              radius: 8.w,
                              child: textFieldPadding(
                                buttonColor: Colors.white,
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: SizedBox(
                                        height: 50.h,
                                        child: TextField(
                                          onChanged: (value) {
                                            if (value.isEmpty) {
                                              setState(() {
                                                isSearching = false;
                                              });
                                            } else {
                                              setState(() {
                                                isSearching = true;
                                              });
                                            }
                                          },
                                          controller: searchController,
                                          decoration: customInputDecoration(
                                              hintText: 'Search'),
                                        ),
                                      ),
                                    ),
                                    isSearching
                                        ? InkWell(
                                            onTap: () {
                                              searchController.text = '';
                                              setState(() {
                                                isSearching = false;
                                              });
                                            },
                                            child: const Icon(Icons.cancel,
                                                color: Colors.grey),
                                          )
                                        : const Icon(Icons.search,
                                            color: Colors.grey)
                                  ],
                                ),
                              ),
                              buttonColor: const Color(0xFF003FCA),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 50.h,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Supports",
                                      style: TextStyle(
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black54),
                                    ),
                                  ],
                                ),
                              ),
                              ...snapshot.data!.docs
                                  .map((e) => supporterRow(e.id))
                            ],
                          ),
                          if (isSearching)
                            Positioned.fill(
                                child: searchResultsWidget(
                                    docs: snapshot.data!.docs))
                        ],
                      ),
                    ),
                  ],
                );
              })
        ],
      ),
    );
  }
}
