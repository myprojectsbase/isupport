import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/work_station/dropbox/dropbox.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:uuid/uuid.dart';

import '../../../../utils/constants.dart';
import '../../../../utils/utilities.dart';

class RegisterGroupPage extends StatefulWidget {
  const RegisterGroupPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<RegisterGroupPage> createState() => _RegisterGroupPageState();
}

class _RegisterGroupPageState extends State<RegisterGroupPage> {
  bool showSMS = true;
  final _formKey = GlobalKey<FormState>();
  TextEditingController groupnameController = TextEditingController();
  TextEditingController groupLeaderController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  DateTime? dateOfBirth;
  String activationCode = '';

  void generateRandomCode() {
    setState(() {
      activationCode = getRandomString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Group Registration"),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 30.h),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      Text(
                        'You are required to provide the group detail before you generate access code to grant the group permission to join your group list.',
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 18.sp, color: Colors.black54),
                      ),
                      Text(
                        'Your request will be sent as an SMS to the phone number provided.',
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 18.sp, color: Colors.black54),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20.h),
                roundedContainer(
                  radius: 20.w,
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.h, horizontal: 15.w),
                    child: Icon(
                      Icons.groups,
                      size: 95.w,
                      color: darkColor,
                    ),
                  ),
                ),
                SizedBox(height: 30.h),
                Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: Column(
                      children: [
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 30.w,
                          child: TextFormField(
                            controller: groupnameController,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field cannot be empty";
                              }
                            },
                            decoration:
                                customInputDecoration(hintText: 'Group name'),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 30.w,
                          child: TextFormField(
                            controller: groupLeaderController,
                            keyboardType: TextInputType.name,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field is empty";
                              }
                            },
                            decoration: customInputDecoration(
                                hintText: 'Full Name of Group Leader'),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 30.w,
                          child: TextFormField(
                            keyboardType: TextInputType.phone,
                            controller: phoneController,
                            validator: (val) {
                              if (val!.isEmpty) {
                                return "Field is empty";
                              }
                            },
                            decoration: customInputDecoration(
                                hintText: "Leader's Phone"),
                          ),
                        ),
                        SizedBox(height: 15.h),
                        textFieldPadding(
                          padding: EdgeInsets.only(left: 20.w),
                          buttonColor: Colors.black54,
                          radius: 30.w,
                          child: TextFormField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            validator: (val) {
                              if (val!.isNotEmpty) {
                                if (!isValidEmail(val)) {
                                  return 'Enter valid email';
                                }
                              }
                            },
                            decoration: customInputDecoration(
                                hintText: "Leader's Email"),
                          ),
                        ),
                        SizedBox(height: 30.h),
                        Text(
                          "Generate Group Access Code",
                          style: TextStyle(
                              fontSize: 16.sp, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 15.h),
                        filledInButton(
                            text: 'Generate',
                            padding: 10.w,
                            fontSize: 25.sp,
                            onTap: () => generateRandomCode()),
                        SizedBox(height: 20.h),
                        roundedContainer(
                          buttonColor: Colors.grey,
                          child: SizedBox(
                            height: 45.h,
                            child: Stack(
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    activationCode,
                                    style: TextStyle(
                                        fontSize: 18.sp, color: Colors.grey),
                                  ),
                                ),
                                if (activationCode.isNotEmpty)
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: InkWell(
                                      onTap: () => setState(() {
                                        activationCode = '';
                                      }),
                                      child: const Align(
                                        alignment: Alignment.centerRight,
                                        child: Icon(Icons.cancel),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 20.h),
                        filledInButton(
                            text: 'Send',
                            padding: 10.w,
                            fontSize: 25.sp,
                            buttonColor: activationCode.isEmpty
                                ? Colors.grey
                                : primaryColor,
                            onTap: () async {
                              if (activationCode.isEmpty) {
                                customScaffoldMessage(
                                    "You need to generate an activation code first",
                                    context);
                              } else {
                                if (_formKey.currentState!.validate()) {
                                  customScaffoldMessage(
                                    'Registering group leader...',
                                    context,
                                    duration: const Duration(hours: 1),
                                  );
                                  List currentUsers =
                                      widget.snapshot.get('users');
                                  String userID =
                                      await DatabaseService().addUserAccount(
                                    authCode: activationCode,
                                    hostAssociatedUID: widget.snapshot.id,
                                    phoneNumber: phoneController.text,
                                    fullName: groupLeaderController.text,
                                    shouldUpdateGroupInfo: true,
                                  );
                                  bool isNewUser =
                                      !currentUsers.contains(userID);
                                  ScaffoldMessenger.of(context)
                                      .hideCurrentSnackBar();
                                  customScaffoldMessage(
                                    'Registering group...',
                                    context,
                                    duration: const Duration(hours: 1),
                                  );
                                  await DatabaseService()
                                      .addGroup(
                                    activationCode: activationCode,
                                    groupName: groupnameController.text,
                                    groupLeader: groupLeaderController.text,
                                    email: emailController.text,
                                    leaderUID: userID,
                                    phoneNumber: phoneController.text,
                                    hostAssociatedUID: widget.snapshot.id,
                                  )
                                      .then((value) {
                                    DatabaseService().updateHostData({
                                      "groups": FieldValue.arrayUnion([value])
                                    }, hostUID: widget.snapshot.id);
                                    DatabaseService().updateUserData({
                                      "groupsLeading":
                                          FieldValue.arrayUnion([value])
                                    }, userID: userID);
                                  });
                                  ScaffoldMessenger.of(context)
                                      .hideCurrentSnackBar();
                                  customScaffoldMessage(
                                    'Sending message to group leader...',
                                    context,
                                    duration: const Duration(hours: 1),
                                  );
                                  bool sendMessage = await sendGroupLeaderSMS(
                                    leaderName: groupLeaderController.text,
                                    groupName: groupnameController.text,
                                    receiverNumber: phoneController.text,
                                    hostName: widget.snapshot.get('fullName'),
                                    authCode: activationCode,
                                    isNewUser: isNewUser,
                                  );
                                  // ignore: use_build_context_synchronously
                                  ScaffoldMessenger.of(context)
                                      .hideCurrentSnackBar();
                                  if (sendMessage) {
                                    // ignore: use_build_context_synchronously
                                    customScaffoldMessage(
                                      'Group successfully created and activation message successfully sent to group leader!',
                                      context,
                                      duration: const Duration(seconds: 3),
                                    );
                                    Navigator.of(context).pop();
                                  } else {
                                    // ignore: use_build_context_synchronously
                                    customScaffoldMessage(
                                      'Error sending message. You can find the code among the group details and send to the leader to active the group.',
                                      context,
                                      duration: const Duration(seconds: 5),
                                    );
                                  }
                                }
                              }
                            }),
                        SizedBox(height: 25.h),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
