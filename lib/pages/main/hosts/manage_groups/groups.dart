import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_groups/group_details.dart';
import 'package:isupport/pages/main/hosts/manage_groups/register_group.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import 'package:isupport/utils/utilities.dart';

class GroupsPage extends StatefulWidget {
  const GroupsPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot<Object?> snapshot;

  @override
  State<GroupsPage> createState() => _GroupsPageState();
}

class _GroupsPageState extends State<GroupsPage> {
  bool hasGroups = false;
  bool pageLoaded = false;
  String? filter;

  @override
  void initState() {
    if (!pageLoaded) {
      try {
        if ((widget.snapshot.get('groups') as List).isNotEmpty) {
          hasGroups = true;
        }
      } catch (e) {
        null;
      }
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  Widget filterDropdownButton() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      child: CustomDropdownWidget(
          initialDropdownValue: filter,
          hint: 'Filter by: Select',
          items: ['All', ...groupCategories],
          onChanged: (newValue) {
            setState(() {
              filter = newValue;
            });
          }),
    );
  }

  Widget groupRow(String groupID) {
    return FutureBuilder(
        future: DatabaseService().getGroupData(groupID: groupID),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          return Column(
            children: [
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        GroupDetailsPage(snapshot: snapshot.data!),
                  ),
                ),
                child: Container(
                  color: Colors.white,
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          snapshot.data!.get('groupName'),
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        child: const Icon(
                          Icons.arrow_forward_ios,
                          // size: 16,
                          color: Colors.black54,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(height: 1.h, color: Colors.grey[400])
            ],
          );
        });
  }

  Widget noGroupsWidget() {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 80.h, horizontal: 30.w),
          child: Column(
            children: [
              Text(
                'You do not have any group yet. Register a campaign group to boost your publicity.',
                style: TextStyle(fontSize: 18.sp),
                textAlign: TextAlign.center,
              ),
              Text(
                'To register a group, make a physical contact for a brief interview, get the leaders contact details and group name to register them.',
                style: TextStyle(fontSize: 18.sp),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 60.w),
          child: filledInButton(
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          RegisterGroupPage(snapshot: widget.snapshot),
                    ),
                  ),
              text: 'Register a Group',
              padding: 15.w,
              fontSize: 18.sp),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: customAppbar(context, title: 'Groups'),
      body: StreamBuilder(
          stream: DatabaseService()
              .hostGroupsSnapshots(hostUID: widget.snapshot.id),
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            }
            if (snapshot.data!.docs.isEmpty) {
              return noGroupsWidget();
            }
            return SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 25.h, horizontal: 30.w),
                    child: Row(
                      children: [
                        Container(
                          height: 50.h,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              top: BorderSide(width: 1.h, color: primaryColor),
                              left: BorderSide(width: 1.h, color: primaryColor),
                              right:
                                  BorderSide(width: 1.h, color: primaryColor),
                              bottom:
                                  BorderSide(width: 1.h, color: primaryColor),
                            ),
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(8.w),
                              topLeft: Radius.circular(8.w),
                            ),
                          ),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 30.w),
                              child: Text(
                                'Total number of groups',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18.sp),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: 50.h,
                            decoration: BoxDecoration(
                              color: Colors.black54,
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(8.w),
                                topRight: Radius.circular(8.w),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                snapshot.data!.docs.length.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18.sp),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 45.w),
                    child: Text(
                      'View groups and supports registered by each group',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18.sp),
                    ),
                  ),
                  SizedBox(height: 20.h),
                  filterDropdownButton(),
                  SizedBox(height: 20.h),
                  ...snapshot.data!.docs.map((e) {
                    if (filter == 'All' || filter == null) {
                      return groupRow(e.id);
                    } else {
                      String category = '';
                      try {
                        category = e.get('category');
                      } catch (e) {
                        null;
                      }
                      if (category == filter) {
                        return groupRow(e.id);
                      } else {
                        return Container();
                      }
                    }
                  })
                ],
              ),
            );
          }),
      floatingActionButton: hasGroups
          ? FloatingActionButton.extended(
              backgroundColor: primaryColor,
              onPressed: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => RegisterGroupPage(
                        snapshot: widget.snapshot,
                      ),
                    ),
                  ),
              icon: const Icon(Icons.add),
              label: const Text('New Group'))
          : null,
    );
  }
}
