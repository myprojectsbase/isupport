import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_groups/supports.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:screenshot/screenshot.dart';
import '../../../../utils/constants.dart';

class GroupDetailsPage extends StatefulWidget {
  const GroupDetailsPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<GroupDetailsPage> createState() => _GroupDetailsPageState();
}

class _GroupDetailsPageState extends State<GroupDetailsPage> {
  Widget infoWidget({required String header, required String body}) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$header :',
            style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
          ),
          Text(
            body,
            style: TextStyle(fontSize: 16.sp),
          ),
        ],
      ),
    );
  }

  Widget body() {
    String category = '';
    String candidate = '';

    try {
      category = widget.snapshot.get('category');
    } catch (e) {
      null;
    }
    try {
      candidate = widget.snapshot.get('candidate');
    } catch (e) {
      null;
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 30.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.w),
            child: Text(
              widget.snapshot.get('groupName'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25.h, horizontal: 30.w),
            child: Row(
              children: [
                Container(
                  height: 50.h,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(width: 1.h, color: primaryColor),
                      left: BorderSide(width: 1.h, color: primaryColor),
                      right: BorderSide(width: 1.h, color: primaryColor),
                      bottom: BorderSide(width: 1.h, color: primaryColor),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(8.w),
                      topLeft: Radius.circular(8.w),
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                      child: Text(
                        'Total number of supports (${widget.snapshot.get('supports').length.toString()})',
                        style: TextStyle(color: Colors.black, fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            GroupSupportsPage(snapshot: widget.snapshot),
                      ),
                    ),
                    child: Container(
                      height: 50.h,
                      decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(8.w),
                          topRight: Radius.circular(8.w),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'View',
                          style:
                              TextStyle(color: Colors.white, fontSize: 18.sp),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 30.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20.h),
                infoWidget(
                  header: 'Group Name',
                  body: widget.snapshot.get('groupName'),
                ),
                infoWidget(
                  header: 'Group Category',
                  body: category,
                ),
                infoWidget(
                  header: 'Supporting',
                  body: candidate,
                ),
                infoWidget(
                  header: 'Group Leader',
                  body: widget.snapshot.get('groupLeader'),
                ),
                infoWidget(
                  header: 'Phone',
                  body: widget.snapshot.get('phoneNumber'),
                ),
                infoWidget(
                  header: 'Email',
                  body: widget.snapshot.get('email'),
                ),
                infoWidget(
                  header: 'State',
                  body: widget.snapshot.get('state') ?? '',
                ),
                infoWidget(
                  header: 'Group description',
                  body: widget.snapshot.get('description') ?? '',
                ),
                SizedBox(height: 20.h),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: customAppbar(context, title: 'Group Details', actions: [
        Padding(
          padding: EdgeInsets.only(right: 20.w),
          child: InkWell(
            onTap: () {
              printPage(
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      body(),
                    ],
                  ),
                  documentName: 'group-details.pdf',
                  context: context);
            },
            child: const Icon(Icons.print),
          ),
        )
      ]),
      body: body(),
    );
  }
}
