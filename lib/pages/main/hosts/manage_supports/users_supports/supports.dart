import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/all_supports.dart';
import 'package:isupport/pages/main/user_details.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/supports_details.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class SupportsPage extends StatefulWidget {
  const SupportsPage({Key? key, required this.hostSnapshot}) : super(key: key);
  final DocumentSnapshot hostSnapshot;

  @override
  State<SupportsPage> createState() => _SupportsPageState();
}

class _SupportsPageState extends State<SupportsPage> {
  bool showingByUsers = true;
  String dropdownValue = 'Select filter';
  int numUsefulUsers = 0;
  int numUsefulGroups = 0;

  Widget noSupportsWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 30.h),
        Text(
          "There are currently no supports",
          style: TextStyle(
              fontSize: 26.sp,
              color: Colors.black54,
              fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40.h),
        Text(
          "Supports are added by agents. Please contact any agent to create a support list",
          style: TextStyle(fontSize: 20.sp, color: Colors.black54),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40.h),
        Image.asset('assets/icons/chat_bubble_outline.png'),
        Text(
          "Contact agents",
          style: TextStyle(
              fontSize: 18.sp,
              color: Colors.black54,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget userRow(DocumentSnapshot snapshot, {bool isUser = true}) {
    return Column(
      children: [
        InkWell(
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => SupportsDetailsPage(
                hostUID: widget.hostSnapshot.id,
                userID: snapshot.id,
                isUser: isUser,
                userSnapshot: snapshot,
                fieldToOrderBy: dropdownValue == 'Select filter'
                    ? showingByUsers
                        ? 'fullName'
                        : 'groupName'
                    : dropdownValue == 'LGA'
                        ? 'localGovernmentArea'
                        : dropdownValue.toLowerCase(),
              ),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
            child: Row(
              children: [
                Text(
                  isUser
                      ? snapshot.get('fullName') ?? 'No name given'
                      : snapshot.get('groupName'),
                  style: TextStyle(fontSize: 18.sp, color: Colors.black54),
                ),
                const Spacer(),
                const Icon(
                  Icons.arrow_forward_ios,
                  // size: 16,
                  color: Colors.black54,
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 1.h,
          color: Colors.grey,
        )
      ],
    );
  }

  Widget allSupportsButton({required bool isUser}) {
    return Column(
      children: [
        InkWell(
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => AllSupportsPage(
                hostUID: widget.hostSnapshot.id,
                isUser: isUser,
                fieldToOrderBy: dropdownValue == 'Select filter'
                    ? showingByUsers
                        ? 'fullName'
                        : 'groupName'
                    : dropdownValue.toLowerCase(),
              ),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
            child: Row(
              children: [
                Text(
                  isUser ? 'All Agents' : 'All Groups',
                  style: TextStyle(fontSize: 18.sp, color: Colors.black),
                ),
                const Spacer(),
                const Icon(
                  Icons.arrow_forward_ios,
                  // size: 16,
                  color: Colors.black54,
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 1.h,
          color: Colors.grey,
        )
      ],
    );
  }

  Widget selectedTab({required String content}) {
    double width = MediaQuery.of(context).size.width / 2;
    return Column(
      children: [
        Container(
          color: Colors.grey,
          width: width,
          padding: EdgeInsets.symmetric(vertical: 9.5.h),
          child: Center(
            child: Text(
              content,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.sp,
                  color: Colors.white),
            ),
          ),
        ),
        Container(
          height: 5.h,
          width: width,
          color: lightGreen,
        )
      ],
    );
  }

  Widget unselectedTab({required String content}) {
    double width = MediaQuery.of(context).size.width / 2;
    return Container(
      color: Colors.grey[200],
      width: width,
      padding: EdgeInsets.symmetric(vertical: 12.h),
      child: Center(
        child: Text(
          content,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 18.sp, color: Colors.grey),
        ),
      ),
    );
  }

  Widget supportsByUsers(List<QueryDocumentSnapshot<Object?>> usefulUsers) {
    return usefulUsers.isEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.h),
            child: Text(
              'You do not have any support registered by any agents yet.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18.sp),
            ),
          )
        : Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Column(
              children: [
                allSupportsButton(isUser: true),
                ...usefulUsers.map((e) => userRow(e)).toList()
              ],
            ),
          );
  }

  Widget supportsByGroups(List<QueryDocumentSnapshot<Object?>> usefulGroups) {
    return usefulGroups.isEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.h),
            child: Text(
              'You do not have any support registered by any group yet.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18.sp),
            ),
          )
        : Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Column(
              children: [
                allSupportsButton(isUser: false),
                ...usefulGroups.map((e) => userRow(e, isUser: false)).toList()
              ],
            ),
          );
    ;
  }

  Widget filterDropdownButton() {
    return Center(
      child: roundedContainer(
        buttonColor: Colors.black54,
        radius: 0,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          child: DropdownButton<String>(
            isDense: true,
            // remove underline
            underline: Container(),

            // Initial Value
            value: dropdownValue,

            // Down Arrow Icon
            icon: Padding(
              padding: EdgeInsets.only(right: 10.w),
              child: const Icon(CupertinoIcons.chevron_down),
            ),

            // Array list of items
            items: ['Select filter', 'State', 'Ward', 'Unit', 'LGA', 'Gender']
                .map((String item) {
              return DropdownMenuItem(
                value: item,
                child: Container(
                  padding: EdgeInsets.only(left: 30.w, right: 70.w),
                  child: Text(item,
                      style: TextStyle(fontSize: 21.sp, color: Colors.black54)),
                ),
              );
            }).toList(),
            // After selecting the desired option,it will
            // change button value to selected value
            onChanged: (String? newValue) {
              setState(() {
                dropdownValue = newValue!;
              });
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Supports"),
      body: widget.hostSnapshot.get('supports').length > 0
          ? SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 30.h),
                  Text(
                    "Total Supports (${widget.hostSnapshot.get('supports').length})",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.sp,
                        color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20.h),
                  const Text("Supporters are organized by agents."),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: const Text(
                      "Tap on an agent to view support list registered by selected agent",
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 25.h),
                    child: const Text(
                      "You can use the filter drop down to display list according to agents name, location, ward, LGA, unit and state",
                      textAlign: TextAlign.center,
                    ),
                  ),
                  filterDropdownButton(),
                  SizedBox(height: 25.h),
                  StreamBuilder(
                      stream: DatabaseService()
                          .hostUsersSnapshots(userID: widget.hostSnapshot.id),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
                        return StreamBuilder(
                            stream: DatabaseService().hostGroupsSnapshots(
                                hostUID: widget.hostSnapshot.id),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot<Object?>>
                                    groupsSnapshot) {
                              if (!snapshot.hasData ||
                                  !groupsSnapshot.hasData) {
                                return const Center(
                                    child: CircularProgressIndicator());
                              }
                              List<QueryDocumentSnapshot<Object?>> usefulUsers =
                                  snapshot.data!.docs
                                      .where((element) =>
                                          (element.get('supports') as List)
                                              .isNotEmpty)
                                      .toList();
                              numUsefulUsers = usefulUsers.length;
                              List<QueryDocumentSnapshot<Object?>>
                                  usefulGroups = groupsSnapshot.data!.docs
                                      .where((element) =>
                                          (element.get('supports') as List)
                                              .isNotEmpty)
                                      .toList();
                              numUsefulUsers = usefulUsers.length;
                              numUsefulGroups = usefulGroups.length;
                              return Column(
                                children: [
                                  Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          if (!showingByUsers) {
                                            setState(() {
                                              showingByUsers = true;
                                            });
                                          }
                                        },
                                        child: showingByUsers
                                            ? selectedTab(
                                                content:
                                                    'Agents ($numUsefulUsers)')
                                            : unselectedTab(
                                                content:
                                                    'Agents ($numUsefulUsers)'),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          if (showingByUsers) {
                                            setState(() {
                                              showingByUsers = false;
                                            });
                                          }
                                        },
                                        child: showingByUsers
                                            ? unselectedTab(
                                                content:
                                                    'Group ($numUsefulGroups)')
                                            : selectedTab(
                                                content:
                                                    'Group ($numUsefulGroups)'),
                                      ),
                                    ],
                                  ),
                                  showingByUsers
                                      ? supportsByUsers(usefulUsers)
                                      : supportsByGroups(usefulGroups),
                                ],
                              );
                            });
                      })
                ],
              ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: noSupportsWidget(),
            ),
    );
  }
}
