import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/user_details.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class SupportsDetailsPage extends StatefulWidget {
  const SupportsDetailsPage(
      {Key? key,
      required this.hostUID,
      required this.userID,
      this.isUser = true,
      required this.fieldToOrderBy,
      required this.userSnapshot})
      : super(key: key);
  final String hostUID;
  final String userID;
  final bool isUser;
  final DocumentSnapshot userSnapshot;
  final String fieldToOrderBy;

  @override
  State<SupportsDetailsPage> createState() => _SupportsDetailsPageState();
}

class _SupportsDetailsPageState extends State<SupportsDetailsPage> {
  Stream<QuerySnapshot<Object?>>? stream;
  TextEditingController searchController = TextEditingController();
  bool pageLoaded = false;
  bool isSearching = false;
  String currentHeaderValue = '';
  String? fieldValueToShow = '';
  String firstItemUnderCurrentHeader = '';
  Widget supporterRow(String supporterUID) {
    return FutureBuilder(
        future: DatabaseService().getSupporterData(supporterUID: supporterUID),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          try {
            if (widget.fieldToOrderBy != 'fullName' &&
                widget.fieldToOrderBy != 'groupName') {
              if (snapshot.data!.get(widget.fieldToOrderBy) !=
                      fieldValueToShow ||
                  snapshot.data!.get(widget.fieldToOrderBy) == '') {
                return Container();
              }
            }
            return Column(
              children: [
                InkWell(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => UserDetails(
                        userData: snapshot.data!,
                        isSupport: true,
                      ),
                    ),
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
                    child: Row(
                      children: [
                        Text(
                          snapshot.data!.get('fullName'),
                          style:
                              TextStyle(fontSize: 16.sp, color: Colors.black54),
                        ),
                        const Spacer(),
                        const Icon(
                          Icons.arrow_forward_ios,
                          // size: 16,
                          color: Colors.black54,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(height: 1.h, color: Colors.grey)
              ],
            );
          } catch (e) {
            print(e);
            return Container();
          }
        });
  }

  Widget headerWidget(String title) {
    return InkWell(
      onTap: () {
        if (title != fieldValueToShow) {
          setState(() {
            fieldValueToShow = title;
          });
        } else {
          setState(() {
            fieldValueToShow = '';
          });
        }
      },
      child: title.isEmpty
          ? Container()
          : Container(
              height: 40.h,
              color: Colors.grey.shade100,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      title.isEmpty ? 'Unassigned' : title,
                      style: TextStyle(
                          fontSize: 18.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Icon(
                      fieldValueToShow == title
                          ? Icons.arrow_drop_up
                          : Icons.arrow_drop_down,
                      size: 36.w,
                      color: Colors.black54,
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget userInfo() {
    String displayName = '';
    String nameField = widget.isUser ? 'fullName' : 'groupName';
    String state = widget.userSnapshot.get('state') ?? '';
    String unit = '';
    String ward = '';
    if (widget.userSnapshot.get(nameField) == null) {
      displayName = widget.userSnapshot.get('phoneNumber');
    } else {
      displayName = widget.userSnapshot.get(nameField);
    }
    try {
      unit = widget.userSnapshot.get('unit');
    } catch (e) {
      null;
    }
    try {
      ward = widget.userSnapshot.get('ward');
    } catch (e) {
      null;
    }
    return Padding(
      padding: EdgeInsets.only(left: 20.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20.h),
          const Text('Showing supports registered by:'),
          SizedBox(height: 5.h),
          Text(
            displayName,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 5.h),
          Text('State: ' + state),
          SizedBox(height: 5.h),
          Text('Unit: ' + unit),
          SizedBox(height: 5.h),
          Text('Wards: ' + ward),
          SizedBox(height: 5.h),
        ],
      ),
    );
  }

  Widget searchResultsWidget(
      {required List<QueryDocumentSnapshot<Object?>> docs}) {
    Iterable<QueryDocumentSnapshot<Object?>> searchResults =
        docs.where((element) {
      if (element.get('fullName') == null) {
        return false;
      } else {
        if ((element.get('fullName') as String)
            .toLowerCase()
            .contains(searchController.text.trim().toLowerCase())) {
          return true;
        } else {
          return false;
        }
      }
    });
    return Container(
      color: Colors.white,
      child: searchResults.isNotEmpty
          ? Column(
              children: searchResults
                  .map(
                    (e) => supporterRow(e.id),
                  )
                  .toList(),
            )
          : const Center(child: Text('No results')),
    );
  }

  @override
  void initState() {
    if (!pageLoaded) {
      stream = widget.isUser
          ? DatabaseService().hostSupportsSnapshots(
              hostUID: widget.hostUID,
              userID: widget.userID,
              fieldToOrderBy: widget.fieldToOrderBy)
          : DatabaseService().groupSupportsSnapshots(groupID: widget.userID);
      pageLoaded = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Supports"),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            userInfo(),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
              child: Text(
                'Registered Supports: (${widget.userSnapshot.get('supports').length})',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp),
              ),
            ),
            StreamBuilder(
                stream: stream,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
                  if (!snapshot.hasData) {
                    return Container();
                  }
                  return Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(
                              left: 20.w, bottom: 5.h, top: 5.h),
                          alignment: Alignment.centerLeft,
                          child: const Text('Use search to find supports')),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        child: roundedContainer(
                          child: textFieldPadding(
                              padding: EdgeInsets.symmetric(horizontal: 20.w),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: SizedBox(
                                      height: 50.h,
                                      child: Center(
                                        child: TextField(
                                          onChanged: (value) {
                                            if (value.isEmpty) {
                                              setState(() {
                                                isSearching = false;
                                              });
                                            } else {
                                              setState(() {
                                                isSearching = true;
                                              });
                                            }
                                          },
                                          controller: searchController,
                                          decoration: customInputDecoration(
                                              isDense: true,
                                              hintText: 'Search'),
                                        ),
                                      ),
                                    ),
                                  ),
                                  isSearching
                                      ? InkWell(
                                          onTap: () {
                                            searchController.text = '';
                                            setState(() {
                                              isSearching = false;
                                            });
                                          },
                                          child: const Icon(Icons.cancel,
                                              color: Colors.grey),
                                        )
                                      : const Icon(Icons.search,
                                          color: Colors.black54)
                                ],
                              ),
                              buttonColor: Colors.transparent),
                          buttonColor: textFieldBlue,
                        ),
                      ),
                      SingleChildScrollView(
                        child: Stack(
                          children: [
                            Column(
                              children: [
                                SizedBox(height: 20.h),
                                Container(
                                  height: 40.h,
                                  color: Colors.grey[300],
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Supports",
                                        style: TextStyle(
                                            fontSize: 18.sp,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black54),
                                      ),
                                    ],
                                  ),
                                ),
                                ...snapshot.data!.docs.map((e) {
                                  String? fieldValue;
                                  try {
                                    fieldValue = e.get(widget.fieldToOrderBy);
                                  } catch (e) {
                                    print(e);
                                    null;
                                  }
                                  if (widget.fieldToOrderBy == 'fullName' ||
                                      widget.fieldToOrderBy == 'groupName') {
                                    return supporterRow(e.id);
                                  } else {
                                    if (fieldValue == null ||
                                        fieldValue == '') {
                                      print("AYE YOO");
                                      return Container();
                                    } else {
                                      if (fieldValue == currentHeaderValue &&
                                          firstItemUnderCurrentHeader !=
                                              e.get(widget.isUser
                                                  ? 'fullName'
                                                  : 'groupName')) {
                                        return supporterRow(e.id);
                                      } else {
                                        currentHeaderValue = fieldValue;
                                        firstItemUnderCurrentHeader = e.get(
                                            widget.isUser
                                                ? 'fullName'
                                                : 'groupName');
                                        return Column(
                                          children: [
                                            headerWidget(fieldValue),
                                            supporterRow(e.id),
                                          ],
                                        );
                                      }
                                    }
                                  }
                                })
                              ],
                            ),
                            if (isSearching)
                              Positioned.fill(
                                  child: searchResultsWidget(
                                      docs: snapshot.data!.docs))
                          ],
                        ),
                      ),
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }
}
