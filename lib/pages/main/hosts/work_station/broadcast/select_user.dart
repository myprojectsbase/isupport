import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/supports_details.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/utilities.dart';
import '../../../../../services/database.dart';

class SelectUserForBroadcast extends StatefulWidget {
  const SelectUserForBroadcast(
      {Key? key, required this.hostSnapshot, required this.message})
      : super(key: key);
  final DocumentSnapshot hostSnapshot;
  final String message;

  @override
  State<SelectUserForBroadcast> createState() => _SelectUserForBroadcastState();
}

class _SelectUserForBroadcastState extends State<SelectUserForBroadcast> {
  int numUsefulUsers = 0;
  List<String> phoneNumbers = [];

  Widget noSupportsWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 30.h),
        Text(
          "There are currently no supports",
          style: TextStyle(
              fontSize: 26.sp,
              color: Colors.black54,
              fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40.h),
        Text(
          "Supports are added by users. Please contact any user to create a support list",
          style: TextStyle(fontSize: 20.sp, color: Colors.black54),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40.h),
        Image.asset('assets/icons/chat_bubble_outline.png'),
        Text(
          "Contact users",
          style: TextStyle(
              fontSize: 18.sp,
              color: Colors.black54,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget userRow(DocumentSnapshot userDocument) {
    return InkWell(
      onTap: () async {
        customScaffoldMessage('Compiling phone numbers...', context,
            duration: const Duration(hours: 1));
        phoneNumbers = await DatabaseService().getSupportsPhoneNumbers(
            supportsList: userDocument.get('supports'));
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        bulkSMSConfirmationDialog(
          context,
          phoneNumbers: phoneNumbers,
          senderName: widget.hostSnapshot.get('fullName'),
          message: widget.message,
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
            child: Text(
              userDocument.get('fullName') ?? 'No name given',
              style: TextStyle(fontSize: 18.sp, color: Colors.black),
            ),
          ),
          Container(
            height: 1.h,
            color: Colors.grey,
          )
        ],
      ),
    );
  }

  Widget greyHeaderWidget({required String content}) {
    return Container(
      color: Colors.grey[200],
      height: 50.h,
      padding: EdgeInsets.symmetric(vertical: 12.h),
      child: Center(
        child: Text(
          content,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget supportsByUsers(List<QueryDocumentSnapshot<Object?>> usefulUsers) {
    return usefulUsers.isEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.h),
            child: Text(
              'You do not have any support registered by any users yet.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18.sp),
            ),
          )
        : Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Column(
              children: usefulUsers.map((e) => userRow(e)).toList(),
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Send Broadcast"),
      body: widget.hostSnapshot.get('supports').length > 0
          ? SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.w, vertical: 25.h),
                    child: Text(
                      "To send a broadcast to a list of supporters registered by a specific user, select the name of the user from the list.",
                      style: TextStyle(fontSize: 16.sp),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  StreamBuilder(
                      stream: DatabaseService()
                          .hostUsersSnapshots(userID: widget.hostSnapshot.id),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                              child: CircularProgressIndicator());
                        }
                        List<QueryDocumentSnapshot<Object?>> usefulUsers =
                            snapshot.data!.docs
                                .where((element) =>
                                    (element.get('supports') as List)
                                        .isNotEmpty)
                                .toList();
                        numUsefulUsers = usefulUsers.length;
                        return Column(
                          children: [
                            greyHeaderWidget(
                                content: 'Users (Total: $numUsefulUsers)'),
                            supportsByUsers(usefulUsers)
                          ],
                        );
                      })
                ],
              ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: noSupportsWidget(),
            ),
    );
  }
}
