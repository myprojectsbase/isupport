import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import '../../../../../services/database.dart';
import '../../../../../utils/utilities.dart';

class SelectGroupForBroadcast extends StatefulWidget {
  const SelectGroupForBroadcast(
      {Key? key, required this.hostSnapshot, required this.message})
      : super(key: key);
  final DocumentSnapshot hostSnapshot;
  final String message;

  @override
  State<SelectGroupForBroadcast> createState() =>
      _SelectGroupForBroadcastState();
}

class _SelectGroupForBroadcastState extends State<SelectGroupForBroadcast> {
  int numUsefulGroups = 0;

  Widget noSupportsWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 30.h),
        Text(
          "There are currently no supports",
          style: TextStyle(
              fontSize: 26.sp,
              color: Colors.black54,
              fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40.h),
        Text(
          "Supports are added by Groups. Please contact any Group to create a support list",
          style: TextStyle(fontSize: 20.sp, color: Colors.black54),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40.h),
        Image.asset('assets/icons/chat_bubble_outline.png'),
        Text(
          "Contact Groups",
          style: TextStyle(
              fontSize: 18.sp,
              color: Colors.black54,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget groupRow(DocumentSnapshot groupDocument) {
    return InkWell(
      onTap: () async {
        customScaffoldMessage('Compiling phone numbers...', context,
            duration: const Duration(hours: 1));
        List<String> phoneNumbers = await DatabaseService()
            .getSupportsPhoneNumbers(
                supportsList: groupDocument.get('supports'));
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        bulkSMSConfirmationDialog(
          context,
          phoneNumbers: phoneNumbers,
          senderName: widget.hostSnapshot.get('fullName'),
          message: widget.message,
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
            child: Text(
              groupDocument.get('groupName') ?? 'No name given',
              style: TextStyle(fontSize: 18.sp, color: Colors.black),
            ),
          ),
          Container(
            height: 1.h,
            color: Colors.grey,
          )
        ],
      ),
    );
  }

  Widget greyHeaderWidget({required String content}) {
    return Container(
      color: Colors.grey[200],
      height: 50.h,
      padding: EdgeInsets.symmetric(vertical: 12.h),
      child: Center(
        child: Text(
          content,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget supportsByGroups(List<QueryDocumentSnapshot<Object?>> usefulGroups) {
    return usefulGroups.isEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.h),
            child: Text(
              'You do not have any support registered by any group yet.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18.sp),
            ),
          )
        : Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Column(
              children: usefulGroups.map((e) => groupRow(e)).toList(),
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Send Broadcast"),
      body: widget.hostSnapshot.get('supports').length > 0
          ? SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.w, vertical: 25.h),
                    child: Text(
                      "To send a broadcast to a list of supporters registered by a specific group, select the name of the group from the list.",
                      style: TextStyle(fontSize: 16.sp),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  StreamBuilder(
                      stream: DatabaseService()
                          .hostGroupsSnapshots(hostUID: widget.hostSnapshot.id),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot<Object?>>
                              groupsSnapshot) {
                        if (!groupsSnapshot.hasData) {
                          return const Center(
                              child: CircularProgressIndicator());
                        }
                        List<QueryDocumentSnapshot<Object?>> usefulGroups =
                            groupsSnapshot.data!.docs
                                .where((element) =>
                                    (element.get('supports') as List)
                                        .isNotEmpty)
                                .toList();
                        numUsefulGroups = usefulGroups.length;
                        return Column(
                          children: [
                            greyHeaderWidget(
                                content: 'Groups (Total: $numUsefulGroups)'),
                            supportsByGroups(usefulGroups)
                          ],
                        );
                      })
                ],
              ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: noSupportsWidget(),
            ),
    );
  }
}
