import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/work_station/dropbox/chatrooms.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

class DropboxPage extends StatefulWidget {
  const DropboxPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot<Object?> snapshot;

  @override
  State<DropboxPage> createState() => _DropboxPageState();
}

class _DropboxPageState extends State<DropboxPage> {
  String selected = 'All iChats';
  Widget selectedContainer({required String text}) {
    return InkWell(
      onTap: () => setState(() {
        selected = text;
      }),
      child: Column(
        children: [
          Container(
            height: 35.h,
            color: darkColor,
            child: Center(
              child: Text(
                text,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            height: 5.h,
            color: lightGreen,
          )
        ],
      ),
    );
  }

  Widget unselectedContainer({required String text}) {
    return InkWell(
      onTap: () => setState(() {
        selected = text;
      }),
      child: Container(
        height: 40.h,
        color: darkColor,
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white60,
              fontSize: 18.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  Widget isSelectedChooser({required String text}) {
    if (text == selected) {
      return selectedContainer(text: text);
    } else {
      return unselectedContainer(text: text);
    }
  }

  Widget dropboxList(List<QueryDocumentSnapshot<Object?>> docs) {
    docs = docs.where((element) {
      int numUnreadMessages = element.get(widget.snapshot.id + "_unseen");
      if (selected == 'Read') {
        if (numUnreadMessages == 0) {
          return true;
        }
        return false;
      } else if (selected == 'Unread') {
        if (numUnreadMessages > 0) {
          return true;
        }
        return false;
      } else {
        return true;
      }
    }).toList();
    return ChatroomsPage(
      docs: docs,
      hostID: widget.snapshot.id,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'iChat'),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 15.w,
              right: 15.w,
              top: 15.h,
              bottom: 30.h,
            ),
            color: darkColor,
            child: Row(children: [
              Expanded(
                child: roundedContainer(
                  radius: 0,
                  buttonColor: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.all(10.w),
                    child: Text(
                      'iChat allows supports to send you messages. It keeps you in touch with the people',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 14.sp),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10.w),
              InkWell(
                onTap: () => customShowDialog(
                    context,
                    Padding(
                      padding:
                          EdgeInsets.only(left: 20.w, right: 20.w, top: 30.h),
                      child: const Text(
                          'iChat allows supports to send you messages. It keeps you in touch with the people. You can only reply to messages rom supports, however if you want to send a personal message to any support, please use the messaging feature to send direct SMS.'),
                    ),
                    actions: [
                      InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20.h, right: 30.w),
                          child: Text(
                            'OK',
                            style: TextStyle(
                                color: Colors.blue,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ]),
                child: const Icon(
                  Icons.info_outline,
                  color: Colors.white,
                ),
              )
            ]),
          ),
          Flex(
            direction: Axis.horizontal,
            children: [
              Flexible(
                flex: 1,
                child: isSelectedChooser(text: 'All iChats'),
              ),
              Flexible(
                flex: 1,
                child: isSelectedChooser(text: 'Read'),
              ),
              Flexible(
                flex: 1,
                child: isSelectedChooser(text: 'Unread'),
              ),
            ],
          ),
          Expanded(
            child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('Dropboxes')
                    .where('members', arrayContains: widget.snapshot.id)
                    .orderBy('lastMessageTime', descending: true)
                    .snapshots(),
                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) {
                    return Container();
                  }
                  if (snapshot.data!.docs.isEmpty) {
                    return Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 60.h, horizontal: 40.w),
                      child: Text(
                        'iChat is empty',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 21.sp,
                          color: Colors.black54,
                        ),
                      ),
                    );
                  }
                  return dropboxList(snapshot.data!.docs);
                }),
          )
        ],
      ),
    );
  }
}
