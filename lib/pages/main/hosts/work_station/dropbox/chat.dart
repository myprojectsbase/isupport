import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';

class ChatPage extends StatefulWidget {
  const ChatPage(
      {Key? key,
      required this.title,
      required this.id,
      required this.peerId,
      required this.roomId,
      required this.senderIsHost})
      : super(key: key);

  final String title;
  final String id;
  final String peerId;
  final String roomId;
  final bool senderIsHost;

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
//  DONT FORGET TO CHANGE BACK THE FIRESTORE RULES!!

  List listMessage = [];
  ScrollController con = ScrollController();
  String title = 'Long List';
  String prevTitle = '';
  List items = [];
  List duplicateItems = [];
  TextEditingController textController = TextEditingController();
  ScrollController listScrollController = ScrollController();
  final itemSize = 80.0;
  final TextEditingController textEditingController = TextEditingController();
  final FocusNode focusNode = FocusNode();
  String senderName = '';
  List<File> filesPicked = <File>[];
  bool uploadingImage = false;

  void setSenderName() async {
    senderName = widget.senderIsHost
        ? await DatabaseService()
            .getSpecificHostData('fullName', hostUID: widget.id)
        : await DatabaseService()
                .getSpecificSupportData('fullName', supportID: widget.id) ??
            'No name given';
  }

  @override
  void initState() {
    super.initState();
    items = listMessage;
    duplicateItems = List.from(items);
    textController = TextEditingController();
    setSenderName();
    prevTitle = title;
    con = ScrollController();
    con.addListener(() {
      if (con.offset >= con.position.maxScrollExtent &&
          !con.position.outOfRange) {
        setState(() {
          title = "reached the bottom";
        });
      } else if (con.offset <= con.position.minScrollExtent &&
          !con.position.outOfRange) {
        setState(() {
          title = "reached the top";
        });
      } else {
        setState(() {
          title = prevTitle;
        });
      }
    });
  }

  void setUnseenToZero() async {
    FirebaseFirestore.instance
        .collection('Dropboxes')
        .doc(widget.roomId)
        .update({widget.id + "_unseen": 0});
  }

  Widget buildItem(int index, DocumentSnapshot snapshot) {
    CrossAxisAlignment alignment;
    late BorderRadius borderRadius = BorderRadius.all(Radius.circular(20.w));
    Color? messageBackgroundColor;
    Color messageFontColor;
    Timestamp timestamp;
    double halfWidth = MediaQuery.of(context).size.width / 2;
    if (snapshot.get('timestamp') is Timestamp) {
      timestamp = snapshot.get('timestamp');
    } else {
      timestamp = Timestamp.fromDate(DateTime.parse(snapshot.get('timestamp')));
    }
    if (snapshot.get('sender_id') == widget.peerId) {
      alignment = CrossAxisAlignment.start;
      messageBackgroundColor = Colors.grey[300];
      messageFontColor = Colors.black;
      setUnseenToZero();
    } else {
      alignment = CrossAxisAlignment.end;
      messageBackgroundColor = const Color(0xff48DF3B);
      messageFontColor = Colors.black;
    }
    return Column(
      crossAxisAlignment: alignment,
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(right: 10.w),
          child: Text(
            Jiffy(timestamp.toDate()).format("EEE,MMM d,yy | h:mm a"),
            style: TextStyle(fontSize: 12.sp),
          ),
        ),
        SizedBox(height: 5.h),
        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width / 1.5,
          ),
          padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 15.w),
          margin: EdgeInsets.only(left: 5.w, right: 5.w),
          // height: 30,
          decoration: BoxDecoration(
              color: messageBackgroundColor, borderRadius: borderRadius),
          child: Column(
            crossAxisAlignment: alignment,
            children: [
              if (snapshot.get('msgPhoto') != '')
                Padding(
                  padding: EdgeInsets.only(bottom: 5.h),
                  child: InkWell(
                    onTap: () => customShowDialog(
                        context,
                        Image.network(
                          snapshot.get('msgPhoto'),
                          width: halfWidth,
                        )),
                    child: CachedNetworkImage(
                      imageUrl: snapshot.get('msgPhoto'),
                      placeholder: (context, string) => SizedBox(
                          height: halfWidth,
                          width: halfWidth,
                          child: const Center(
                              child: CupertinoActivityIndicator())),
                      width: halfWidth,
                    ),
                  ),
                ),
              if (snapshot.get('text') != '')
                Text(
                  snapshot.get('text'),
                  style: TextStyle(color: messageFontColor),
                ),
            ],
          ),
        ),
        SizedBox(height: 10.h)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: widget.title),
      body: Center(
        child: Stack(children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 75.w),
            child: StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('Dropboxes')
                  .doc(widget.roomId)
                  .collection("messages")
                  .orderBy("timestamp", descending: true)
                  // .limit(20)
                  .snapshots(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  listMessage = snapshot.data!.docs;
                  return ListView.builder(
                    padding: EdgeInsets.all(10.h),
                    itemBuilder: (context, index) =>
                        buildItem(index, snapshot.data!.docs[index]),
                    itemCount: snapshot.data!.docs.length,
                    reverse: true,
                    controller: listScrollController,
                  );
                }
              },
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.w),
            alignment: Alignment.bottomCenter,
            child: ChatInputWidget(
              focusNode: focusNode,
              isDropbox: true,
              senderName: senderName,
              id: widget.id,
              peerId: widget.peerId,
              listScrollController: listScrollController,
              senderIsHost: widget.senderIsHost,
              roomId: widget.roomId,
            ),
          )
        ]),
      ),
    );
  }
}
