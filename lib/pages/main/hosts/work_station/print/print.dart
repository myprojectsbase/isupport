import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_agents/users.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/all_supports.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/supports.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

class PrintPage extends StatefulWidget {
  const PrintPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<PrintPage> createState() => _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {
  Widget optionItem(String title, {Widget? nextWidget, Function()? onTap}) {
    return Column(
      children: [
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
            child: Row(
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 18.sp,
                    color: Colors.black54,
                  ),
                ),
                const Spacer(),
                nextWidget ??
                    const Icon(
                      Icons.arrow_forward_ios,
                      // size: 16,
                      color: Colors.black54,
                    ),
              ],
            ),
          ),
        ),
        Container(
          height: 1.h,
          color: Colors.grey,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Print Data'),
      body: SingleChildScrollView(
          child: Column(
        children: [
          SizedBox(height: 30.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30.w),
            child: Text(
              'Search allows you to easily find people and places through their location, personal data like name, state, LGA, ward and unit',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.sp,
                color: darkColor,
              ),
            ),
          ),
          SizedBox(height: 10.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: roundedContainer(
              child: textFieldPadding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 50.h,
                          child: Center(
                            child: TextField(
                              decoration: customInputDecoration(
                                hintText: 'Search',
                                isDense: true,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const Icon(Icons.search, color: Colors.black54)
                    ],
                  )),
              buttonColor: Colors.blue.withOpacity(0.5),
            ),
          ),
          SizedBox(height: 25.h),
          Text(
            'What do you want to print?',
            style: TextStyle(
              fontSize: 22.sp,
              color: darkColor,
            ),
          ),
          SizedBox(height: 10.h),
          Text(
            'You can print all data supplied by supports',
            style: TextStyle(
              fontSize: 16.sp,
              color: darkColor,
            ),
          ),
          SizedBox(height: 25.h),
          Container(
            width: double.infinity,
            height: 50,
            color: Colors.grey[300],
            child: Center(
              child: Text(
                'Supports',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.sp,
                  color: Colors.black54,
                ),
              ),
            ),
          ),
          optionItem(
            'All supports data',
            nextWidget: Text(
              widget.snapshot.get('supports').length.toString(),
              style: TextStyle(
                fontSize: 18.sp,
                color: Colors.black54,
              ),
            ),
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => AllSupportsPage(
                    hostUID: widget.snapshot.id,
                    isUser: true,
                    fieldToOrderBy: 'fullName'),
              ),
            ),
          ),
          // optionItem('Support based on location'),
          optionItem(
            'Support based on LGA',
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => AllSupportsPage(
                    hostUID: widget.snapshot.id,
                    isUser: true,
                    fieldToOrderBy: 'localGovernmentArea'),
              ),
            ),
          ),
          optionItem(
            "Support based on user's list",
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    SupportsPage(hostSnapshot: widget.snapshot),
              ),
            ),
          ),
          optionItem(
            'Support based on unit',
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => AllSupportsPage(
                    hostUID: widget.snapshot.id,
                    isUser: true,
                    fieldToOrderBy: 'unit'),
              ),
            ),
          ),
          optionItem(
            'Support based on ward',
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => AllSupportsPage(
                    hostUID: widget.snapshot.id,
                    isUser: true,
                    fieldToOrderBy: 'ward'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h),
            child: Text(
              "You can print all data regarding user's profile",
              style: TextStyle(
                fontSize: 16.sp,
                color: darkColor,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 50,
            color: Colors.grey[300],
            child: Center(
              child: Text(
                'Agents',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.sp,
                  color: Colors.black54,
                ),
              ),
            ),
          ),
          optionItem(
            "All agents data",
            nextWidget: Container(),
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => UsersPage(
                  hostName: widget.snapshot.get('fullName'),
                  uid: widget.snapshot.id,
                ),
              ),
            ),
          ),
          // optionItem('Agents based on unit'),
          // optionItem('Single Agents data'),
        ],
      )),
    );
  }
}
