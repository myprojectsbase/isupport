import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:isupport/pages/main/chat/chatrooms.dart';
import 'package:isupport/pages/main/hosts/manage_groups/groups.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/all_supports.dart';
import 'package:isupport/pages/main/hosts/work_station/analytics.dart';
import 'package:isupport/pages/main/hosts/work_station/broadcast/broadcast.dart';
import 'package:isupport/pages/main/hosts/work_station/dropbox/dropbox.dart';
import 'package:isupport/pages/main/hosts/work_station/print/print.dart';
import 'package:isupport/pages/main/hosts/work_station/send_message.dart';
import 'package:isupport/pages/main/hosts/work_station/situation_room/situation_room.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import 'package:isupport/utils/utilities.dart';

import '../../help.dart';
import '../manage_agents/users.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/supports.dart';

class WorkStationPage extends StatefulWidget {
  const WorkStationPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<WorkStationPage> createState() => _WorkStationPageState();
}

class _WorkStationPageState extends State<WorkStationPage> {
  Widget itemWidget(
      {required String title, required Widget child, Function()? onTap}) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            height: 100.w,
            width: 100.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.w),
              border: Border.all(width: 1.5.w, color: lightGreen),
            ),
            child: Center(child: child),
          ),
          SizedBox(height: 5.h),
          Text(
            title,
            style: TextStyle(fontSize: 14.sp),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
      appBar: customAppbar(context, title: 'Work Station'),
      child: ListView(
        primary: true,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: 20.w, right: 20.w, top: 20.h, bottom: 20.h),
            child: Text(
                'Search allows you to easily find people and places through their personal data like name, state, location and many more',
                style: TextStyle(fontSize: 14.sp, color: Colors.black),
                textAlign: TextAlign.center),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => AllSupportsPage(
                  hostUID: widget.snapshot.id,
                  isUser: true,
                  isSearch: true,
                  fieldToOrderBy: 'fullName',
                ),
              )),
              child: roundedContainer(
                child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20.w,
                      vertical: 10.w,
                    ),
                    child: Row(
                      children: [
                        Text(
                          'Search',
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16.sp,
                          ),
                        ),
                        const Spacer(),
                        const Icon(Icons.search, color: Colors.black54)
                      ],
                    )),
                buttonColor: textFieldBlue,
              ),
            ),
          ),
          SizedBox(height: 25.h),
          Center(
            child: Text('Work Station Tools',
                style: TextStyle(fontSize: 20.sp, color: Colors.black),
                textAlign: TextAlign.center),
          ),
          SizedBox(height: 25.h),
          Center(
            child: Wrap(
              runSpacing: 30.h,
              // alignment: WrapAlignment.end,
              spacing: 20.w,
              children: [
                itemWidget(
                  title: 'Total Agents',
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 3.h),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[400], shape: BoxShape.circle),
                        child: Padding(
                          padding: EdgeInsets.all(5.w),
                          child: Icon(
                            Icons.person,
                            size: 25.w,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(height: 5.h),
                      FittedBox(
                        child: Text(
                          formattedNumber(widget.snapshot.get('users').length),
                          style: TextStyle(
                              fontSize: 30.sp, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => UsersPage(
                        uid: widget.snapshot.id,
                        hostName: widget.snapshot.get('fullName')),
                  )),
                ),
                itemWidget(
                  title: 'Total Supports',
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[400], shape: BoxShape.circle),
                        child: Padding(
                          padding: EdgeInsets.all(5.w),
                          child: Icon(
                            Icons.people,
                            size: 25.w,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(height: 5.h),
                      FittedBox(
                        child: Text(
                            formattedNumber(
                                widget.snapshot.get('supports').length),
                            style: TextStyle(
                                fontSize: 30.sp, fontWeight: FontWeight.bold)),
                      )
                    ],
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          SupportsPage(hostSnapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Group',
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[400], shape: BoxShape.circle),
                    child: Padding(
                      padding: EdgeInsets.all(8.w),
                      child: Icon(
                        Icons.groups,
                        size: 30.w,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          GroupsPage(snapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Print Data',
                  child: Icon(
                    Icons.print,
                    size: 40.w,
                    color: Colors.black54,
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          PrintPage(snapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Send SMS',
                  child: Icon(
                    Icons.send,
                    size: 40.w,
                    color: Colors.black54,
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          SendMessagePage(snapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Send Broadcast',
                  child: Icon(
                    Icons.wifi_tethering,
                    size: 40.w,
                    color: Colors.black54,
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          BroadcastPage(snapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Inbox',
                  child: SizedBox(
                    height: 65.h,
                    width: 50.h,
                    child: Stack(
                      children: [
                        Positioned.fill(
                          child: Icon(
                            Icons.inbox,
                            size: 40.w,
                            color: Colors.black54,
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: StreamBuilder(
                              stream: FirebaseFirestore.instance
                                  .collection('Rooms')
                                  .where('members',
                                      arrayContains: widget.snapshot.id)
                                  .snapshots(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<QuerySnapshot<Object?>>
                                      snapshot) {
                                if (snapshot.hasData) {
                                  if (snapshot.data!.docs.isEmpty) {
                                    return Container();
                                  }
                                  int numRoomsWithUnreadMessages = 0;
                                  for (var element in snapshot.data!.docs) {
                                    try {
                                      if (element.get(
                                              widget.snapshot.id + "_unseen") >
                                          0) {
                                        numRoomsWithUnreadMessages++;
                                      }
                                    } catch (error) {
                                      // print('');
                                    }
                                  }
                                  return numRoomsWithUnreadMessages > 0
                                      ? Container(
                                          alignment: Alignment.center,
                                          child: IntrinsicWidth(
                                            child: Container(
                                              alignment: Alignment.center,
                                              margin: const EdgeInsets.only(
                                                  bottom: 25),
                                              padding: const EdgeInsets.all(4),
                                              decoration: const BoxDecoration(
                                                  color: Colors.red,
                                                  shape: BoxShape.circle),
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(2.w),
                                                  child: Text(
                                                    numRoomsWithUnreadMessages
                                                        .toString(),
                                                    textAlign: TextAlign.center,
                                                    style: const TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container();
                                }
                                return Container();
                              }),
                        ),
                      ],
                    ),
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ChatroomsPage(id: widget.snapshot.id, isHost: true),
                    ),
                  ),
                ),
                itemWidget(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          SituationRoomPage(uid: widget.snapshot.id),
                    ),
                  ),
                  title: 'Situation Room',
                  child: Icon(
                    Icons.camera_alt,
                    size: 40.w,
                    color: Colors.black54,
                  ),
                ),
                itemWidget(
                  title: 'Analytics',
                  child: Icon(
                    Icons.bar_chart,
                    size: 40.w,
                    color: Colors.black54,
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          AnalyticsPage(snapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'iChat',
                  child: SizedBox(
                    height: 75.h,
                    width: 50.h,
                    child: Stack(
                      children: [
                        Positioned.fill(
                          child: Icon(
                            FontAwesomeIcons.message,
                            size: 40.w,
                            color: primaryColor,
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: StreamBuilder(
                              stream: FirebaseFirestore.instance
                                  .collection('Dropboxes')
                                  .where('members',
                                      arrayContains: widget.snapshot.id)
                                  .snapshots(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<QuerySnapshot<Object?>>
                                      snapshot) {
                                if (snapshot.hasData) {
                                  if (snapshot.data!.docs.isEmpty) {
                                    return Container();
                                  }
                                  int numRoomsWithUnreadMessages = 0;
                                  for (var element in snapshot.data!.docs) {
                                    try {
                                      if (element.get(
                                              widget.snapshot.id + "_unseen") >
                                          0) {
                                        numRoomsWithUnreadMessages++;
                                      }
                                    } catch (error) {
                                      // print('');
                                    }
                                  }
                                  return numRoomsWithUnreadMessages > 0
                                      ? Container(
                                          alignment: Alignment.center,
                                          // height: 50,
                                          child: IntrinsicWidth(
                                            child: Container(
                                              alignment: Alignment.center,
                                              margin: const EdgeInsets.only(
                                                  bottom: 25),
                                              padding: const EdgeInsets.all(4),
                                              decoration: const BoxDecoration(
                                                  color: Colors.red,
                                                  shape: BoxShape.circle),
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(2.w),
                                                  child: Text(
                                                    numRoomsWithUnreadMessages
                                                        .toString(),
                                                    textAlign: TextAlign.center,
                                                    style: const TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container();
                                }
                                return Container();
                              }),
                        ),
                      ],
                    ),
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          DropboxPage(snapshot: widget.snapshot),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Need Help?',
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => const HelpPage(),
                    ),
                  ),
                  child: Icon(
                    Icons.phone,
                    size: 40.w,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 30.h)
        ],
      ),
    );
  }
}
