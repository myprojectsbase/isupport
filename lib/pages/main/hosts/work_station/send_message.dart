import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/chat/chatrooms.dart';
import 'package:isupport/pages/main/hosts/work_station/broadcast/select_group.dart';
import 'package:isupport/pages/main/hosts/work_station/broadcast/select_user.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';
import '../../../../../utils/utilities.dart';

class SendMessagePage extends StatefulWidget {
  const SendMessagePage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<SendMessagePage> createState() => _SendMessagePageState();
}

class _SendMessagePageState extends State<SendMessagePage> {
  String dropdownValue = 'Select User';
  Map<String, String> userNameToNumberMap = {};
  TextEditingController messageController = TextEditingController();

  Widget filterDropdownButton() {
    return FutureBuilder(
        future: DatabaseService()
            .getUsersToNumberMap(userList: widget.snapshot.get('users')),
        builder: (BuildContext context, snapshot) {
          if (!snapshot.hasData) {
            // List<String> userIDs = await getSpecificHostData('users', hostUID: hostUID);
            return Container();
          }
          userNameToNumberMap = (snapshot.data! as Map<String, String>);
          return Center(
            child: roundedContainer(
              buttonColor: Colors.black54,
              radius: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5.h),
                child: FittedBox(
                  child: DropdownButton<String>(
                    isDense: true,
                    // remove underline
                    underline: Container(),

                    // Initial Value
                    value: dropdownValue,

                    // Down Arrow Icon
                    icon: Padding(
                      padding: EdgeInsets.only(right: 10.w),
                      child: const Icon(CupertinoIcons.chevron_down),
                    ),

                    // Array list of items
                    items: [
                      // initial value should be contained in items
                      'Select User',
                      ...userNameToNumberMap.keys
                    ].map((String item) {
                      return DropdownMenuItem(
                        value: item,
                        child: Container(
                          padding: EdgeInsets.only(
                              left: 30.w,
                              right: item == dropdownValue ? 70.w : 0),
                          child: FittedBox(
                            child: Text(item,
                                style: TextStyle(
                                    fontSize: 21.sp, color: Colors.black54)),
                          ),
                        ),
                      );
                    }).toList(),
                    // After selecting the desired option,it will
                    // change button value to selected value
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue!;
                      });
                    },
                  ),
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
      appBar: customAppbar(context, title: 'Send Broadcast'),
      child: Stack(
        children: [
          Column(
            children: [
              SizedBox(height: 25.h),
              Text(
                'You can send broadcast to a list of your users',
                style: TextStyle(fontSize: 18.sp),
              ),
              SizedBox(height: 25.h),
              Text(
                'To send message to a user please use',
                style: TextStyle(fontSize: 18.sp),
              ),
              // SizedBox(height: 25.h),
              InkWell(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ChatroomsPage(id: widget.snapshot.id, isHost: true),
                  ),
                ),
                child: Text(
                  'Send Message',
                  style: TextStyle(
                    fontSize: 18.sp,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 25.h),
              Container(
                margin: EdgeInsets.only(bottom: 18.h),
                alignment: Alignment.centerLeft,
                child: Text(
                  'Sending to:',
                  style: TextStyle(fontSize: 18.sp),
                ),
              ),
              filterDropdownButton()
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Stack(
              children: [
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(bottom: 15.h),
                  child: roundedContainer(
                      buttonColor: Colors.black,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15.w),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                controller: messageController,
                                decoration: customInputDecoration(
                                    hintText: 'Type message here'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 5.w),
                              child: InkWell(
                                onTap: () async {
                                  if (messageController.text.isNotEmpty) {
                                    await SystemChannels.textInput
                                        .invokeMethod('TextInput.hide');
                                    String? phoneNumber =
                                        userNameToNumberMap[dropdownValue];
                                    if (phoneNumber != null &&
                                        phoneNumber != '') {
                                      bulkSMSConfirmationDialog(context,
                                          phoneNumbers: [phoneNumber],
                                          senderName:
                                              widget.snapshot.get('fullName'),
                                          message: messageController.text);
                                    } else {
                                      customScaffoldMessage(
                                          'User account selected has no phone number associated with it',
                                          context,
                                          duration: const Duration(seconds: 3));
                                    }
                                  } else {
                                    customScaffoldMessage(
                                        'Message box cannot be empty', context,
                                        duration: const Duration(seconds: 3));
                                  }
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: lightGreen,
                                      shape: BoxShape.circle),
                                  child: const Padding(
                                    padding: EdgeInsets.all(1),
                                    child: Icon(Icons.arrow_upward,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )),
                ),
                if (dropdownValue == 'Select User')
                  Positioned.fill(
                    child: InkWell(
                      child: Container(color: Colors.white30),
                      onTap: () => customScaffoldMessage(
                        'Message receiver not selected',
                        context,
                        duration: const Duration(seconds: 3),
                      ),
                    ),
                  )
              ],
            ),
          )
        ],
      ),
    );
  }
}
