import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_groups/group_details.dart';
import 'package:isupport/pages/main/user_details.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/utilities.dart';

import '../../../../services/database.dart';
import '../../../../utils/constants.dart';

class AnalyticsPage extends StatefulWidget {
  const AnalyticsPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<AnalyticsPage> createState() => _AnalyticsPageState();
}

class _AnalyticsPageState extends State<AnalyticsPage> {
  Widget sideBySideDecoratedData(
      {required String title, required String value}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 50.h,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  top: BorderSide(width: 1.h, color: primaryColor),
                  left: BorderSide(width: 1.h, color: primaryColor),
                  right: BorderSide(width: 1.h, color: primaryColor),
                  bottom: BorderSide(width: 1.h, color: primaryColor),
                ),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(8.w),
                  topLeft: Radius.circular(8.w),
                ),
              ),
              child: Container(
                padding: EdgeInsets.only(left: 15.w),
                alignment: Alignment.centerLeft,
                child: Text(
                  title,
                  style: TextStyle(color: Colors.black, fontSize: 18.sp),
                ),
              ),
            ),
          ),
          Container(
            width: 115.w,
            height: 50.h,
            decoration: BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(8.w),
                topRight: Radius.circular(8.w),
              ),
            ),
            child: Center(
              child: Text(
                value,
                style: TextStyle(color: Colors.white, fontSize: 18.sp),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget topToBottomDecoratedData(
      {required String title,
      required String value,
      required bool isUser,
      required Function() onTap}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Column(
        children: [
          Container(
            height: 35.h,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                top: BorderSide(width: 1.h, color: primaryColor),
                left: BorderSide(width: 1.h, color: primaryColor),
                right: BorderSide(width: 1.h, color: primaryColor),
                bottom: BorderSide(width: 1.h, color: primaryColor),
              ),
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(8.w),
                topLeft: Radius.circular(8.w),
              ),
            ),
            child: Center(
              child: Text(
                title,
                style: TextStyle(color: Colors.black, fontSize: 18.sp),
              ),
            ),
          ),
          InkWell(
            onTap: onTap,
            child: Container(
              height: 35.h,
              decoration: BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.w),
                  bottomLeft: Radius.circular(8.w),
                ),
              ),
              child: Center(
                child: Text(
                  value,
                  style: TextStyle(color: Colors.white, fontSize: 18.sp),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget analyticsPadding({required Widget child}) {
    return Container(
      color: Colors.grey[200],
      margin: EdgeInsets.only(top: 5.h),
      padding: EdgeInsets.symmetric(vertical: 15.h),
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Analytics'),
      body: Column(
        children: [
          analyticsPadding(
            child: sideBySideDecoratedData(
              title: 'Total numbers of users',
              value: widget.snapshot.get('users').length.toString(),
            ),
          ),
          analyticsPadding(
            child: sideBySideDecoratedData(
              title: 'Total numbers of supports',
              value: formattedNumber(widget.snapshot.get('supports').length),
            ),
          ),
          analyticsPadding(
            child: sideBySideDecoratedData(
              title: 'Total numbers of groups',
              value: widget.snapshot.get('groups').length.toString(),
            ),
          ),
          FutureBuilder(
              future: DatabaseService().userWithHighestSupportRegistrations(
                  userList: widget.snapshot.get('users')),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return Container();
                }
                return analyticsPadding(
                  child: topToBottomDecoratedData(
                    title: 'User with highest support registrations',
                    value: snapshot.data!.get('fullName') ??
                        '${snapshot.data!.get('phoneNumber') as String} (No name)' ??
                        'No name/phone number',
                    isUser: true,
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => UserDetails(
                            userData: snapshot.data, isSupport: false),
                      ),
                    ),
                  ),
                );
              }),
          FutureBuilder(
              future: DatabaseService().groupWithHighestSupportRegistrations(
                  groupsList: widget.snapshot.get('groups')),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return Container();
                }
                return analyticsPadding(
                  child: topToBottomDecoratedData(
                    title: 'Group with highest support registrations',
                    value: snapshot.data!.get('groupName'),
                    isUser: false,
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            GroupDetailsPage(snapshot: snapshot.data),
                      ),
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
