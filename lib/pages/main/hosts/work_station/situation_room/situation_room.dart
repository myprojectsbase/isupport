import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/work_station/situation_room/situation_details.dart';
import 'package:isupport/pages/main/users/situation_report/situation_room.dart';
import 'package:isupport/utils/components.dart';

import '../../../../../services/database.dart';
import '../../../../../utils/utilities.dart';

class SituationRoomPage extends StatefulWidget {
  const SituationRoomPage({super.key, required this.uid});
  final String uid;

  @override
  State<SituationRoomPage> createState() => _SituationRoomPageState();
}

class _SituationRoomPageState extends State<SituationRoomPage> {
  List<String> filterOptions = ['All Media', 'All Photos', 'All Videos'];
  String filter = 'All Media';
  double mediaHeight = 75.w;
  double mediaWidth = 100.w;

  Widget imageContainer(QueryDocumentSnapshot element) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SituationDetailsPage(
            document: element,
            isImages: true,
          ),
        ),
      ),
      child: Container(
        margin: EdgeInsets.only(right: 8.w),
        height: mediaHeight,
        width: mediaWidth,
        color: Colors.grey.shade200,
        child: CachedNetworkImage(
          imageUrl: element.get('imageURLs').first,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
      appBar: customAppbar(context, title: 'Situation Room'),
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h),
            child: Text(
              "You are now viewing both videos and photos",
              style: TextStyle(fontSize: 18.sp),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h),
            child: CustomDropdownWidget(
              initialDropdownValue: filter,
              hint: 'Filter by: Select',
              items: filterOptions,
              onChanged: (newValue) {
                setState(() {
                  filter = newValue!;
                });
              },
            ),
          ),
          StreamBuilder(
            stream:
                DatabaseService().getHostSituations(hostAssociated: widget.uid),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 30.h),
                  child: Wrap(
                    spacing: 15.w,
                    runSpacing: 15.w,
                    children: snapshot.data!.docs
                        .where((element) {
                          if (filter == 'All Videos') {
                            return (element.get('imageURLs') as List).isEmpty;
                          }
                          if (filter == 'All Photos') {
                            return (element.get('videoURLs') as List).isEmpty;
                          }
                          return true;
                        })
                        .map(
                          (e) => (e.get('imageURLs') as List).isNotEmpty
                              ? imageContainer(e)
                              : SizedBox(
                                  height: mediaHeight,
                                  width: mediaWidth,
                                  child: VideoThumbnail(
                                    document: e,
                                  ),
                                ),
                        )
                        .toList(),
                  ),
                );
              }
              return Container();
            },
          )
        ],
      ),
    );
  }
}
