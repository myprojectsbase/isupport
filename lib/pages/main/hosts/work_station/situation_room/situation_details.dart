import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/user_details.dart';
import 'package:isupport/services/database.dart';
import 'package:video_player/video_player.dart';

import '../../../../../utils/components.dart';

class SituationDetailsPage extends StatelessWidget {
  SituationDetailsPage(
      {super.key, required this.document, required this.isImages});
  final QueryDocumentSnapshot<Object?> document;
  final bool isImages;
  final double mediaHeight = 75.w;
  final double mediaWidth = 100.w;

  Widget imageContainer(String url, BuildContext context) {
    return GestureDetector(
      onTap: () => customShowDialog(
          context,
          CachedNetworkImage(
            imageUrl: url,
            fit: BoxFit.fill,
          )),
      child: Container(
        margin: EdgeInsets.only(right: 8.w),
        height: mediaHeight,
        width: mediaWidth,
        color: Colors.grey.shade200,
        child: CachedNetworkImage(
          imageUrl: url,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
      appBar: customAppbar(context, title: 'Situation Details'),
      child: ListView(
        children: [
          document.get('description') != null
              ? Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(
                    top: 20.w,
                    bottom: 20.w,
                    // left: 30.w,
                    right: 30.w,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Agent: ",
                            style: TextStyle(
                                fontSize: 20.sp, fontWeight: FontWeight.bold),
                          ),
                          FutureBuilder(
                              future: DatabaseService().getUserData(
                                  userID: document.get('userAssociated')),
                              builder: (builder,
                                  AsyncSnapshot<DocumentSnapshot<Object?>>
                                      snapshot) {
                                if (snapshot.hasData) {
                                  return InkWell(
                                      onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => UserDetails(
                                                  userData: snapshot.data!,
                                                  isSupport: false),
                                            ),
                                          ),
                                      child: Text(
                                        "${snapshot.data!.get('fullName')}",
                                        style: TextStyle(
                                            fontSize: 20.sp,
                                            color: Colors.blue),
                                      ));
                                }
                                return Container();
                              })
                        ],
                      ),
                      SizedBox(height: 15.h),
                      Row(
                        children: [
                          Text(
                            "Description:  ",
                            style: TextStyle(
                                fontSize: 20.sp, fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                            child: Text(
                              "${document.get('description')}",
                              style: TextStyle(fontSize: 20.sp),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              : Container(height: 40.w),
          Text(
            "Photos/Videos:  ",
            style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h),
            child: Wrap(
              spacing: 15.w,
              runSpacing: 15.w,
              children:
                  (document.get(isImages ? 'imageURLs' : 'videoURLs') as List)
                      .map(
                        (e) => isImages
                            ? imageContainer(e, context)
                            : SizedBox(
                                height: mediaHeight,
                                width: mediaWidth,
                                child: VideoWigdet(
                                  url: e,
                                ),
                              ),
                      )
                      .toList(),
            ),
          )
        ],
      ),
    );
  }
}

class VideoWigdet extends StatefulWidget {
  const VideoWigdet(
      {super.key,
      required this.url,
      this.isExpanded = false,
      this.autoPlay = false});
  final String url;
  final bool isExpanded;
  final bool autoPlay;

  @override
  State<VideoWigdet> createState() => _VideoWigdetState();
}

class _VideoWigdetState extends State<VideoWigdet> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.url);
    _controller.initialize().then((_) {
      if (widget.autoPlay) {
        _controller.play();
      }
      setState(() {}); //when your thumbnail will show.
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (!widget.autoPlay) {
          customShowDialog(
            context,
            AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: VideoWigdet(
                url: widget.url,
                autoPlay: true,
              ),
            ),
          );
        }
      },
      child: _controller.value.isInitialized
          ? VideoPlayer(_controller)
          : SizedBox(
              width: 50.w,
              height: 50.w,
              child: const Center(
                child: CupertinoActivityIndicator(),
              ),
            ),
    );
  }
}
