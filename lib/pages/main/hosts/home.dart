import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/manage_agents/users.dart';
import 'package:isupport/pages/main/hosts/manage_supports/users_supports/supports.dart';
import 'package:isupport/pages/main/hosts/work_station/workstation.dart';
import 'package:isupport/pages/main/settings/profile/profile.dart';
import 'package:isupport/pages/main/settings/settings.dart';
import 'package:isupport/pages/registry/pick_color.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:jiffy/jiffy.dart';
import '../../registry/choose.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../chat/chat.dart';
import '../chat/chatrooms.dart';
import '../help.dart';
import 'work_station/broadcast/broadcast.dart';

class HostHomePage extends StatefulWidget {
  const HostHomePage({Key? key, required this.uid}) : super(key: key);
  final String uid;

  @override
  State<HostHomePage> createState() => _HostHomePageState();
}

class _HostHomePageState extends State<HostHomePage>
    with WidgetsBindingObserver {
  bool pageLoaded = false;
  Widget dummyHeader() {
    return customScaffold(
      appBar: customAppbar(context),
      child: const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void registerNotifications() async {
    final FirebaseMessaging fcm = FirebaseMessaging.instance;
    // 3. On iOS, this helps to take the user permissions
    NotificationSettings settings = await fcm.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');

      FirebaseMessaging.instance
          .getInitialMessage()
          .then((RemoteMessage? message) {
        if (message != null) {
          print("This should work!!");
          if (message.notification!.body!.contains('New message:  ')) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return ChatPage(
                    title: message.data['senderName'],
                    peerId: message.data['senderID'],
                    id: widget.uid,
                    roomId: message.data['roomID'],
                    senderIsHost: true,
                  );
                },
              ),
            );
          }
        }
        // _serialiseAndNavigate(message);
      });
      // For handling the received notifications
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        // notification came in when user was online
      });
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        print('A new onMessageOpenedApp event was published!');
        print("This should work!!");
        if (message.notification!.body!.contains('New message:  ')) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return ChatPage(
                  title: message.data['senderName'],
                  peerId: message.data['senderID'],
                  id: widget.uid,
                  roomId: message.data['roomID'],
                  senderIsHost: true,
                );
              },
            ),
          );
        }
      });
    } else {
      print('User declined or has not accepted permission');
    }
  }

  void disableIfInactive() async {
    bool isActive = await DatabaseService().checkIfHostIsActive(widget.uid);
    if (!isActive) {
      customScaffoldMessage(
          'Your access for this account has expired. Please visit isupport.com.ng for details on how to resubscribe.',
          context,
          duration: const Duration(seconds: 6));
      DatabaseService()
          .updateHostData({'isOnline': false}, hostUID: widget.uid);
      DatabaseService().checkAndRemovePastTokens();
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => const RegisterOrLogin(),
        ),
      );
    }
  }

  void setNewTheme(int colorCode) {
    setState(() {
      setPrimaryColor(colorCode: colorCode);
    });
  }

  void getThemeFromRemote() async {
    try {
      int? themeColorCode = await DatabaseService()
          .getSpecificHostData("themeColorCode", hostUID: widget.uid);
      if (themeColorCode != null) {
        setNewTheme(themeColorCode);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    if (pageLoaded == false) {
      registerNotifications();
      disableIfInactive();
      DatabaseService().updateHostData({'isOnline': true}, hostUID: widget.uid);
      getThemeFromRemote();
      pageLoaded = true;
    }
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      DatabaseService().updateHostData({'isOnline': true}, hostUID: widget.uid);
    } else {
      DatabaseService()
          .updateHostData({'isOnline': false}, hostUID: widget.uid);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: DatabaseService().hostSnapshots(hostUID: widget.uid),
      builder: (BuildContext context,
          AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
        if (!snapshot.hasData) {
          return dummyHeader();
        }
        String? bannerPhotoURL;
        try {
          bannerPhotoURL = snapshot.data!.get('bannerPhotoURL');
        } catch (e) {
          null;
        }
        return customScaffold(
          drawer: customDrawer(context, snapshot, setNewTheme),
          appBar: roundedAppbar(context, uid: widget.uid, isHost: true),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  bannerPhotoURL == null
                      ? SizedBox(height: 30.h)
                      : Container(
                          width: double.infinity,
                          padding: EdgeInsets.only(top: 10.h, bottom: 15.h),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.w),
                            child: Image.network(
                              bannerPhotoURL,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 75.w,
                        width: 75.w,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey.withOpacity(0.6),
                          image: snapshot.data!.get('profilePhotoURL') == null
                              ? null
                              : DecorationImage(
                                  image: CachedNetworkImageProvider(
                                    snapshot.data!.get('profilePhotoURL'),
                                  ),
                                ),
                        ),
                        child: snapshot.data!.get('profilePhotoURL') == null
                            ? Padding(
                                padding: EdgeInsets.all(20.w),
                                child: Icon(
                                  Icons.person,
                                  color: Colors.black54,
                                  size: 35.w,
                                ),
                              )
                            : null,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 15.w),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Hi",
                                style: TextStyle(
                                    fontSize: 22.sp, color: Colors.black54)),
                            Text(snapshot.data!.get('fullName'),
                                style: TextStyle(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 1.h,
                    margin: EdgeInsets.only(
                        top: bannerPhotoURL == null ? 20.h : 10.h, bottom: 7.h),
                    color: primaryColor,
                  ),
                  Row(
                    children: [
                      Text(
                        "Election Date: ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14.sp),
                      ),
                      Text(
                          snapshot.data!.get('nextElectionDate') != null
                              ? Jiffy((snapshot.data!.get('nextElectionDate')
                                          as Timestamp?)!
                                      .toDate())
                                  .format("MMM d yyyy")
                                  .toUpperCase()
                              : 'Set Election Date',
                          style: TextStyle(
                              color: Colors.black54, fontSize: 14.sp)),
                      const Spacer(),
                      Text(
                          Jiffy(DateTime.now())
                              .format("MMM d yyyy")
                              .toUpperCase(),
                          style:
                              TextStyle(color: Colors.black54, fontSize: 14.sp))
                    ],
                  ),
                  SizedBox(height: bannerPhotoURL != null ? 20.h : 50.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          roundedContainer(
                            radius: 30.w,
                            child: InkWell(
                              onTap: () =>
                                  Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) => UsersPage(
                                    uid: widget.uid,
                                    hostName: snapshot.data!.get('fullName')),
                              )),
                              child: SizedBox(
                                height: 130.w,
                                width: 130.w,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(height: 5.w),
                                    Container(
                                      decoration: BoxDecoration(
                                          color: Colors.grey[300],
                                          shape: BoxShape.circle),
                                      child: Padding(
                                        padding: EdgeInsets.all(8.w),
                                        child: Icon(
                                          Icons.person,
                                          size: 38.w,
                                          color: Colors.black54,
                                        ),
                                      ),
                                    ),
                                    FittedBox(
                                      child: Text(
                                          formattedNumber(snapshot.data!
                                              .get('users')
                                              .length),
                                          style: TextStyle(
                                              fontSize: 40.sp,
                                              fontWeight: FontWeight.bold)),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          Text("Total Agents",
                              style: TextStyle(
                                  fontSize: 16.sp, color: Colors.black54))
                        ],
                      ),
                      SizedBox(width: 20.w),
                      Column(
                        children: [
                          roundedContainer(
                            radius: 30.w,
                            child: InkWell(
                              onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SupportsPage(
                                          hostSnapshot: snapshot.data!),
                                ),
                              ),
                              child: SizedBox(
                                height: 130.w,
                                width: 130.w,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(height: 5.w),
                                    Container(
                                      decoration: BoxDecoration(
                                          color: Colors.grey[300],
                                          shape: BoxShape.circle),
                                      child: Padding(
                                        padding: EdgeInsets.all(12.w),
                                        child: Icon(
                                          Icons.people,
                                          size: 25.w,
                                          color: Colors.black54,
                                        ),
                                      ),
                                    ),
                                    FittedBox(
                                      child: Text(
                                          formattedNumber(snapshot.data!
                                              .get('supports')
                                              .length),
                                          style: TextStyle(
                                              fontSize: 40.sp,
                                              fontWeight: FontWeight.bold)),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          Text("Total Supports",
                              style: TextStyle(
                                  fontSize: 16.sp, color: Colors.black54))
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: bannerPhotoURL != null ? 20.h : 40.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          InkWell(
                            onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    BroadcastPage(snapshot: snapshot.data!),
                              ),
                            ),
                            child: roundedContainer(
                              radius: 30.w,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 40.h, vertical: 40.w),
                                child: Icon(Icons.message,
                                    size: 50.w, color: primaryColor),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          Text("Send Broadcast",
                              style: TextStyle(
                                  fontSize: 16.sp, color: Colors.black54))
                        ],
                      ),
                      SizedBox(width: 20.w),
                      Column(
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      WorkStationPage(snapshot: snapshot.data!),
                                ),
                              );
                            },
                            child: roundedContainer(
                              radius: 30.w,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 40.h, vertical: 40.w),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: lightGreen,
                                      shape: BoxShape.circle),
                                  child: Padding(
                                    padding: EdgeInsets.all(9.w),
                                    child: Icon(
                                      Icons.work,
                                      color: Colors.white,
                                      size: 30.w,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          Text(
                            "Work Station",
                            style: TextStyle(
                                fontSize: 16.sp, color: Colors.black54),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

Drawer customDrawer(
    BuildContext context,
    AsyncSnapshot<DocumentSnapshot<Object?>> snapshot,
    Function(int colorCode) changeHomePageTheme) {
  return Drawer(
    backgroundColor: const Color(0xFF444444),
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top + 40.h),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 50.w,
                width: 50.w,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: const Color(0XFFE5E5E5),
                  image: snapshot.data!.get('profilePhotoURL') == null
                      ? null
                      : DecorationImage(
                          image: CachedNetworkImageProvider(
                            snapshot.data!.get('profilePhotoURL'),
                          ),
                        ),
                ),
                child: snapshot.data!.get('profilePhotoURL') == null
                    ? Padding(
                        padding: EdgeInsets.all(10.w),
                        child: Icon(
                          Icons.person,
                          color: Colors.black54,
                          size: 25.w,
                        ),
                      )
                    : null,
              ),
              Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Hi", style: TextStyle(color: Colors.white)),
                    SizedBox(height: 3.h),
                    Text(snapshot.data!.get('fullName'),
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)),
                  ],
                ),
              ),
              const Spacer(),
              Align(
                alignment: Alignment.bottomRight,
                child: InkWell(
                    onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) => ProfilePage(
                                snapshot: snapshot.data!, isHost: true),
                          ),
                        ),
                    child: Icon(Icons.edit, color: Colors.white, size: 16.sp)),
              )
            ],
          ),
          Container(
            height: 1.h,
            margin: EdgeInsets.only(top: 20.h, bottom: 25.h),
            color: Colors.white,
          ),
          drawerItem(
              icon: const Icon(
                Icons.home,
                color: Colors.white,
              ),
              title: 'Dashboard',
              onTap: () => Navigator.of(context).pop()),
          drawerItem(
              icon: const Icon(
                Icons.person,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => UsersPage(
                          uid: snapshot.data!.id,
                          hostName: snapshot.data!.get('fullName')),
                    ),
                  ),
              title: 'Agents'),
          drawerItem(
              icon: const Icon(
                Icons.work,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          WorkStationPage(snapshot: snapshot.data!),
                    ),
                  ),
              title: 'Work Station'),
          drawerItem(
              icon: const Icon(
                Icons.forum,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => ChatroomsPage(
                          id: snapshot.data!.id,
                          hostSnapshot: snapshot.data!,
                          isHost: true,
                          hostUID: snapshot.data!.id),
                    ),
                  ),
              title: 'Messages'),
          drawerItem(
              icon: const Icon(
                Icons.phone,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => const HelpPage(),
                    ),
                  ),
              title: 'Help'),
          drawerItem(
              icon: const Icon(
                Icons.security,
                color: Colors.white,
              ),
              title: 'Situation Room'),
          drawerItem(
              icon: const Icon(
                Icons.settings,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SettingsPage(
                        snapshot: snapshot.data!,
                        isHost: true,
                        changeHomepageThemeColor: changeHomePageTheme,
                      ),
                    ),
                  ),
              title: 'Settings'),
          SizedBox(height: 15.h),
          drawerItem(
            icon: const Icon(
              Icons.logout,
              color: Colors.white,
            ),
            title: 'Log out',
            onTap: () {
              DatabaseService().updateHostData({'isOnline': false},
                  hostUID: snapshot.data!.id);
              DatabaseService().checkAndRemovePastTokens();
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (BuildContext context) => const RegisterOrLogin(),
                ),
              );
            },
          ),
        ],
      ),
    ),
  );
}

Widget drawerItem(
    {required Icon icon, required String title, void Function()? onTap}) {
  return InkWell(
    onTap: onTap,
    child: Padding(
      padding: EdgeInsets.symmetric(vertical: 19.h),
      child: Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 15.w), child: icon),
          Text(title, style: TextStyle(color: Colors.white, fontSize: 18.sp))
        ],
      ),
    ),
  );
}
