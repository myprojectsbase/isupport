import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HelpPage extends StatefulWidget {
  const HelpPage({Key? key}) : super(key: key);

  @override
  State<HelpPage> createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return customScaffold(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20.h),
            Text(
              'iSupport is readily available to meet your support needs.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 16.sp,
              ),
            ),
            SizedBox(height: 20.h),
            Text(
              'Call our support line for help',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 16.sp,
              ),
            ),
            SizedBox(height: 20.h),
            Text(
              '+234 803 559 2221',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 20.sp,
              ),
            ),
            SizedBox(height: 20.h),
            Text(
              'OR',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 16.sp,
              ),
            ),
            SizedBox(height: 20.h),
            Text(
              'Send us a message for your enquiries, complaints or suggestions.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.sp,
              ),
            ),
            SizedBox(height: 20.h),
            textFieldPadding(
              buttonColor: Colors.black54,
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              radius: 0,
              child: TextField(
                controller: emailController,
                decoration: customInputDecoration(hintText: 'Your Email'),
              ),
            ),
            SizedBox(height: 20.h),
            textFieldPadding(
              buttonColor: Colors.black54,
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              radius: 0,
              child: TextField(
                controller: phoneController,
                decoration: customInputDecoration(hintText: 'Phone'),
              ),
            ),
            SizedBox(height: 20.h),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Type your message',
                style: TextStyle(
                  fontSize: 16.sp,
                ),
              ),
            ),
            SizedBox(height: 10.h),
            textFieldPadding(
              buttonColor: Colors.black54,
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              radius: 0,
              child: TextField(
                maxLines: 8,
                controller: messageController,
                decoration: customInputDecoration(hintText: 'Message'),
              ),
            ),
            SizedBox(height: 10.h),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.h),
              child: filledInButton(
                onTap: () async {
                  if (messageController.text.isEmpty) {
                    customScaffoldMessage(
                        'Message box cannot be left empty.', context,
                        duration: const Duration(seconds: 2));
                  } else {
                    if (phoneController.text.isEmpty &&
                        emailController.text.isEmpty) {
                      customScaffoldMessage(
                        'Please supply us with either an email, a phone number or both to ensure we can get back to you.',
                        context,
                        duration: const Duration(seconds: 3),
                      );
                    } else {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      String uid = prefs.getString('uid')!;
                      String userType = prefs.getString('userType')!;
                      customScaffoldMessage(
                        'Submitting...',
                        context,
                        duration: const Duration(hours: 3),
                      );
                      bool isSuccessful =
                          await DatabaseService().sendSupportRequest(
                        phoneNumber: phoneController.text,
                        email: emailController.text,
                        message: messageController.text,
                        userType: userType,
                        uid: uid,
                      );
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      if (isSuccessful) {
                        customScaffoldMessage(
                          'Message sent successfully',
                          context,
                          duration: const Duration(hours: 3),
                        );
                        messageController.text = '';
                      } else {
                        customScaffoldMessage(
                          'Error sending message. Please try again.',
                          context,
                          duration: const Duration(hours: 3),
                        );
                      }
                    }
                  }
                },
                text: 'Send',
                radius: 0,
                fontSize: 24.sp,
              ),
            ),
            SizedBox(height: 10.h),
          ],
        ),
      ),
      appBar: customAppbar(context, title: 'Help'),
    );
  }
}
