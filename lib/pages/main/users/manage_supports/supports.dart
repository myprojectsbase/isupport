import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/user_details.dart';
import 'package:isupport/utils/components.dart';
import 'package:localstore/localstore.dart';

import '../../../../services/database.dart';

class UsersSupportsPage extends StatefulWidget {
  const UsersSupportsPage(
      {Key? key, required this.hostUID, required this.userID})
      : super(key: key);
  final String hostUID;
  final String userID;

  @override
  State<UsersSupportsPage> createState() => _UsersSupportsPageState();
}

class _UsersSupportsPageState extends State<UsersSupportsPage> {
  bool showingOfflineSupports = false;
  List<String> filterOptions = [
    "Show by ward",
    "Show by other",
    "Show by date added"
  ];
  String dropdownValue = "Show by ward";
  Widget supporterRow(String supporterUID) {
    return FutureBuilder(
        future: DatabaseService().getSupporterData(supporterUID: supporterUID),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          return Column(
            children: [
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => UserDetails(
                      userData: snapshot.data!,
                      isSupport: true,
                    ),
                  ),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
                  child: Row(
                    children: [
                      Text(
                        snapshot.data!.get('fullName'),
                        style: TextStyle(fontSize: 18.sp, color: Colors.black),
                      ),
                      const Spacer(),
                      const Icon(
                        Icons.arrow_forward_ios,
                        // size: 16,
                        color: Colors.black54,
                      ),
                    ],
                  ),
                ),
              ),
              Container(height: 1.h, color: Colors.grey)
            ],
          );
        });
  }

  Widget offlineRow(dynamic data) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
          color: Colors.redAccent,
          child: Text(
            data['fullName'],
            style: TextStyle(fontSize: 18.sp, color: Colors.white),
          ),
        ),
        Container(height: 1.h, color: Colors.grey)
      ],
    );
  }

  Widget filterDropdownButton() {
    return roundedContainer(
      buttonColor: Colors.black54,
      radius: 0,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.h),
        child: DropdownButton<String>(
          isDense: true,
          // remove underline
          underline: Container(),

          // Initial Value
          value: dropdownValue,

          // Down Arrow Icon
          icon: Padding(
            padding: EdgeInsets.only(right: 10.w),
            child: const Icon(CupertinoIcons.chevron_down),
          ),

          // Array list of items
          items: filterOptions.map((String items) {
            return DropdownMenuItem(
              value: items,
              child: Container(
                padding: EdgeInsets.only(left: 55.w),
                child: Text(items,
                    style: TextStyle(fontSize: 18.sp, color: Colors.grey)),
              ),
            );
          }).toList(),
          // After selecting the desired option,it will
          // change button value to selected value
          onChanged: (String? newValue) {
            setState(() {
              dropdownValue = newValue!;
            });
          },
        ),
      ),
    );
  }

  Widget showOfflineSupports() {
    return FutureBuilder(
      future: Localstore.instance
          .collection('${widget.hostUID}OfflineSupports')
          .get(),
      builder: (context, AsyncSnapshot<Map<String, dynamic>?> result) {
        if (result.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (!result.hasData || result.data!.isEmpty) {
          return Center(
            child: Padding(
              padding: EdgeInsets.only(top: 150.h),
              child: Text(
                'No offline supports',
                style: TextStyle(fontSize: 18.sp),
              ),
            ),
          );
        }
        List<Widget> kids = [];
        result.data!.forEach(
          (key, value) {
            kids.add(
              offlineRow(value),
            );
          },
        );
        return Column(
          children: kids,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Support List"),
      body: StreamBuilder(
          stream: DatabaseService().hostSupportsSnapshots(
              hostUID: widget.hostUID, userID: widget.userID),
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            return SingleChildScrollView(
              child: showingOfflineSupports
                  ? showOfflineSupports()
                  : Column(
                      children: [
                        SizedBox(height: 30.h),
                        Text(
                          "Total Supports (${snapshot.data!.docs.length})",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black54),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 30.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('Filter',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.sp,
                                    color: Colors.grey)),
                            SizedBox(width: 12.w),
                            filterDropdownButton(),
                          ],
                        ),
                        SizedBox(height: 30.h),
                        Container(
                          height: 50.h,
                          width: double.infinity,
                          color: Colors.grey[300],
                          child: Center(
                            child: Text(
                              "Recently added List",
                              style: TextStyle(
                                  fontSize: 20.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black54),
                            ),
                          ),
                        ),
                        // SizedBox(height: 10.h),
                        ...snapshot.data!.docs.map((e) => supporterRow(e.id))
                      ],
                    ),
            );
          }),
      floatingActionButton: FloatingActionButton.extended(
        label: Text(
            showingOfflineSupports ? 'Online Supports' : 'Offline Supports'),
        icon: const Icon(Icons.swap_horiz),
        onPressed: () => setState(() {
          showingOfflineSupports = !showingOfflineSupports;
        }),
      ),
    );
  }
}
