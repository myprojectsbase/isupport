// ignore_for_file: body_might_complete_normally_nullable, use_build_context_synchronously

import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:isupport/pages/main/groups/manage_supports/registration_complete.dart'
    as GroupRegistrationComplete;
import 'package:isupport/pages/main/hosts/manage_agents/users.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/wards.dart';
import 'package:jiffy/jiffy.dart';
import 'package:localstore/localstore.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../utils/constants.dart';
import '../../../../utils/utilities.dart';
import 'registration_complete.dart';

class RegisterSupportPage extends StatefulWidget {
  const RegisterSupportPage(
      {Key? key,
      this.userID,
      this.groupID,
      required this.hostUID,
      this.supporterSnapshot})
      : super(key: key);
  final String? userID;
  final String? groupID;
  final String hostUID;
  final DocumentSnapshot? supporterSnapshot;

  @override
  State<RegisterSupportPage> createState() => _RegisterSupportPageState();
}

class _RegisterSupportPageState extends State<RegisterSupportPage> {
  bool pageLoaded = false;
  final _formKey = GlobalKey<FormState>();
  String? profilePhotoURL;
  TextEditingController nameController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  TextEditingController wardController = TextEditingController();
  TextEditingController unitController = TextEditingController();
  TextEditingController votersCardNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController bankController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  // TextEditingController qualificationController = TextEditingController();
  TextEditingController occupationController = TextEditingController();

  DateTime? dateOfBirth;
  late String imageUrl;
  File? image;
  String? state;
  String? lga;
  String? gender;
  String? qualification;
  String? employmentStatus;
  List<String> employmentStatuses = ['Employed', 'Unemployed'];
  List<String> genders = ['Male', 'Female'];
  List<String> qualifications = const [
    'Primary School Certificate',
    'WASSCE',
    'BSc',
    'MSc',
    'PhD',
    'Post Doctorate'
  ];
  bool stateSelected = false;

  Future<List<File>> getImage({bool allowMultiple = false}) async {
    List<File> imagesAssociated = <File>[];
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: allowMultiple,
        type: FileType.custom,
        allowedExtensions: ['jpg', 'jpeg', 'png', 'svg']);

    if (result != null) {
      imagesAssociated = result.paths.map((path) => File(path!)).toList();
      print(imagesAssociated);
    }
    return imagesAssociated;
  }

  selectImage({bool usingCamera = false}) async {
    await Permission.storage.request();
    File? imageSelected;
    var permissionStatus = await Permission.storage.status;

    if (permissionStatus.isGranted) {
      if (usingCamera) {
        final ImagePicker _picker = ImagePicker();
        // Capture a photo
        final XFile? photo =
            await _picker.pickImage(source: ImageSource.camera);
        imageSelected = File(photo!.path);
      } else {
        //Select Image from galery
        List<File> results = await getImage();
        imageSelected = results.first;
      }
      CroppedFile? croppedImage = await getImageCropped(imageSelected);
      setState(() {
        image = File(croppedImage!.path);
      });
    } else {
      customScaffoldMessage(
          'Permission not granted. Kindly grant permission then try again.',
          context);
    }
  }

  void getSupportData() async {
    String occupation = '';
    String remoteDateOfBirth = '';
    try {
      remoteDateOfBirth =
          Jiffy(widget.supporterSnapshot!.get('dateOfBirth').toDate())
              .format("d/MM/yyyy");
    } catch (e) {
      null;
    }
    try {
      wardController.text = widget.supporterSnapshot!.get('ward');
    } catch (e) {
      null;
    }
    try {
      employmentStatus = widget.supporterSnapshot!.get('employmentStatus');
    } catch (e) {
      null;
    }
    try {
      bankController.text = widget.supporterSnapshot!.get('bank');
    } catch (e) {
      null;
    }
    try {
      accountNumberController.text =
          widget.supporterSnapshot!.get('accountNumber');
    } catch (e) {
      null;
    }
    try {
      unitController.text = widget.supporterSnapshot!.get('unit');
    } catch (e) {
      null;
    }
    try {
      occupation = widget.supporterSnapshot!.get('occupation');
    } catch (e) {
      null;
    }
    try {
      state = widget.supporterSnapshot!.get('state');
      if (state != null) {
        if (!statesToLGAsToWardsMap.keys.contains(state) || state == '') {
          state = null;
        }
      }
    } catch (e) {
      null;
    }
    try {
      gender = widget.supporterSnapshot!.get('gender');
      if (gender != null) {
        if (!genders.contains(gender) || gender == '') {
          gender = null;
        }
      }
    } catch (e) {
      null;
    }
    try {
      qualification = widget.supporterSnapshot!.get('educationalQualification');
      if (qualification != null) {
        if (!qualifications.contains(qualification) || qualification == '') {
          qualification = null;
        }
      }
    } catch (e) {
      null;
    }
    try {
      lga = widget.supporterSnapshot!.get('localGovernmentArea');
      if (state != null) {
        if (lga != null) {
          if (!statesToLGAsToWardsMap[state]!["lga"]!.contains(lga) ||
              lga == '') {
            lga = null;
          }
        }
      } else {
        lga = null;
      }
    } catch (e) {
      null;
    }
    profilePhotoURL = widget.supporterSnapshot!.get('profilePhotoURL');
    nameController.text = widget.supporterSnapshot!.get('fullName');
    dobController.text = remoteDateOfBirth;
    votersCardNoController.text =
        widget.supporterSnapshot!.get('votersCardNumber');
    addressController.text = widget.supporterSnapshot!.get('address');
    emailController.text = widget.supporterSnapshot!.get('email');
    occupationController.text = occupation;
    phoneController.text = widget.supporterSnapshot!.get('phoneNumber');
    // setState(() {
    //   pageLoaded = true;
    // });
  }

  @override
  void initState() {
    if (!pageLoaded) {
      if (widget.supporterSnapshot != null) {
        getSupportData();
      }
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: "Register"),
      body: pageLoaded
          ? Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Form(
                        key: _formKey,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20.w),
                          child: Column(
                            children: [
                              SizedBox(height: 20.h),
                              Text(
                                'Fill the form to set support data',
                                style: TextStyle(fontSize: 18.sp),
                              ),
                              SizedBox(height: 20.h),
                              Row(
                                children: [
                                  Container(
                                    height: 100.w,
                                    width: 100.w,
                                    color: Colors.grey,
                                    child: widget.supporterSnapshot == null
                                        ? image == null
                                            ? null
                                            : FittedBox(
                                                fit: BoxFit.fill,
                                                child: Image.file(image!),
                                              )
                                        : widget.supporterSnapshot!
                                                    .get('profilePhotoURL') !=
                                                null
                                            ? FittedBox(
                                                fit: BoxFit.fill,
                                                child: Image.network(
                                                  widget.supporterSnapshot!
                                                      .get('profilePhotoURL'),
                                                ),
                                              )
                                            : null,
                                  ),
                                  const Spacer(),
                                  PopupMenuButton(
                                    itemBuilder: (context) => [
                                      PopupMenuItem(
                                          onTap: () {
                                            selectImage(usingCamera: true);
                                          },
                                          child: const Text("Take Photo")),
                                      PopupMenuItem(
                                          onTap: () async {
                                            selectImage();
                                          },
                                          child: const Text("Pick Image"))
                                    ],
                                    child: InkWell(
                                      child: roundedContainer(
                                        buttonColor: Colors.black54,
                                        radius: 10.w,
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 25.w, vertical: 10.h),
                                          child: Text(
                                            image == null
                                                ? 'Capture Photo'
                                                : 'Change Photo',
                                            style: TextStyle(
                                              fontSize: 16.sp,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: nameController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration: customInputDecoration(
                                      hintText: 'Full name (Surname first)'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  keyboardType: TextInputType.phone,
                                  controller: phoneController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration:
                                      customInputDecoration(hintText: 'Phone'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (val) {
                                    if (val!.isNotEmpty) {
                                      if (!isValidEmail(val)) {
                                        return 'Enter valid email';
                                      }
                                    }
                                  },
                                  decoration:
                                      customInputDecoration(hintText: 'Email'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              CustomDropdownWidget(
                                hint: 'Select Gender',
                                initialDropdownValue: gender,
                                items: genders,
                                onChanged: (newValue) {
                                  gender = newValue!;
                                },
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: dobController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                    List<String> dateAsList = val.split('/');
                                    if (dateAsList.length < 3) {
                                      return "Date must be in format DD/MM/YYYY e.g: 01/12/1980";
                                    } else {
                                      try {
                                        dateOfBirth = DateTime(
                                            int.parse(dateAsList[2]),
                                            int.parse(dateAsList[1]),
                                            int.parse(dateAsList[0]));
                                      } catch (e) {
                                        return "Date must be in format DD/MM/YYYY e.g: 01/12/1980";
                                      }
                                    }
                                  },
                                  decoration: customInputDecoration(
                                      hintText: 'DOB (e.g: 01/12/1980)'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              Stack(
                                children: [
                                  CustomDropdownWidget(
                                      hint: 'Select State',
                                      initialDropdownValue: state,
                                      items:
                                          statesToLGAsToWardsMap.keys.toList(),
                                      onChanged: ((newValue) {
                                        setState(() {
                                          state = newValue!;
                                        });
                                      })),
                                  // StateSelectWidget(onChanged: ((newValue) {
                                  //   setState(() {
                                  //     state = newValue!;
                                  //   });
                                  // })),
                                  if (stateSelected)
                                    Positioned.fill(
                                        child: Container(
                                      color: Colors.white54,
                                    ))
                                ],
                              ),
                              SizedBox(height: 15.h),
                              Stack(
                                children: [
                                  CustomDropdownWidget(
                                    hint: 'Select LGA',
                                    initialDropdownValue: lga,
                                    items: state == null
                                        ? statesToLGAsToWardsMap[
                                            "AKWA IBOM STATE"]!["lga"]!
                                        : statesToLGAsToWardsMap[state!]![
                                            "lga"]!,
                                    onChanged: ((newValue) {
                                      lga = newValue!;
                                      setState(() {
                                        stateSelected = true;
                                      });
                                    }),
                                  ),
                                  // LGASelectWidget(
                                  //   onChanged: ((newValue) {
                                  //     lga = newValue!;
                                  //     setState(() {
                                  //       stateSelected = true;
                                  //     });
                                  //   }),
                                  //   state: state == 'Select State'
                                  //       ? "AKWA IBOM STATE"
                                  //       : state,
                                  // ),
                                  if (state == null)
                                    Positioned.fill(
                                        child: InkWell(
                                      onTap: () => customScaffoldMessage(
                                          'Kindly select state first!',
                                          context),
                                      child: Container(
                                        color: Colors.white54,
                                      ),
                                    ))
                                ],
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: votersCardNoController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration: customInputDecoration(
                                      hintText: 'Voters Card No'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: addressController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration: customInputDecoration(
                                      hintText: 'Address'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: wardController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration:
                                      customInputDecoration(hintText: 'Ward'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: unitController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration:
                                      customInputDecoration(hintText: 'Unit'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              CustomDropdownWidget(
                                hint: 'Select Qualification',
                                initialDropdownValue: qualification,
                                items: qualifications,
                                onChanged: (newValue) {
                                  qualification = newValue!;
                                },
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: occupationController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration: customInputDecoration(
                                      hintText: 'Occupation'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              CustomDropdownWidget(
                                hint: 'Employment Status',
                                initialDropdownValue: employmentStatus,
                                items: employmentStatuses,
                                onChanged: (newValue) {
                                  employmentStatus = newValue!;
                                },
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: bankController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration:
                                      customInputDecoration(hintText: 'Bank'),
                                ),
                              ),
                              SizedBox(height: 15.h),
                              textFieldPadding(
                                padding: EdgeInsets.only(left: 10.w),
                                buttonColor: Colors.black,
                                radius: 0,
                                child: TextFormField(
                                  controller: accountNumberController,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Field is empty";
                                    }
                                  },
                                  decoration: customInputDecoration(
                                      hintText: 'Account Number'),
                                ),
                              ),
                              SizedBox(height: 40.h),
                              Container(
                                margin: EdgeInsets.only(bottom: 30.h),
                                // padding: EdgeInsets.symmetric(horizontal: 10.h),
                                alignment: Alignment.bottomCenter,
                                child: filledInButton(
                                  text: 'Save',
                                  radius: 0,
                                  padding: 10.w,
                                  fontSize: 31.sp,
                                  onTap: () async {
                                    SystemChannels.textInput
                                        .invokeMethod('TextInput.hide');
                                    if (_formKey.currentState!.validate()) {
                                      customScaffoldMessage(
                                        widget.supporterSnapshot == null
                                            ? "Registering..."
                                            : "Updating...",
                                        context,
                                        duration: const Duration(hours: 1),
                                      );
                                      bool isConnected =
                                          await checkInternetConnection();
                                      if (isConnected == false) {
                                        final db = Localstore.instance;
                                        // gets new id
                                        final id = getRandomAlphaNumerals();
                                        if (image != null) {
                                          profilePhotoURL =
                                              await saveFileToAppStorage(
                                                  image!);
                                        }
                                        // save the item
                                        db
                                            .collection(widget.userID == null
                                                ? '${widget.hostUID}OfflineGroupSupports'
                                                : '${widget.hostUID}OfflineSupports')
                                            .doc(id)
                                            .set({
                                          'profilePhotoURL': profilePhotoURL,
                                          'fullName': nameController.text,
                                          'dateOfBirth':
                                              dateOfBirth!.toString(),
                                          'state': state,
                                          'gender': gender,
                                          'ward': wardController.text,
                                          'unit': unitController.text,
                                          'localGovernmentArea': lga,
                                          'votersCardNumber':
                                              votersCardNoController.text,
                                          'address': addressController.text,
                                          'email': emailController.text,
                                          'occupation':
                                              occupationController.text,
                                          'educationalQualification':
                                              qualification,
                                          'phoneNumber': phoneController.text,
                                        }).then((value) async {
                                          print('VALUE:');
                                          print(value);
                                          ScaffoldMessenger.of(context)
                                              .hideCurrentSnackBar();
                                          customScaffoldMessage(
                                            'Support saved successfully to Offline Supports',
                                            context,
                                            duration:
                                                const Duration(seconds: 3),
                                          );
                                          Navigator.of(context).pop();
                                          // Navigator.of(context).pop();
                                          final items = await db
                                              .collection(widget.userID == null
                                                  ? '${widget.hostUID}OfflineGroupSupports'
                                                  : '${widget.hostUID}OfflineSupports')
                                              .get();
                                          print(items);
                                        }).catchError((onError) {
                                          ScaffoldMessenger.of(context)
                                              .hideCurrentSnackBar();
                                          customScaffoldMessage(
                                            onError.toString(),
                                            context,
                                            duration:
                                                const Duration(seconds: 3),
                                          );
                                        });
                                      } else {
                                        bool operationSuccessful = false;
                                        if (image != null) {
                                          profilePhotoURL =
                                              await uploadFileToFirebaseStorage(
                                                  image!);
                                        }
                                        String supportUID = '';
                                        if (widget.supporterSnapshot == null) {
                                          supportUID = await DatabaseService()
                                              .addSupportsAccount(
                                                  ward: wardController.text,
                                                  unit: unitController.text,
                                                  profilePhotoURL:
                                                      profilePhotoURL,
                                                  fullName: nameController.text,
                                                  dateOfBirth: dateOfBirth!,
                                                  state: state,
                                                  gender: gender,
                                                  localGovernmentArea: lga,
                                                  votersCardNumber:
                                                      votersCardNoController
                                                          .text,
                                                  address:
                                                      addressController.text,
                                                  email: emailController.text,
                                                  occupation:
                                                      occupationController.text,
                                                  educationalQualification:
                                                      qualification,
                                                  phoneNumber:
                                                      phoneController.text,
                                                  userAssociatedUID:
                                                      widget.userID,
                                                  groupAssociated:
                                                      widget.groupID,
                                                  hostAssociatedUID:
                                                      widget.hostUID,
                                                  bank: bankController.text,
                                                  accountNumber:
                                                      accountNumberController
                                                          .text,
                                                  employmentStatus:
                                                      employmentStatus!);
                                          operationSuccessful =
                                              supportUID != '';
                                        } else {
                                          Map<String, Object?> data = {
                                            'profilePhotoURL': profilePhotoURL,
                                            'fullName': nameController.text,
                                            'dateOfBirth': dateOfBirth!,
                                            'state': state,
                                            'gender': gender,
                                            'localGovernmentArea': lga,
                                            'votersCardNumber':
                                                votersCardNoController.text,
                                            'ward': wardController.text,
                                            'unit': unitController.text,
                                            'address': addressController.text,
                                            'email': emailController.text,
                                            'occupation':
                                                occupationController.text,
                                            'educationalQualification':
                                                qualification,
                                            'phoneNumber': phoneController.text,
                                            'bank': bankController.text,
                                            'accountNumber':
                                                accountNumberController.text,
                                            'employmentStatus':
                                                employmentStatus,
                                          };
                                          operationSuccessful =
                                              await DatabaseService()
                                                  .updateSupportData(
                                            data,
                                            supporterID:
                                                widget.supporterSnapshot!.id,
                                          );
                                        }
                                        ScaffoldMessenger.of(context)
                                            .hideCurrentSnackBar();
                                        if (operationSuccessful) {
                                          if (widget.supporterSnapshot ==
                                              null) {
                                            Navigator.of(context)
                                                .pushReplacement(
                                              MaterialPageRoute(
                                                builder: (BuildContext context) => widget
                                                            .groupID ==
                                                        null
                                                    ? RegistrationCompletePage(
                                                        hostUID: widget.hostUID,
                                                        userID: widget.userID!,
                                                      )
                                                    : GroupRegistrationComplete
                                                        .RegistrationCompletePage(
                                                            groupID:
                                                                widget.groupID!,
                                                            hostUID:
                                                                widget.hostUID),
                                              ),
                                            );
                                          } else {
                                            if (supportUID
                                                .contains('already')) {
                                              customScaffoldMessage(
                                                supportUID,
                                                context,
                                                duration:
                                                    const Duration(seconds: 4),
                                              );
                                            } else {
                                              customScaffoldMessage(
                                                'Update successful!',
                                                context,
                                                duration:
                                                    const Duration(seconds: 3),
                                              );
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            }
                                          }
                                        } else {
                                          customScaffoldMessage(
                                            widget.supporterSnapshot == null
                                                ? 'Registration failed. Try again!'
                                                : 'Update failed. Try again!',
                                            context,
                                            duration:
                                                const Duration(seconds: 3),
                                          );
                                        }
                                      }
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
