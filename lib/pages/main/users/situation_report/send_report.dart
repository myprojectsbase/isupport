// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/users/situation_report/situation_room.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:permission_handler/permission_handler.dart';

class SendReportPage extends StatefulWidget {
  const SendReportPage(
      {super.key,
      required this.isPhotos,
      required this.userAssociated,
      required this.hostAssociated});
  final String userAssociated;
  final String hostAssociated;
  final bool isPhotos;

  @override
  State<SendReportPage> createState() => _SendReportPageState();
}

class _SendReportPageState extends State<SendReportPage> {
  List<File> files = [];
  TextEditingController descriptionController = TextEditingController();
  List<String> imageURLs = [];
  List<String> videoURLs = [];

  void getFiles() async {
    await Permission.storage.request();
    var permissionStatus = await Permission.storage.status;
    if (permissionStatus.isGranted) {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          allowMultiple: true,
          type: widget.isPhotos ? FileType.image : FileType.video);

      if (result != null) {
        files = result.paths.map((path) => File(path!)).toList();
        setState(() {});
        print(files);
      }
    } else {
      customScaffoldMessage(
          'Permission not granted. Kindly grant permission then try again.',
          context);
    }
  }

  Widget imageItem(File file) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15.w)),
      height: 100.w,
      width: 100.w,
      child: FittedBox(
        clipBehavior: Clip.hardEdge,
        fit: BoxFit.fill,
        child: Image.file(file),
      ),
    );
  }

  Widget videoItem(File file) {
    return SizedBox(
      width: 110.w,
      child: Column(
        children: [
          Container(
            decoration:
                BoxDecoration(borderRadius: BorderRadius.circular(15.w)),
            height: 100.w,
            width: 100.w,
            child: VideoThumbnail(file: file),
          ),
          Text(
            file.path.split('/').last,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    getFiles();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Send Report', actions: [
        Padding(
          padding: EdgeInsets.only(right: 20.w),
          child: InkWell(
            onTap: getFiles,
            child: const Icon(Icons.camera_alt),
          ),
        )
      ]),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 15.h),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.h),
                      child: Text(
                        'You have selected (${files.length}) media files',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.sp,
                        ),
                      ),
                    ),
                    Text(
                      'Send situation report to your host',
                      style: TextStyle(
                        fontSize: 18.sp,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 50.h),
                      child: Wrap(
                        spacing: 15.w,
                        runSpacing: 15.w,
                        children: files.map((e) {
                          if (widget.isPhotos) {
                            return imageItem(e);
                          } else {
                            return videoItem(e);
                          }
                        }).toList(),
                      ),
                    ),
                    TextField(
                      controller: descriptionController,
                      maxLines: 5,
                      decoration: const InputDecoration(
                          hintText: 'Add description',
                          border: OutlineInputBorder()),
                    )
                  ],
                ),
              ),
            ),
          ),
          Row(
            children: [
              Flexible(
                flex: 1,
                child: InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 15.h),
                    width: double.infinity,
                    color: Colors.grey.shade400,
                    child: Center(
                        child:
                            Text("Cancel", style: TextStyle(fontSize: 18.sp))),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: InkWell(
                  onTap: () async {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    customScaffoldMessage(
                      'Sending report...',
                      context,
                      duration: const Duration(hours: 1),
                    );
                    for (var element in files) {
                      String fileURL =
                          await uploadFileToFirebaseStorage(element);
                      if (widget.isPhotos) {
                        imageURLs.add(fileURL);
                      } else {
                        videoURLs.add(fileURL);
                      }
                    }
                    bool sendReport = await DatabaseService().addSituation(
                      hostAssociated: widget.hostAssociated,
                      userAssociated: widget.userAssociated,
                      description: descriptionController.text,
                      imageURLs: imageURLs,
                      videoURLs: videoURLs,
                    );
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    if (sendReport) {
                      customScaffoldMessage('Report successful!', context);
                      Navigator.of(context).pop();
                    } else {
                      customScaffoldMessage(
                        'Report unsuccessful :( Please try again.',
                        context,
                      );
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 15.h),
                    width: double.infinity,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        "Send Selected",
                        style: TextStyle(color: Colors.white, fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
