import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/work_station/situation_room/situation_details.dart';
import 'package:isupport/pages/main/users/situation_report/send_report.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import 'package:video_player/video_player.dart';

class SituationRoomPage extends StatefulWidget {
  const SituationRoomPage(
      {super.key, required this.userAssociated, required this.hostAssociated});
  final String userAssociated;
  final String hostAssociated;

  @override
  State<SituationRoomPage> createState() => _SituationRoomPageState();
}

class _SituationRoomPageState extends State<SituationRoomPage> {
  Widget imageContainer(QueryDocumentSnapshot element) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SituationDetailsPage(
            document: element,
            isImages: true,
          ),
        ),
      ),
      child: Container(
        margin: EdgeInsets.only(right: 8.w),
        height: 60.w,
        width: 75.w,
        color: Colors.grey.shade200,
        child: CachedNetworkImage(
          imageUrl: element.get('imageURLs').first,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Situation Room'),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 25.h, horizontal: 25.w),
              child: Text(
                'With situation room you can send live reports to  your host by making videos and pictures.',
                style: TextStyle(fontSize: 18.sp),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 15.h, left: 25.w, right: 25.w),
              child: Text(
                'Take Video',
                style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Text(
                'Simply take a video and give a short description of what is going on in polling centers.',
                style: TextStyle(fontSize: 18.sp),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30.h),
              child: InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => SendReportPage(
                      isPhotos: false,
                      userAssociated: widget.userAssociated,
                      hostAssociated: widget.hostAssociated,
                    ),
                  ),
                ),
                child: Container(
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: darkColor),
                  padding: EdgeInsets.all(15.w),
                  child: Icon(
                    Icons.videocam,
                    color: Colors.white,
                    size: 45.w,
                  ),
                ),
              ),
            ),
            StreamBuilder(
              stream: DatabaseService().getUserSituations(
                  userAssociated: widget.userAssociated,
                  hostAssociated: widget.hostAssociated),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  print(snapshot.data!.docs);
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    dragStartBehavior: DragStartBehavior.down,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: snapshot.data!.docs
                          .where((element) =>
                              (element.get('videoURLs') as List).isNotEmpty)
                          .map(
                            (e) => Container(
                              margin: EdgeInsets.only(right: 8.w),
                              height: 60.w,
                              width: 75.w,
                              color: Colors.grey.shade200,
                              child: VideoThumbnail(
                                document: e,
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  );
                }
                return Container();
              },
            ),
            Container(
              height: 1,
              color: Colors.grey,
              margin: EdgeInsets.symmetric(vertical: 25.h, horizontal: 15.w),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 15.h, left: 25.w, right: 25.w),
              child: Text(
                'Take Photo',
                style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Text(
                'Simply take a photo and give a short description of what is going on in polling centers.',
                style: TextStyle(fontSize: 18.sp),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30.h),
              child: InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => SendReportPage(
                      isPhotos: true,
                      userAssociated: widget.userAssociated,
                      hostAssociated: widget.hostAssociated,
                    ),
                  ),
                ),
                child: Container(
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: darkColor),
                  padding: EdgeInsets.all(15.w),
                  child: Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                    size: 45.w,
                  ),
                ),
              ),
            ),
            StreamBuilder(
              stream: DatabaseService().getUserSituations(
                  userAssociated: widget.userAssociated,
                  hostAssociated: widget.hostAssociated),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    dragStartBehavior: DragStartBehavior.down,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: snapshot.data!.docs
                          .where((element) =>
                              (element.get('imageURLs') as List).isNotEmpty)
                          .map((e) => imageContainer(e))
                          .toList(),
                    ),
                  );
                }
                return Container();
              },
            ),
            SizedBox(height: 20.h)
          ],
        ),
      ),
    );
  }
}

class VideoThumbnail extends StatefulWidget {
  const VideoThumbnail({super.key, this.document, this.file});
  final QueryDocumentSnapshot<Object?>? document;
  final File? file;

  @override
  State<VideoThumbnail> createState() => _VideoThumbnailState();
}

class _VideoThumbnailState extends State<VideoThumbnail> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = widget.file == null
        ? VideoPlayerController.network(
            widget.document!.get('videoURLs').first!)
        : VideoPlayerController.file(widget.file!);
    _controller.initialize().then((_) {
      setState(() {}); //when your thumbnail will show.
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (widget.file == null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SituationDetailsPage(
                document: widget.document!,
                isImages: false,
              ),
            ),
          );
        }
      },
      child: _controller.value.isInitialized
          ? VideoPlayer(_controller)
          : SizedBox(
              width: 50.w,
              height: 50.w,
              child: const Center(child: CupertinoActivityIndicator())),
    );
  }
}
