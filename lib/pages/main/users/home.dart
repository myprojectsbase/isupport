// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/chat/chatrooms.dart';
import 'package:isupport/pages/main/groups/home.dart';
import 'package:isupport/pages/main/help.dart';
import 'package:isupport/pages/main/hosts/manage_agents/users.dart';
import 'package:isupport/pages/main/hosts/work_station/dropbox/dropbox.dart';
import 'package:isupport/pages/main/groups/manage_groups/edit_group.dart';
import 'package:isupport/pages/main/users/situation_report/situation_room.dart';
// import 'package:isupport/pages/main/hosts/manage_supports/supports_details.dart';
import 'package:isupport/pages/registry/register.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/utilities.dart';
import 'package:jiffy/jiffy.dart';
import 'package:localstore/localstore.dart';
import '../../registry/choose.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../chat/chat.dart';
import '../settings/profile/profile.dart';
import '../settings/settings.dart';
import 'manage_supports/register_support.dart';
import 'manage_supports/supports.dart';

class UserHomePage extends StatefulWidget {
  const UserHomePage({Key? key, required this.uid}) : super(key: key);
  final String uid;

  @override
  State<UserHomePage> createState() => _UserHomePageState();
}

class _UserHomePageState extends State<UserHomePage>
    with WidgetsBindingObserver {
  bool pageLoaded = false;
  String? hostBannerImageURL;
  late String hostAssociated;

  Widget dummyHeader() {
    return customScaffold(
      appBar: customAppbar(context),
      child: const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void registerNotifications() async {
    final FirebaseMessaging fcm = FirebaseMessaging.instance;
    // 3. On iOS, this helps to take the user permissions
    NotificationSettings settings = await fcm.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');

      FirebaseMessaging.instance
          .getInitialMessage()
          .then((RemoteMessage? message) {
        if (message != null) {
          print("This should work!!");
          if (message.notification!.body!.contains('New message:  ')) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return ChatPage(
                    title: message.data['senderName'],
                    peerId: message.data['senderID'],
                    id: widget.uid,
                    roomId: message.data['roomID'],
                    senderIsHost: false,
                  );
                },
              ),
            );
          }
        }
        // _serialiseAndNavigate(message);
      });
      // For handling the received notifications
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        // notification came in when user was online
      });
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        print('A new onMessageOpenedApp event was published!');
        print("This should work!!");
        if (message.notification!.body!.contains('New message:  ')) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return ChatPage(
                  title: message.data['senderName'],
                  peerId: message.data['senderID'],
                  id: widget.uid,
                  roomId: message.data['roomID'],
                  senderIsHost: false,
                );
              },
            ),
          );
        }
      });
    } else {
      print('User declined or has not accepted permission');
    }
  }

  void disableIfInactive() async {
    String hostID = await DatabaseService()
        .getSpecificUserData('hostAssociated', userID: widget.uid);
    bool isHostActive = await DatabaseService().checkIfHostIsActive(hostID);
    bool isActive = true;
    try {
      isActive = await DatabaseService()
          .getSpecificUserData('isActive', userID: widget.uid);
    } catch (e) {
      null;
    }
    if (!isHostActive) {
      customScaffoldMessage(
          "Your host's access for this account has expired. Please visit isupport.com.ng for details on how to resubscribe.",
          context,
          duration: const Duration(seconds: 6));
      DatabaseService().updateUserData({'isOnline': false}, userID: widget.uid);
      DatabaseService().checkAndRemovePastTokens();
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => const RegisterOrLogin(),
        ),
      );
    } else {
      if (!isActive) {
        customScaffoldMessage(
            "Your account has been suspended and your access revoked. Kindly contact your host if you feel this was in error. Thank you.",
            context,
            duration: const Duration(seconds: 6));
        DatabaseService()
            .updateUserData({'isOnline': false}, userID: widget.uid);
        DatabaseService().checkAndRemovePastTokens();
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (BuildContext context) => const RegisterOrLogin(),
          ),
        );
      }
    }
  }

  void checkForNewGroupAssociation() async {
    List groupsLeading = [];
    List<DocumentSnapshot> snapshotsOfGroupsToActivate = [];
    try {
      groupsLeading = await DatabaseService().getSpecificUserData(
        'groupsLeading',
        userID: widget.uid,
      );
    } catch (e) {
      null;
    }
    if (groupsLeading.isNotEmpty) {
      for (var element in groupsLeading) {
        print(element);
        DocumentSnapshot groupSnapshot =
            await DatabaseService().getGroupData(groupID: element);
        bool isActivated = true;
        try {
          isActivated = groupSnapshot.get('groupActivated');
        } catch (e) {
          null;
        }
        if (isActivated == false) {
          snapshotsOfGroupsToActivate.add(groupSnapshot);
        }
      }
      if (snapshotsOfGroupsToActivate.isNotEmpty) {
        print(snapshotsOfGroupsToActivate);
        customScaffoldMessage(
            "You have been selected to lead the ${snapshotsOfGroupsToActivate[0].get('groupName')} group. Please fill in the required data to activate the group!",
            context,
            duration: const Duration(seconds: 4));
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) =>
                EditGroupPage(groupSnapshot: snapshotsOfGroupsToActivate[0]),
          ),
        );
      }
    }
  }

  Widget groupChooseWidget(List groupsLeading) {
    Widget groupItem(String groupID) {
      return FutureBuilder(
          future: DatabaseService().getGroupData(groupID: groupID),
          builder: (BuildContext context,
              AsyncSnapshot<DocumentSnapshot<Object?>> groupSnapshot) {
            if (!groupSnapshot.hasData) {
              return Container();
            }
            return Padding(
              padding: EdgeInsets.only(bottom: 10.h),
              child: InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        GroupHomePage(uid: groupID),
                  ),
                ),
                child: roundedContainer(
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 8.h, horizontal: 10.w),
                    child: Stack(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle, color: Color(0XFFC4C4C4)),
                          child: Padding(
                            padding: EdgeInsets.all(8.w),
                            child: groupSnapshot.data!.get('profilePhotoURL') ==
                                    null
                                ? Icon(
                                    Icons.groups,
                                    size: 21.w,
                                    color: Colors.black54,
                                  )
                                : FittedBox(
                                    fit: BoxFit.fill,
                                    child: Image.network(groupSnapshot.data!
                                        .get('profilePhotoURL')),
                                  ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.h, left: 50.w),
                          child: Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              groupSnapshot.data!.get('groupName'),
                              style: TextStyle(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
    }

    return Padding(
      padding: EdgeInsets.all(20.w),
      child: IntrinsicHeight(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.h),
              child: Text(
                'Select a group to access it',
                style: TextStyle(
                  fontSize: 20.sp,
                  color: Colors.black54,
                ),
              ),
            ),
            ...groupsLeading.map((e) => groupItem(e)).toList()
          ],
        ),
      ),
    );
  }

  void uploadOfflineSupportsData() async {
    bool isConnected = await checkInternetConnection();
    if (isConnected) {
      String hostID = await DatabaseService()
          .getSpecificUserData('hostAssociated', userID: widget.uid);
      CollectionRef collectionRef =
          Localstore.instance.collection('${hostID}OfflineSupports');
      final items = await collectionRef.get();
      if (items != null && items != {}) {
        for (var value in items.keys) {
          String? imagePath;
          print(value);
          var doc = items[value];
          if (doc != null) {
            print('Uploading data...');
            String? profilePhotoURL = doc['profilePhotoURL'];
            if (profilePhotoURL != null) {
              imagePath = profilePhotoURL;
              var imageFile = File(profilePhotoURL);
              profilePhotoURL = await uploadFileToFirebaseStorage(imageFile);
            }
            DatabaseService().addSupportsAccount(
              bank: doc['bank'],
              accountNumber: doc['accountNumber'],
              employmentStatus: doc['employmentStatus'],
              ward: doc['ward'],
              unit: doc['unit'],
              address: doc['address'],
              profilePhotoURL: profilePhotoURL,
              fullName: doc['fullName'],
              dateOfBirth: DateTime.parse(doc['dateOfBirth']),
              state: doc['state'],
              gender: doc['gender'],
              localGovernmentArea: doc['localGovernmentArea'],
              votersCardNumber: doc['votersCardNumber'],
              email: doc['email'],
              occupation: doc['occupation'],
              educationalQualification: doc['educationalQualification'],
              phoneNumber: doc['phoneNumber'],
              userAssociatedUID: widget.uid,
              hostAssociatedUID: hostID,
            );
            print('Upload successful. Deleting data...');
            print(doc.values);
            collectionRef
                .doc(value.replaceAll('/${hostID}OfflineSupports/', ''))
                .delete()
                .then((value) {
              if (imagePath != null) {
                final dir = File(imagePath);
                dir.deleteSync();
              }
              print('Done');
            });
          }
        }
      }
    }
  }

  void setNewTheme(int colorCode) {
    setState(() {
      setPrimaryColor(colorCode: colorCode);
    });
  }

  void getThemeFromRemote() async {
    hostAssociated = await DatabaseService()
        .getSpecificUserData('hostAssociated', userID: widget.uid);
    try {
      int? themeColorCode = await DatabaseService()
          .getSpecificHostData("themeColorCode", hostUID: hostAssociated);
      if (themeColorCode != null) {
        setNewTheme(themeColorCode);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    if (pageLoaded == false) {
      getThemeFromRemote();
      disableIfInactive();
      registerNotifications();
      checkForNewGroupAssociation();
      uploadOfflineSupportsData();
      DatabaseService().updateUserData({'isOnline': true}, userID: widget.uid);
      pageLoaded = true;
    }
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      DatabaseService().updateUserData({'isOnline': true}, userID: widget.uid);
    } else {
      DatabaseService().updateUserData({'isOnline': false}, userID: widget.uid);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: DatabaseService().userSnapshots(userID: widget.uid),
      builder: (BuildContext context,
          AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
        if (!snapshot.hasData) {
          return dummyHeader();
        }
        if (!snapshot.data!.exists) {
          DatabaseService().checkAndRemovePastTokens();
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: Text(
                      "Your account has been deleted and your access revoked. Kindly contact your host if you feel this was in error. Thank you.",
                      style: TextStyle(
                        fontSize: 22.sp,
                        color: darkColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 45.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    child: filledInButton(
                      text: 'Exit',
                      padding: 8.w,
                      onTap: () => Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const RegisterOrLogin(),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }
        List groupsLeading = [];
        try {
          groupsLeading = snapshot.data!.get('groupsLeading');
        } catch (e) {
          null;
        }
        Future.delayed(const Duration(seconds: 0), () async {
          try {
            hostBannerImageURL = await DatabaseService().getSpecificHostData(
                'bannerPhotoURL',
                hostUID: snapshot.data!.get('hostAssociated'));
            setState(() {});
          } catch (e) {
            null;
          }
        });
        return customScaffold(
          drawer: customDrawer(context, snapshot),
          appBar: roundedAppbar(context, uid: widget.uid, isHost: false),
          child: SingleChildScrollView(
            child: Column(
              children: [
                hostBannerImageURL == null
                    ? SizedBox(height: 30.h)
                    : Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10.h, bottom: 15.h),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.w),
                          child: Image.network(
                            hostBannerImageURL!,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 75.w,
                      width: 75.w,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey.withOpacity(0.6),
                        image: snapshot.data!.get('profilePhotoURL') == null
                            ? null
                            : DecorationImage(
                                image: CachedNetworkImageProvider(
                                  snapshot.data!.get('profilePhotoURL'),
                                ),
                              ),
                      ),
                      child: snapshot.data!.get('profilePhotoURL') == null
                          ? Padding(
                              padding: EdgeInsets.all(20.w),
                              child: Icon(
                                Icons.person,
                                color: Colors.black54,
                                size: 35.w,
                              ),
                            )
                          : null,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Hi",
                              style: TextStyle(
                                  fontSize: 22.sp, color: Colors.black54)),
                          Text(
                              snapshot.data!.get('fullName') ?? 'No name given',
                              style: TextStyle(
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 1.h,
                  margin: EdgeInsets.only(top: 20.h, bottom: 7.h),
                  color: primaryColor,
                ),
                Row(
                  children: [
                    Text(
                      "Election Date: ",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 14.sp),
                    ),
                    FutureBuilder(
                        future: DatabaseService().getHostData(
                            hostUID: snapshot.data!.get('hostAssociated')),
                        builder: (BuildContext context,
                            AsyncSnapshot<DocumentSnapshot<Object?>>
                                hostSnapshot) {
                          if (!hostSnapshot.hasData) {
                            return Container();
                          }
                          return Text(
                              hostSnapshot.data!.get('nextElectionDate') != null
                                  ? Jiffy((hostSnapshot.data!
                                                  .get('nextElectionDate')
                                              as Timestamp?)!
                                          .toDate())
                                      .format("MMM d yyyy")
                                      .toUpperCase()
                                  : 'Set Election Date',
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 14.sp));
                        }),
                    const Spacer(),
                    Text(
                        Jiffy(DateTime.now())
                            .format("MMM d yyyy")
                            .toUpperCase(),
                        style:
                            TextStyle(color: Colors.black54, fontSize: 14.sp))
                  ],
                ),
                hostBannerImageURL != null
                    ? SizedBox(height: 40.h)
                    : SizedBox(height: 73.h),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  child: Column(
                    children: [
                      Text(
                          "Support are electorates, to bring them on board you need to get their data by registering them.",
                          style:
                              TextStyle(fontSize: 16.sp, color: Colors.black54),
                          textAlign: TextAlign.center),
                      SizedBox(height: 14.h),
                      InkWell(
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                RegisterSupportPage(
                                    userID: widget.uid,
                                    hostUID:
                                        snapshot.data!.get('hostAssociated')),
                          ),
                        ),
                        child: roundedContainer(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 8.h, horizontal: 10.w),
                            child: Stack(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color(0XFFC4C4C4)),
                                  child: Padding(
                                    padding: EdgeInsets.all(8.w),
                                    child: Icon(
                                      Icons.people,
                                      size: 21.w,
                                      color: Colors.black54,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(top: 10.h, left: 10.w),
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "Register Support",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      hostBannerImageURL != null
                          ? SizedBox(height: 40.h)
                          : SizedBox(height: 51.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              roundedContainer(
                                radius: 30.w,
                                child: InkWell(
                                  onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          UsersSupportsPage(
                                              userID: snapshot.data!.id,
                                              hostUID: snapshot.data!
                                                  .get('hostAssociated')),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 40.w, vertical: 15.h),
                                    child: Column(
                                      children: [
                                        Icon(Icons.people, size: 50.w),
                                        Text(
                                            snapshot.data!
                                                .get('supports')
                                                .length
                                                .toString(),
                                            style: TextStyle(
                                                fontSize: 40.sp,
                                                fontWeight: FontWeight.bold))
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 10.h),
                              Text("Total Supports",
                                  style: TextStyle(
                                      fontSize: 16.sp, color: Colors.black54))
                            ],
                          ),
                          SizedBox(width: 30.w),
                          Column(
                            children: [
                              roundedContainer(
                                radius: 30.w,
                                child: InkWell(
                                  onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ChatroomsPage(
                                        id: snapshot.data!.id,
                                        isHost: false,
                                        hostUID: snapshot.data!
                                            .get('hostAssociated'),
                                      ),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 35.h, vertical: 25.w),
                                    child: SizedBox(
                                      height: 75.h,
                                      width: 60.h,
                                      child: Stack(
                                        children: [
                                          Positioned.fill(
                                            child: Icon(
                                              Icons.message,
                                              size: 50.w,
                                              color: primaryColor,
                                            ),
                                          ),
                                          Positioned(
                                            right: 0,
                                            top: 0,
                                            child: StreamBuilder(
                                                stream: FirebaseFirestore
                                                    .instance
                                                    .collection('Rooms')
                                                    .where('members',
                                                        arrayContains:
                                                            widget.uid)
                                                    .snapshots(),
                                                builder: (BuildContext context,
                                                    AsyncSnapshot<
                                                            QuerySnapshot<
                                                                Object?>>
                                                        snapshot) {
                                                  if (snapshot.hasData) {
                                                    if (snapshot
                                                        .data!.docs.isEmpty) {
                                                      return Container();
                                                    }
                                                    int numRoomsWithUnreadMessages =
                                                        0;
                                                    for (var element in snapshot
                                                        .data!.docs) {
                                                      try {
                                                        if (element.get(
                                                                "${widget.uid}_unseen") >
                                                            0) {
                                                          numRoomsWithUnreadMessages++;
                                                        }
                                                      } catch (error) {
                                                        // print('');
                                                      }
                                                    }
                                                    return numRoomsWithUnreadMessages >
                                                            0
                                                        ? Container(
                                                            alignment: Alignment
                                                                .center,
                                                            child:
                                                                IntrinsicWidth(
                                                              child: Container(
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                margin:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        bottom:
                                                                            25),
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(4),
                                                                decoration: const BoxDecoration(
                                                                    color: Colors
                                                                        .red,
                                                                    shape: BoxShape
                                                                        .circle),
                                                                child: Center(
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsets
                                                                        .all(2
                                                                            .w),
                                                                    child: Text(
                                                                      numRoomsWithUnreadMessages
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          const TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        : Container();
                                                  }
                                                  return Container();
                                                }),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 10.h),
                              Text("Messages",
                                  style: TextStyle(
                                      fontSize: 16.sp, color: Colors.black54))
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 30.h),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            groupsLeading.isEmpty
                                ? Container()
                                : Padding(
                                    padding: EdgeInsets.only(right: 30.w),
                                    child: Column(
                                      children: [
                                        Column(
                                          children: [
                                            roundedContainer(
                                              radius: 30.w,
                                              child: InkWell(
                                                onTap: () => customShowDialog(
                                                  context,
                                                  groupChooseWidget(
                                                      groupsLeading),
                                                ),
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 40.w,
                                                      vertical: 15.h),
                                                  child: Column(
                                                    children: [
                                                      Icon(Icons.groups,
                                                          size: 50.w),
                                                      Text(
                                                          groupsLeading.length
                                                              .toString(),
                                                          style: TextStyle(
                                                              fontSize: 40.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 10.h),
                                            Text("Groups you Lead",
                                                style: TextStyle(
                                                    fontSize: 16.sp,
                                                    color: Colors.black54))
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                            Column(
                              children: [
                                roundedContainer(
                                  radius: 30.w,
                                  child: InkWell(
                                    onTap: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            SituationRoomPage(
                                          userAssociated: widget.uid,
                                          hostAssociated: hostAssociated,
                                        ),
                                      ),
                                    ),
                                    child: SizedBox(
                                      height: 123.w,
                                      width: 123.w,
                                      child: Icon(Icons.camera_alt, size: 50.w),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                Text("Situation Room",
                                    style: TextStyle(
                                        fontSize: 16.sp, color: Colors.black54))
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

Drawer customDrawer(
    BuildContext context, AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
  return Drawer(
    backgroundColor: const Color(0xFF444444),
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top + 40.h),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 50.w,
                width: 50.w,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: const Color(0XFFE5E5E5),
                  image: snapshot.data!.get('profilePhotoURL') == null
                      ? null
                      : DecorationImage(
                          image: CachedNetworkImageProvider(
                            snapshot.data!.get('profilePhotoURL'),
                          ),
                        ),
                ),
                child: snapshot.data!.get('profilePhotoURL') == null
                    ? Padding(
                        padding: EdgeInsets.all(10.w),
                        child: Icon(
                          Icons.person,
                          color: Colors.black54,
                          size: 25.w,
                        ),
                      )
                    : null,
              ),
              Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Hi", style: TextStyle(color: Colors.white)),
                    SizedBox(height: 3.h),
                    Text(snapshot.data!.get('fullName') ?? 'No name given',
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)),
                  ],
                ),
              ),
              const Spacer(),
              InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        ProfilePage(snapshot: snapshot.data!, isHost: false),
                  ),
                ),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Icon(Icons.edit, color: Colors.white, size: 16.sp),
                ),
              )
            ],
          ),
          Container(
            height: 1.h,
            margin: EdgeInsets.only(top: 20.h, bottom: 25.h),
            color: Colors.white,
          ),
          drawerItem(
              icon: const Icon(
                Icons.home,
                color: Colors.white,
              ),
              title: 'Dashboard',
              onTap: () => Navigator.of(context).pop()),
          drawerItem(
              icon: const Icon(
                Icons.person,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => UsersSupportsPage(
                          userID: snapshot.data!.id,
                          hostUID: snapshot.data!.get('hostAssociated')),
                    ),
                  ),
              title: 'Supports'),
          drawerItem(
              icon: const Icon(
                Icons.forum,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => ChatroomsPage(
                          id: snapshot.data!.id,
                          isHost: false,
                          hostUID: snapshot.data!.get('hostAssociated')),
                    ),
                  ),
              title: 'Messages'),
          drawerItem(
              icon: const Icon(
                Icons.phone,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => const HelpPage(),
                    ),
                  ),
              title: 'Help'),
          drawerItem(
              icon: const Icon(
                Icons.settings,
                color: Colors.white,
              ),
              onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SettingsPage(
                        snapshot: snapshot.data!,
                        isHost: false,
                      ),
                    ),
                  ),
              title: 'Settings'),
          SizedBox(height: 15.h),
          drawerItem(
            icon: const Icon(
              Icons.logout,
              color: Colors.white,
            ),
            title: 'Log out',
            onTap: () {
              DatabaseService().updateUserData({'isOnline': false},
                  userID: snapshot.data!.id);
              DatabaseService().checkAndRemovePastTokens();
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (BuildContext context) => const RegisterOrLogin(),
                ),
              );
            },
          ),
        ],
      ),
    ),
  );
}

Widget drawerItem(
    {required Icon icon, required String title, void Function()? onTap}) {
  return InkWell(
    onTap: onTap,
    child: Padding(
      padding: EdgeInsets.symmetric(vertical: 19.h),
      child: Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 15.w), child: icon),
          Text(title, style: TextStyle(color: Colors.white, fontSize: 18.sp))
        ],
      ),
    ),
  );
}
