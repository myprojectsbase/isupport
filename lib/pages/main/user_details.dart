import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/users/manage_supports/register_support.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';
import 'package:jiffy/jiffy.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../utils/utilities.dart';

class UserDetails extends StatefulWidget {
  const UserDetails({Key? key, required this.userData, required this.isSupport})
      : super(key: key);
  final bool isSupport;
  final DocumentSnapshot<Object?> userData;

  @override
  State<UserDetails> createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  Future<void> launchURL(String url) async {
    var uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  void openEmail(String emailAddr) {
    String url = "mailto:$emailAddr";
    launchURL(url);
  }

  void openPhone(String phoneNumb) {
    String url = "tel:$phoneNumb";
    launchURL(url);
  }

  Widget body() {
    String dateOfBirth = '';
    String occupation = '';
    String sex = '';
    String qualification = '';
    String ward = '';
    String unit = '';
    String employmentStatus = '';
    String bank = '';
    String accountNumber = '';
    try {
      occupation = widget.userData.get('occupation');
    } catch (e) {
      null;
    }
    try {
      employmentStatus = widget.userData.get('employmentStatus');
    } catch (e) {
      null;
    }
    try {
      bank = widget.userData.get('bank');
    } catch (e) {
      null;
    }
    try {
      accountNumber = widget.userData.get('accountNumber');
    } catch (e) {
      null;
    }
    try {
      ward = widget.userData.get('ward');
    } catch (e) {
      null;
    }
    try {
      unit = widget.userData.get('unit');
    } catch (e) {
      null;
    }
    try {
      sex = widget.userData.get('gender');
    } catch (e) {
      null;
    }
    try {
      qualification = widget.userData.get('educationalQualification');
    } catch (e) {
      null;
    }
    try {
      dateOfBirth = Jiffy(widget.userData.get('dateOfBirth').toDate())
          .format("d-MM-yyyy");
    } catch (e) {
      print(e.toString());
    }
    if (dateOfBirth == '') {
      try {
        dateOfBirth = Jiffy(widget.userData.get('dateOfBirth'), "d/MM/yyyy")
            .format("d-MM-yyyy");
      } catch (e) {
        print(e.toString());
        print('move ahead with empty date');
      }
    }
    return Column(
      children: [
        SizedBox(height: 20.h),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            widget.userData.get('profilePhotoURL') == null
                ? Container(
                    height: 70.w,
                    width: 70.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.w),
                      child: Icon(
                        Icons.person,
                        color: Colors.black54,
                        size: 45.w,
                      ),
                    ),
                  )
                : InkWell(
                    onTap: () => customShowDialog(
                      context,
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.w),
                            image: DecorationImage(
                                image: NetworkImage(
                                    widget.userData.get('profilePhotoURL')),
                                fit: BoxFit.fill)),
                        height: 250.w,
                        width: 250.w,
                      ),
                    ),
                    child: Container(
                      height: 70.w,
                      width: 70.w,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey.withOpacity(0.4),
                        image: DecorationImage(
                          image: NetworkImage(
                            widget.userData.get('profilePhotoURL'),
                          ),
                        ),
                      ),
                      child: null,
                    ),
                  ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.userData.get('fullName') ?? 'No name given',
                      style: TextStyle(
                        fontSize: 22.sp,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    widget.isSupport
                        ? widget.userData.get('userAssociated') != null
                            ? FutureBuilder(
                                future: DatabaseService().getSpecificUserData(
                                    'fullName',
                                    userID:
                                        widget.userData.get('userAssociated')),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData == false) {
                                    return Container();
                                  }
                                  return Padding(
                                    padding: EdgeInsets.only(top: 5.h),
                                    child: Text(
                                        "Registered by: ${snapshot.data}",
                                        style: TextStyle(
                                            fontSize: 18.sp,
                                            color: Colors.black54)),
                                  );
                                })
                            : FutureBuilder(
                                future: DatabaseService().getSpecificGroupData(
                                    'groupName',
                                    groupID:
                                        widget.userData.get('groupAssociated')),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData == false) {
                                    return Container();
                                  }
                                  return Text("Registered by: ${snapshot.data}",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color: Colors.black54));
                                })
                        : Container(),
                  ],
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20.h),
        Row(
          children: [
            Text("State:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Text(widget.userData.get('state') ?? '',
                style: TextStyle(color: Colors.black54, fontSize: 20.sp))
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("LGA:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Expanded(
              child: Text(widget.userData.get('localGovernmentArea') ?? '',
                  style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            )
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("DOB:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Text(dateOfBirth,
                style: TextStyle(color: Colors.black54, fontSize: 20.sp))
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("Voters Card No:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Text(widget.userData.get('votersCardNumber') ?? '',
                style: TextStyle(color: Colors.black54, fontSize: 20.sp))
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("Sex:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            InkWell(
              child: Text(sex,
                  style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            )
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("Email:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Expanded(
              child: InkWell(
                onTap: () => openEmail(widget.userData.get('email')),
                child: Text(
                  widget.userData.get('email') ?? '',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("Phone Number:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            InkWell(
              onTap: () => openPhone(widget.userData.get('phoneNumber')),
              child: Text(widget.userData.get('phoneNumber') ?? '',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold,
                  )),
            )
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("Address:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Expanded(
              child: InkWell(
                child: Text(widget.userData.get('address') ?? '',
                    style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
              ),
            )
          ],
        ),
        SizedBox(height: 13.h),
        widget.isSupport
            ? Column(
                children: [
                  Row(
                    children: [
                      Text("Educational Qualification:  ",
                          style: TextStyle(
                              color: Colors.black54, fontSize: 20.sp)),
                      Expanded(
                        child: InkWell(
                          child: Text(qualification,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 20.sp)),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 13.h),
                  Row(
                    children: [
                      Text("Ward:  ",
                          style: TextStyle(
                              color: Colors.black54, fontSize: 20.sp)),
                      Expanded(
                        child: InkWell(
                          child: Text(ward,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 20.sp)),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 13.h),
                  Row(
                    children: [
                      Text("Unit:  ",
                          style: TextStyle(
                              color: Colors.black54, fontSize: 20.sp)),
                      Expanded(
                        child: InkWell(
                          child: Text(
                            unit,
                            style: TextStyle(
                                color: Colors.black54, fontSize: 20.sp),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 13.h),
                  Row(
                    children: [
                      Text("Occupation:  ",
                          style: TextStyle(
                              color: Colors.black54, fontSize: 20.sp)),
                      Expanded(
                        child: InkWell(
                          child: Text(occupation,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 20.sp)),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 13.h),
                  Row(
                    children: [
                      Text("Employment Status:  ",
                          style: TextStyle(
                              color: Colors.black54, fontSize: 20.sp)),
                      Expanded(
                        child: InkWell(
                          child: Text(employmentStatus,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 20.sp)),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 13.h),
                ],
              )
            : Container(),
        Row(
          children: [
            Text("Bank:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Expanded(
              child: InkWell(
                child: Text(bank,
                    style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
              ),
            )
          ],
        ),
        SizedBox(height: 13.h),
        Row(
          children: [
            Text("Account Number:  ",
                style: TextStyle(color: Colors.black54, fontSize: 20.sp)),
            Expanded(
              child: InkWell(
                onTap: () => openPhone(accountNumber),
                child: Text(
                  accountNumber,
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 13.h),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
        appBar: customAppbar(context,
            title: widget.isSupport ? 'Support Details' : 'Agent Details',
            actions: [
              if (widget.isSupport)
                Padding(
                  padding: EdgeInsets.only(right: 20.w),
                  child: InkWell(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => widget.userData
                                    .get('userAssociated') !=
                                null
                            ? RegisterSupportPage(
                                userID: widget.userData.get('userAssociated'),
                                hostUID: widget.userData.get('hostAssociated'),
                                supporterSnapshot: widget.userData,
                              )
                            : RegisterSupportPage(
                                groupID: widget.userData.get('groupAssociated'),
                                hostUID: widget.userData.get('hostAssociated'),
                                supporterSnapshot: widget.userData,
                              ),
                      ),
                    ),
                    child: const Icon(Icons.edit),
                  ),
                ),
              Padding(
                padding: EdgeInsets.only(right: 20.w),
                child: InkWell(
                  onTap: () {
                    printPage(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          body(),
                        ],
                      ),
                      documentName: 'user-details.pdf',
                      context: context,
                    );
                  },
                  child: const Icon(Icons.print),
                ),
              )
            ]),
        child: ListView(
          children: [
            body(),
            if (widget.isSupport)
              Column(
                children: [
                  SizedBox(height: 20.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: filledInButton(
                        radius: 0,
                        padding: 10.w,
                        fontSize: 18.sp,
                        buttonColor: Colors.red,
                        text: 'Delete Support',
                        onTap: () async {
                          DatabaseService().deleteSupporter(
                            isUserRegistered:
                                widget.userData.get('userAssociated') != null,
                            hostUID: widget.userData.get('hostAssociated'),
                            userID: widget.userData.get('userAssociated') ??
                                widget.userData.get('groupAssociated'),
                            uid: widget.userData.id,
                          );
                          Navigator.of(context).pop();
                        }),
                  ),
                ],
              )
          ],
        ));
  }
}
