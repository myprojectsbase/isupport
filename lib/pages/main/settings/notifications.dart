import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';

import '../../../services/database.dart';
import '../../../utils/constants.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage(
      {Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  bool? notificationsOn;
  bool pageLoaded = false;

  @override
  void initState() {
    if (pageLoaded == false) {
      try {
        notificationsOn = widget.snapshot.get('notificationsOn');
        setState(() {});
      } catch (e) {
        print(e.toString());
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Account Info'),
      body: Stack(children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 35.w),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 50.w),
                child: Text(
                  'You can turn notifications on or off by togging the notification button.',
                  style: TextStyle(fontSize: 18.sp),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Turn off notifications'),
                  Switch(
                    value: notificationsOn != null ? notificationsOn! : true,
                    onChanged: (value) {
                      setState(() {
                        notificationsOn = value;
                      });
                    },
                  )
                ],
              )
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: SizedBox(
            height: 50.h,
            child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
              InkWell(
                onTap: () => Navigator.of(context).pop(),
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  color: Colors.grey[600],
                  child: Center(
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18.sp),
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  if (notificationsOn != null) {
                    print(notificationsOn);
                    customScaffoldMessage('Updating...', context,
                        duration: const Duration(hours: 1));
                    widget.isHost
                        ? DatabaseService().updateHostData(
                            {'notificationsOn': notificationsOn},
                            hostUID: widget.snapshot.id).then((value) {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            customScaffoldMessage(
                                'Notifications successfully updated', context);
                            Navigator.of(context).pop();
                          })
                        : DatabaseService().updateUserData(
                            {'notificationsOn': notificationsOn},
                            userID: widget.snapshot.id).then((value) {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            customScaffoldMessage(
                                'Notifications successfully updated', context);
                            Navigator.of(context).pop();
                          });
                  } else {
                    Navigator.of(context).pop();
                  }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  color: primaryColor,
                  child: Center(
                    child: Text(
                      'Save',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18.sp),
                    ),
                  ),
                ),
              ),
            ]),
          ),
        )
      ]),
    );
  }
}
