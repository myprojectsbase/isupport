import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class SetGenderPage extends StatefulWidget {
  const SetGenderPage({Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<SetGenderPage> createState() => _SetGenderPageState();
}

class _SetGenderPageState extends State<SetGenderPage> {
  String? gender = 'Male';
  bool pageLoaded = false;
  Widget fieldWidget() {
    return Center(
      child: roundedContainer(
        buttonColor: Colors.black54,
        radius: 0,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          child: DropdownButton<String>(
            isDense: true,
            // remove underline
            underline: Container(),

            // Initial Value
            value: gender,

            // Down Arrow Icon
            icon: Padding(
              padding: EdgeInsets.only(right: 10.w),
              child: const Icon(CupertinoIcons.chevron_down),
            ),

            // Array list of items
            items: ['Male', 'Female'].map((String item) {
              return DropdownMenuItem(
                value: item,
                child: Container(
                  padding: EdgeInsets.only(left: 30.w, right: 70.w),
                  child: Text(item,
                      style: TextStyle(fontSize: 21.sp, color: Colors.black54)),
                ),
              );
            }).toList(),
            // After selecting the desired option,it will
            // change button value to selected value
            onChanged: (String? newValue) {
              setState(() {
                gender = newValue!;
              });
            },
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    if (pageLoaded == false) {
      try {
        gender = (widget.snapshot.get('gender') as String?);
      } catch (e) {
        print(e.toString());
      }
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 40.h, horizontal: 35.w),
                    child: const Text(
                      'Edit and save changes to any update you make on your profile',
                      textAlign: TextAlign.center,
                    )),
                SizedBox(height: 10.h),
                fieldWidget(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    customScaffoldMessage('Updating...', context,
                        duration: const Duration(hours: 1));
                    widget.isHost
                        ? DatabaseService().updateHostData({'gender': gender},
                            hostUID: widget.snapshot.id).then((value) {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            customScaffoldMessage(
                                'Gender successfully updated', context);
                            Navigator.of(context).pop();
                          })
                        : DatabaseService().updateUserData({'gender': gender},
                            userID: widget.snapshot.id).then((value) {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            customScaffoldMessage(
                                'Gender successfully updated', context);
                            Navigator.of(context).pop();
                          });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
      appBar: customAppbar(context, title: 'Profile Gender'),
    );
  }
}
