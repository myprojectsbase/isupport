import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:isupport/utils/components.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:image_cropper/image_cropper.dart';
import '../../../../services/database.dart';
import '../../../../utils/constants.dart';

class UploadPage extends StatefulWidget {
  const UploadPage({Key? key, required this.uid, required this.isHost})
      : super(key: key);
  final String? uid;
  final bool isHost;

  @override
  _UploadPageState createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> {
  late String imageUrl;
  File? image;
  bool loading = false;
  bool pageLoadedOnce = false;
  bool warning = false;
  ScrollController scrollController = ScrollController();

  Future<List<File>> getImage({bool allowMultiple = false}) async {
    List<File> imagesAssociated = <File>[];
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: allowMultiple,
        type: FileType.custom,
        allowedExtensions: ['jpg', 'jpeg', 'png', 'svg']);

    if (result != null) {
      imagesAssociated = result.paths.map((path) => File(path!)).toList();
      print(imagesAssociated);
    }
    return imagesAssociated;
  }

  Future linkUploadToUserFirestore() async {
    String downloadURL = await uploadImageToStorage(image!);
    widget.isHost
        ? await DatabaseService().updateHostData(
            {'profilePhotoURL': downloadURL}, hostUID: widget.uid)
        : await DatabaseService().updateUserData(
            {'profilePhotoURL': downloadURL},
            userID: widget.uid!);
  }

  Future<String> uploadImageToStorage(File image) async {
    //Set File Name
    String fileName = image.path.split('/').last;

    //Create Reference
    Reference reference =
        FirebaseStorage.instance.ref().child('images').child(fileName);

    //Now We have to check the status of UploadTask
    UploadTask uploadTask = reference.putFile(image);

    late String url;
    await uploadTask.whenComplete(() async {
      url = await uploadTask.snapshot.ref.getDownloadURL();
    });

    return url;
  }

  Future<CroppedFile?> getImageCropped(File image) async {
    int maxImageHeight = 250;
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      maxHeight: maxImageHeight,
      maxWidth: maxImageHeight,
      compressQuality: 100,
      sourcePath: image.path,
      aspectRatio: const CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Crop Image',
            toolbarColor: primaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original),
        IOSUiSettings(
          aspectRatioLockEnabled: true,
          minimumAspectRatio: 1.0,
        )
      ],
    );
    return croppedFile;
  }

  selectImage({bool usingCamera = false}) async {
    await Permission.storage.request();
    File? imageSelected;
    var permissionStatus = await Permission.storage.status;

    if (permissionStatus.isGranted) {
      if (usingCamera) {
        final ImagePicker _picker = ImagePicker();
        // Capture a photo
        final XFile? photo =
            await _picker.pickImage(source: ImageSource.camera);
        imageSelected = File(photo!.path);
      } else {
        //Select Image from galery
        List<File> results = await getImage();
        imageSelected = results.first;
      }
      CroppedFile? croppedImage = await getImageCropped(imageSelected);
      setState(() {
        image = File(croppedImage!.path);
      });
    } else {
      customScaffoldMessage(
          'Permission not granted. Try Again with permission access', context);
    }
  }

  Widget cameraOrFilePopup({required String buttonTitle}) {
    return PopupMenuButton(
      itemBuilder: (context) => [
        PopupMenuItem(
            onTap: () {
              selectImage(usingCamera: true);
            },
            child: const Text("Take Photo")),
        PopupMenuItem(
            onTap: () async {
              selectImage();
            },
            child: const Text("Pick Image"))
      ],
      child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
                return primaryColor; // Use the component's default.
              },
            ),
          ),
          onPressed: null,
          child: Text(
            buttonTitle,
            style: const TextStyle(color: Colors.white),
          )),
    );
  }

  Widget imageUploadWidget() {
    double imageSize = MediaQuery.of(context).size.width / 1.5;
    return SingleChildScrollView(
      child: Column(
        children: [
          (image == null)
              ? Column(
                  children: [
                    warning
                        ? const Center(
                            child: Text(
                              "Select an image",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red),
                            ),
                          )
                        : Container(),
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 10,
                                color: warning
                                    ? Colors.red
                                    : const Color(0xFFFFF2DC))),
                        height: imageSize,
                        width: imageSize,
                        child: Center(
                          child: cameraOrFilePopup(buttonTitle: 'Select Image'),
                        )),
                  ],
                )
              : Column(
                  children: [
                    Image.file(image!),
                    const SizedBox(height: 10),
                    cameraOrFilePopup(buttonTitle: 'Change Image')
                  ],
                ),
          const SizedBox(height: 40),
          // next line makes the upload button always visible if it's a business post
          image != null
              ? Container(
                  height: 50.h,
                  width: 200.w,
                  margin: EdgeInsets.only(bottom: 50.h),
                  child: filledInButton(
                    padding: 15.w,
                    text: "Upload",
                    fontSize: 16.sp,
                    onTap: () async {
                      if (image == null) {
                        setState(() {
                          warning = true;
                        });
                      } else {
                        setState(() {
                          loading = true;
                        });
                        await linkUploadToUserFirestore().then((value) {
                          Navigator.of(context).pop();
                          setState(() {});
                        }).catchError((onError) {
                          // customScaffoldMessage(onError.toString(), context,
                          //     devoidScaffold: true);
                        });
                      }
                    },
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Widget profilePictureUploadWidget() {
    Widget heading = const Text(
      "Choose Profile Image",
      style: TextStyle(color: Colors.white),
    );
    Widget loadingHeading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Text(
          "Uploading ",
          style: TextStyle(color: Colors.white),
        ),
        SizedBox(width: 15),
        SizedBox(
            height: 15,
            width: 15,
            child: CircularProgressIndicator(value: null, color: Colors.white))
      ],
    );
    return IntrinsicHeight(
      child: Column(
        children: [
          Container(
            height: 50,
            color: primaryColor,
            child: Center(
              child: loading ? loadingHeading : heading,
            ),
          ),
          Expanded(
            child: Padding(
                padding: const EdgeInsets.all(20), child: imageUploadWidget()),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    if (!pageLoadedOnce) {
      pageLoadedOnce = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return profilePictureUploadWidget();
  }
}
