import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class SetNamePage extends StatefulWidget {
  const SetNamePage({Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<SetNamePage> createState() => _SetNamePageState();
}

class _SetNamePageState extends State<SetNamePage> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController middleNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  late String? fullName;
  bool pageLoaded = false;
  Widget fieldWidget(
      {required String title, required TextEditingController controller}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyle(fontSize: 16.sp)),
          SizedBox(height: 10.h),
          textFieldPadding(
            padding: EdgeInsets.only(left: 10.w),
            buttonColor: Colors.grey,
            radius: 0,
            child: TextFormField(
              controller: controller,
              validator: (val) {
                if (val!.isEmpty) {
                  return "Field is empty";
                }
              },
              decoration: customInputDecoration(),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    if (pageLoaded == false) {
      fullName = widget.snapshot.get('fullName');
      if (fullName != null && fullName != '') {
        fullName = fullName!.trim();
        List<String> fullNameAsList = fullName!.split(' ');
        print(fullNameAsList);
        if (fullNameAsList.length < 4) {
          switch (fullNameAsList.length) {
            case 1:
              firstNameController.text = fullName!;
              break;
            case 2:
              firstNameController.text = fullNameAsList[0];
              lastNameController.text = fullNameAsList[1];
              break;
            case 3:
              firstNameController.text = fullNameAsList[0];
              middleNameController.text = fullNameAsList[1];
              lastNameController.text = fullNameAsList[2];
              break;
          }
        } else {
          firstNameController.text = fullName!;
        }
      }
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 40.h, horizontal: 35.w),
                    child: const Text(
                      'Edit and save changes to any update you make on your profile',
                      textAlign: TextAlign.center,
                    )),
                SizedBox(height: 10.h),
                fieldWidget(
                    title: 'First Name', controller: firstNameController),
                fieldWidget(
                    title: 'Middle Name', controller: middleNameController),
                fieldWidget(title: 'Last Name', controller: lastNameController),
                SizedBox(height: 80.h)
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    String newName = '';
                    if (firstNameController.text.isNotEmpty) {
                      newName += firstNameController.text;
                    }
                    if (middleNameController.text.isNotEmpty) {
                      newName += ' ' + middleNameController.text;
                    }
                    if (lastNameController.text.isNotEmpty) {
                      newName += ' ' + lastNameController.text;
                    }
                    if (fullName != newName) {
                      print(newName);
                      customScaffoldMessage('Updating...', context,
                          duration: const Duration(hours: 1));
                      widget.isHost
                          ? DatabaseService().updateHostData(
                              {'fullName': newName},
                              hostUID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Name successfully updated', context);
                              Navigator.of(context).pop();
                            })
                          : DatabaseService().updateUserData(
                              {'fullName': newName},
                              userID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Name successfully updated', context);
                              Navigator.of(context).pop();
                            });
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
      appBar: customAppbar(context, title: 'Profile Name'),
    );
  }
}
