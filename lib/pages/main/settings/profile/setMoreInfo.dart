import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class SetMoreInfoPage extends StatefulWidget {
  const SetMoreInfoPage({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<SetMoreInfoPage> createState() => _SetMoreInfoPageState();
}

class _SetMoreInfoPageState extends State<SetMoreInfoPage> {
  TextEditingController stateController = TextEditingController();
  TextEditingController lgaController = TextEditingController();
  TextEditingController votersCardNumberController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController bankController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  bool pageLoaded = false;
  Widget fieldWidget(
      {required String title, required TextEditingController controller}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyle(fontSize: 16.sp)),
          SizedBox(height: 10.h),
          textFieldPadding(
            padding: EdgeInsets.only(left: 10.w),
            buttonColor: Colors.grey,
            radius: 0,
            child: TextFormField(
              controller: controller,
              validator: (val) {
                if (val!.isEmpty) {
                  return "Field is empty";
                }
              },
              decoration: customInputDecoration(),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    if (pageLoaded == false) {
      stateController.text = widget.snapshot.get('state') ?? '';
      lgaController.text = widget.snapshot.get('localGovernmentArea') ?? '';
      votersCardNumberController.text =
          widget.snapshot.get('votersCardNumber') ?? '';
      addressController.text = widget.snapshot.get('address') ?? '';
      try {
        accountNumberController.text =
            widget.snapshot.get('accountNumber') ?? '';
        bankController.text = widget.snapshot.get('bank') ?? '';
      } catch (e) {
        null;
      }
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.h, horizontal: 35.w),
                    child: const Text(
                      'Edit and save changes to any update you make on your profile',
                      textAlign: TextAlign.center,
                    )),
                fieldWidget(title: 'State', controller: stateController),
                fieldWidget(title: 'LGA', controller: lgaController),
                fieldWidget(
                    title: 'Voters Card Number',
                    controller: votersCardNumberController),
                fieldWidget(title: 'Address', controller: addressController),
                fieldWidget(title: 'Bank', controller: bankController),
                fieldWidget(
                    title: 'Account Number',
                    controller: accountNumberController),
                SizedBox(height: 80.h)
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    customScaffoldMessage('Updating...', context,
                        duration: const Duration(hours: 1));
                    DatabaseService().updateUserData({
                      'localGovernmentArea': lgaController.text,
                      'state': stateController.text,
                      'votersCardNumber': votersCardNumberController.text,
                      'address': addressController.text,
                      'bank': bankController.text,
                      'accountNumber': accountNumberController.text,
                    }, userID: widget.snapshot.id).then((value) {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      customScaffoldMessage(
                          'Info successfully updated', context);
                      Navigator.of(context).pop();
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
      appBar: customAppbar(context, title: 'More Information'),
    );
  }
}
