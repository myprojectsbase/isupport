import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';

import '../../../../utils/constants.dart';

class SetBirthdayPage extends StatefulWidget {
  const SetBirthdayPage(
      {Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<SetBirthdayPage> createState() => _SetBirthdayPageState();
}

class _SetBirthdayPageState extends State<SetBirthdayPage> {
  DateTime? selectedDate;

  @override
  void initState() {
    Timestamp? savedDate;
    try {
      savedDate = (widget.snapshot.get('dateOfBirth') as Timestamp?);
    } catch (e) {
      print(e.toString());
    }
    if (savedDate != null) {
      selectedDate = savedDate.toDate();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Profile Birthday'),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.h, vertical: 50.w),
                  child: Text(
                    'You can use the date picker to set your date of birth.',
                    style: TextStyle(fontSize: 18.sp),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 100.h),
                SizedBox(
                  height: 200.h,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    initialDateTime: selectedDate ?? DateTime(1960),
                    minimumDate: DateTime(1950),
                    maximumDate: DateTime(2004),
                    onDateTimeChanged: (newDate) {
                      selectedDate = newDate;
                    },
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    if (selectedDate != null) {
                      print(selectedDate);
                      customScaffoldMessage('Updating...', context,
                          duration: const Duration(hours: 1));
                      widget.isHost
                          ? DatabaseService().updateHostData(
                              {'dateOfBirth': selectedDate},
                              hostUID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Date of birth successfully updated',
                                  context);
                              Navigator.of(context).pop();
                            })
                          : DatabaseService().updateUserData(
                              {'dateOfBirth': selectedDate},
                              userID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Date of birth successfully updated',
                                  context);
                              Navigator.of(context).pop();
                            });
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
    );
  }
}
