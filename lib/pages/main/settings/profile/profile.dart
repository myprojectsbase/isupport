import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/settings/profile/setBirthday.dart';
import 'package:isupport/pages/main/settings/profile/setEmail.dart';
import 'package:isupport/pages/main/settings/profile/setGender.dart';
import 'package:isupport/pages/main/settings/profile/setMoreInfo.dart';
import 'package:isupport/pages/main/settings/profile/setName.dart';
import 'package:isupport/pages/main/settings/profile/upload.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';
import 'package:jiffy/jiffy.dart';

import '../../../../../services/database.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Widget itemWidget(
      {required String title,
      required String body,
      required Function() onTap}) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 18.sp),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      body,
                      style: TextStyle(fontSize: 11.sp, color: Colors.grey),
                    ),
                    SizedBox(width: 2.w),
                    const Icon(
                      CupertinoIcons.chevron_forward,
                      color: Colors.black54,
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 0.5.w,
            color: Colors.grey,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Edit Profile'),
      body: StreamBuilder(
          stream: widget.isHost
              ? DatabaseService().hostSnapshots(hostUID: widget.snapshot.id)
              : DatabaseService().userSnapshots(userID: widget.snapshot.id),
          builder: (BuildContext context,
              AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            String? gender = 'Set Gender';
            Timestamp? dateOfBirth;
            try {
              gender = (snapshot.data!.get('gender') as String?);
            } catch (e) {
              print('no gender field');
            }
            try {
              dateOfBirth = (snapshot.data!.get('dateOfBirth') as Timestamp?);
            } catch (e) {
              print('no dateOfBirth field');
            }
            return Column(
              children: [
                SizedBox(height: 30.h),
                Center(
                  child: InkWell(
                    onTap: () => customShowDialog(
                      context,
                      UploadPage(uid: snapshot.data!.id, isHost: widget.isHost),
                    ),
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: snapshot.data!.get('profilePhotoURL') == null
                                ? Colors.grey[300]
                                : primaryColor,
                            shape: BoxShape.circle,
                          ),
                          child: Container(
                            height: 100.w,
                            width: 100.w,
                            margin: EdgeInsets.all(3.w),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image:
                                  snapshot.data!.get('profilePhotoURL') == null
                                      ? null
                                      : DecorationImage(
                                          image: CachedNetworkImageProvider(
                                              snapshot.data!
                                                  .get('profilePhotoURL'))),
                              border:
                                  Border.all(color: Colors.grey, width: 1.w),
                            ),
                            padding:
                                snapshot.data!.get('profilePhotoURL') == null
                                    ? EdgeInsets.all(20.h)
                                    : null,
                            child: snapshot.data!.get('profilePhotoURL') == null
                                ? Icon(
                                    Icons.person,
                                    color: Colors.black54,
                                    size: 40.w,
                                  )
                                : Container(),
                          ),
                        ),
                        SizedBox(height: 10.h),
                        Text(
                          'Edit Photo',
                          style: TextStyle(fontSize: 16.sp),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 1.h,
                  color: Colors.grey,
                  margin: EdgeInsets.only(top: 20.h, bottom: 40.h),
                ),
                itemWidget(
                  title: 'Account Info',
                  body: snapshot.data!.get('email') ?? 'Set Email',
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SetEmailPage(
                          snapshot: snapshot.data!, isHost: widget.isHost),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Name',
                  body: snapshot.data!.get('fullName') ?? 'Set Name',
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SetNamePage(
                          snapshot: snapshot.data!, isHost: widget.isHost),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Gender',
                  body: gender ?? 'Set Gender',
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SetGenderPage(
                          snapshot: snapshot.data!, isHost: widget.isHost),
                    ),
                  ),
                ),
                itemWidget(
                  title: 'Birthday',
                  body: dateOfBirth == null
                      ? 'Set Birthday'
                      : Jiffy(dateOfBirth.toDate()).format("d-MM-yyyy"),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SetBirthdayPage(
                          snapshot: snapshot.data!, isHost: widget.isHost),
                    ),
                  ),
                ),
                if (!widget.isHost)
                  itemWidget(
                    title: 'Set More Info',
                    body: '(Set state, account number, etc.)',
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            SetMoreInfoPage(snapshot: snapshot.data!),
                      ),
                    ),
                  ),
              ],
            );
          }),
    );
  }
}
