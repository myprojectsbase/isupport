import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class SetEmailPage extends StatefulWidget {
  const SetEmailPage({Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<SetEmailPage> createState() => _SetEmailPageState();
}

class _SetEmailPageState extends State<SetEmailPage> {
  TextEditingController emailController = TextEditingController();
  late String? email;
  bool pageLoaded = false;
  Widget fieldWidget(
      {required String title, required TextEditingController controller}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyle(fontSize: 16.sp)),
          SizedBox(height: 10.h),
          textFieldPadding(
            padding: EdgeInsets.only(left: 10.w),
            buttonColor: Colors.grey,
            radius: 0,
            child: TextFormField(
              controller: controller,
              validator: (val) {
                if (val!.isEmpty) {
                  return "Field is empty";
                }
              },
              decoration: customInputDecoration(),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    if (pageLoaded == false) {
      email = widget.snapshot.get('email');
      if (email != null && email != '') {
        emailController.text = email!;
      }
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 40.h, horizontal: 35.w),
                    child: const Text(
                      'Edit and save changes to any update you make on your profile',
                      textAlign: TextAlign.center,
                    )),
                SizedBox(height: 30.h),
                fieldWidget(title: 'Email', controller: emailController),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    String newEmail = emailController.text;
                    if (email != newEmail) {
                      print(newEmail);
                      customScaffoldMessage('Updating...', context,
                          duration: const Duration(hours: 1));
                      widget.isHost
                          ? DatabaseService().updateHostData(
                              {'email': newEmail},
                              hostUID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Email successfully updated', context);
                              Navigator.of(context).pop();
                            })
                          : DatabaseService().updateUserData(
                              {'email': newEmail},
                              userID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Email successfully updated', context);
                              Navigator.of(context).pop();
                            });
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
      appBar: customAppbar(context, title: 'Profile Email'),
    );
  }
}
