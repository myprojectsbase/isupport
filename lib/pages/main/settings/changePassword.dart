import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';
import 'package:isupport/utils/constants.dart';

import '../../../../../services/database.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage(
      {Key? key, required this.snapshot, required this.isHost})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  TextEditingController currentPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController repeatPasswordController = TextEditingController();
  ScrollController scrollController = ScrollController();
  late String? password;
  bool pageLoaded = false;
  final _formKey = GlobalKey<FormState>();
  Widget fieldWidget({
    required String title,
    required TextEditingController controller,
    bool isRepeat = false,
    isCurrent = false,
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyle(fontSize: 16.sp)),
          SizedBox(height: 10.h),
          textFieldPadding(
            padding: EdgeInsets.only(left: 10.w),
            buttonColor: Colors.grey,
            radius: 0,
            child: TextFormField(
              onTap: () {
                final position = scrollController.position.maxScrollExtent;
                scrollController.animateTo(
                  position,
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.easeOut,
                );
              },
              controller: controller,
              validator: (val) {
                if (val!.isEmpty) {
                  return "Field is empty";
                } else {
                  if (val.length < 8) {
                    return "Password can't have less than 8 characters!";
                  } else {
                    if (isRepeat) {
                      if (val != newPasswordController.text) {
                        return "Value does not match with new password";
                      }
                    }
                    if (isCurrent) {
                      if (val != password && val != '12345678') {
                        return "Please put in the correct current password";
                      }
                    }
                  }
                }
              },
              decoration: customInputDecoration(),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    if (pageLoaded == false) {
      password = widget.snapshot.get('password');
      print(password);
      pageLoaded = true;
      setState(() {});
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: scrollController,
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 40.h, horizontal: 35.w),
                      child: const Text(
                        'Edit and save changes to any update you make on your profile',
                        textAlign: TextAlign.center,
                      )),
                  SizedBox(height: 10.h),
                  fieldWidget(
                      title: 'Current Password',
                      controller: currentPasswordController,
                      isCurrent: true),
                  fieldWidget(
                      title: 'New Password', controller: newPasswordController),
                  fieldWidget(
                      title: 'Repeat Password',
                      controller: repeatPasswordController,
                      isRepeat: true),
                  SizedBox(height: 80.h)
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    if (_formKey.currentState!.validate()) {
                      customScaffoldMessage('Updating...', context,
                          duration: const Duration(hours: 1));
                      widget.isHost
                          ? DatabaseService().updateHostData(
                              {'password': repeatPasswordController.text},
                              hostUID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Password successfully updated', context);
                              Navigator.of(context).pop();
                            })
                          : DatabaseService().updateUserData(
                              {'password': repeatPasswordController.text},
                              userID: widget.snapshot.id).then((value) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              customScaffoldMessage(
                                  'Password successfully updated', context);
                              Navigator.of(context).pop();
                            });
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
      appBar: customAppbar(context, title: 'Change Password'),
    );
  }
}
