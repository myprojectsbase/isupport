import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/settings/changePassword.dart';
import 'package:isupport/pages/main/settings/notifications.dart';
import 'package:isupport/pages/main/settings/profile/profile.dart';
import 'package:isupport/pages/main/settings/setElectionDate.dart';
import 'package:isupport/pages/registry/pick_color.dart';
import 'package:isupport/utils/components.dart';

import '../../../services/database.dart';
import '../../registry/choose.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage(
      {Key? key,
      required this.snapshot,
      required this.isHost,
      this.changeHomepageThemeColor})
      : super(key: key);
  final DocumentSnapshot snapshot;
  final bool isHost;
  final Function(int colorCode)? changeHomepageThemeColor;

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

Widget settingsItem({required String title, required Function() onTap}) {
  return InkWell(
    onTap: onTap,
    child: Container(
      width: double.infinity,
      color: Colors.white,
      margin: EdgeInsets.only(top: 10.h),
      padding: EdgeInsets.all(22.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 18.sp,
              color: Colors.black54,
            ),
          ),
          const Icon(
            CupertinoIcons.chevron_forward,
            color: Colors.black54,
          )
        ],
      ),
    ),
  );
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200]!,
      appBar: customAppbar(context, title: 'Settings'),
      body: StreamBuilder(
          stream: widget.isHost
              ? DatabaseService().hostSnapshots(hostUID: widget.snapshot.id)
              : DatabaseService().userSnapshots(userID: widget.snapshot.id),
          builder: (BuildContext context,
              AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return Column(
              children: [
                settingsItem(
                  title: 'Profile',
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => ProfilePage(
                          snapshot: snapshot.data!, isHost: widget.isHost),
                    ),
                  ),
                ),
                if (widget.isHost)
                  settingsItem(
                    title: 'Change Appearance',
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => PickColorPage(
                          changeHomepageThemeColor: ((colorCode) {
                            setState(() {
                              widget.changeHomepageThemeColor!(colorCode);
                            });
                          }),
                          uid: widget.snapshot.id,
                        ),
                      ),
                    ),
                  ),
                settingsItem(
                  title: 'Notification Settings',
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => NotificationsPage(
                          snapshot: snapshot.data!, isHost: widget.isHost),
                    ),
                  ),
                ),
                widget.isHost
                    ? settingsItem(
                        title: 'Set Election Date',
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                SetElectionDate(snapshot: snapshot.data!),
                          ),
                        ),
                      )
                    : Container(),
                Container(
                  width: double.infinity,
                  color: Colors.white,
                  margin: EdgeInsets.only(top: 10.h),
                  padding: EdgeInsets.all(25.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Security',
                        style: TextStyle(
                          fontSize: 18.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black54,
                        ),
                      ),
                      SizedBox(height: 30.h),
                      InkWell(
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ChangePasswordPage(
                                    snapshot: snapshot.data!,
                                    isHost: widget.isHost),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Change Password',
                              style: TextStyle(
                                fontSize: 18.sp,
                                color: Colors.black54,
                              ),
                            ),
                            const Icon(
                              CupertinoIcons.chevron_forward,
                              color: Colors.black54,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30.h),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Version',
                          style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black54,
                          )),
                      Text('1.0.0',
                          style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black54,
                          ))
                    ],
                  ),
                ),
                SizedBox(height: 30.h),
                Padding(
                  padding: EdgeInsets.all(40.w),
                  child: filledInButton(
                    text: 'Sign Out',
                    buttonColor: Colors.grey,
                    fontSize: 20.sp,
                    onTap: () {
                      widget.isHost
                          ? DatabaseService().updateHostData(
                              {'isOnline': false}, hostUID: snapshot.data!.id)
                          : DatabaseService().updateUserData(
                              {'isOnline': false},
                              userID: snapshot.data!.id);
                      DatabaseService().checkAndRemovePastTokens();
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const RegisterOrLogin(),
                        ),
                      );
                    },
                  ),
                ),
                Column(
                  children: [
                    Text(
                      '©2022 isupport.com.ng',
                      style: TextStyle(
                        fontSize: 11.sp,
                        color: Colors.black54,
                      ),
                    ),
                    Text(
                      'All rights reserved',
                      style: TextStyle(
                        fontSize: 11.sp,
                        color: Colors.black54,
                      ),
                    ),
                  ],
                )
              ],
            );
          }),
    );
  }
}
