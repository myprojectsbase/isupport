import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';

import '../../../../utils/constants.dart';

class SetElectionDate extends StatefulWidget {
  const SetElectionDate({Key? key, required this.snapshot}) : super(key: key);
  final DocumentSnapshot snapshot;

  @override
  State<SetElectionDate> createState() => _SetElectionDateState();
}

class _SetElectionDateState extends State<SetElectionDate> {
  DateTime? selectedDate;

  @override
  void initState() {
    Timestamp? savedDate;
    try {
      savedDate = (widget.snapshot.get('nextElectionDate') as Timestamp?);
    } catch (e) {
      print(e.toString());
    }
    if (savedDate != null) {
      selectedDate = savedDate.toDate();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Account Info'),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.h, vertical: 50.w),
                  child: Text(
                    'You can use the date picker to set election date.',
                    style: TextStyle(fontSize: 18.sp),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 100.h),
                SizedBox(
                  height: 200.h,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    initialDateTime: selectedDate ?? DateTime(2022),
                    minimumDate: DateTime(2022),
                    maximumDate: DateTime(2050),
                    onDateTimeChanged: (newDate) {
                      selectedDate = newDate;
                    },
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 50.h,
              child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.grey[600],
                    child: Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    if (selectedDate != null) {
                      print(selectedDate);
                      customScaffoldMessage('Updating...', context,
                          duration: const Duration(hours: 1));
                      DatabaseService().updateHostData(
                          {'nextElectionDate': selectedDate},
                          hostUID: widget.snapshot.id).then((value) {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        customScaffoldMessage(
                            'Election date successfully updated', context);
                        Navigator.of(context).pop();
                      });
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          )
        ],
      ),
    );
  }
}
