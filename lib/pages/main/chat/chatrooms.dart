import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/chat/select_host.dart';
import 'package:isupport/pages/main/chat/select_user.dart';
import 'package:isupport/utils/components.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:isupport/utils/constants.dart';
import 'package:jiffy/jiffy.dart';
import '../../../../services/database.dart';
import 'chat.dart';

class ChatroomsPage extends StatefulWidget {
  const ChatroomsPage(
      {Key? key,
      required this.id,
      required this.isHost,
      this.hostUID,
      this.hostSnapshot})
      : super(key: key);

  final String id;
  final bool isHost;
  final String? hostUID;
  final DocumentSnapshot? hostSnapshot;

  @override
  _ChatroomsPageState createState() => _ChatroomsPageState();
}

class _ChatroomsPageState extends State<ChatroomsPage> {
  List listMessage = [];
  ScrollController listScrollController = ScrollController();
  Map peerData = {};
  bool doneLoading = false;
  TextEditingController searchController = TextEditingController();

  Widget buildItem(DocumentSnapshot snapshot) {
    List members = snapshot.get('members');
    print(members);
    String peerId = members[0] == widget.id ? members[1] : members[0];
    String roomId = snapshot.id;
    bool senderIsHost = widget.id == snapshot.get("hostUID");
    return StreamBuilder(
        stream: senderIsHost
            ? DatabaseService().userSnapshots(userID: peerId)
            : DatabaseService().hostSnapshots(hostUID: peerId),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Object?>> peerSnapshot) {
          if (peerSnapshot.hasData) {
            if (!peerSnapshot.data!.exists) {
              return Container();
            }
            int numUnreadMessages = 0;
            String lastMessage = snapshot.get("lastMessage");
            bool lastMessageHasImage = snapshot.get("lastMessageHasImage");
            if (lastMessageHasImage) {
              if (lastMessage.isEmpty) {
                lastMessage = "Image  📷";
              } else {
                lastMessage += "  •  Image  📷";
              }
            }
            String lastMessageSender = snapshot.get("lastMessageSender");
            if (lastMessageSender == widget.id) {
              lastMessage = "You: $lastMessage";
            }
            bool isOnline = false;
            // so older versions don't break
            try {
              isOnline = peerSnapshot.data!['isOnline'];
            } catch (error) {
              // print("there was error");
            }
            try {
              numUnreadMessages = snapshot.get("${widget.id}_unseen");
            } catch (error) {
              // print("there was error");
            }
            return RoomWidget(
              id: widget.id,
              roomId: roomId,
              peerId: peerId,
              peerName: peerSnapshot.data!['fullName'] ??
                  peerSnapshot.data!['phoneNumber'] + ' (No name)',
              peerPhotoURL: peerSnapshot.data!['profilePhotoURL'] ?? '',
              isOnline: isOnline,
              lastMessage: lastMessage,
              numUnreadMessages: numUnreadMessages,
              senderIsHost: senderIsHost,
              timeString:
                  Jiffy((snapshot.get('lastMessageTime') as Timestamp).toDate())
                      .fromNow(),
            );
          }
          return Container();
        });
  }

  void setupPage() async {}

  @override
  void initState() {
    super.initState();
    setupPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context, title: 'Messages', actions: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12),
          child: ElevatedButton(
              style: ButtonStyle(padding: MaterialStateProperty.all(null)),
              onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => widget.isHost
                          ? SelectUserPage(
                              hostUID: widget.id,
                              hostSnapshot: widget.hostSnapshot!)
                          : SelectHostPage(
                              userID: widget.id, hostUID: widget.hostUID!),
                    ),
                  ),
              child: const Icon(
                CupertinoIcons.paperplane_fill,
                color: Colors.black,
              )),
        )
      ]),
      body: Column(
        children: [
          // Container(
          //   padding: EdgeInsets.only(left: 10.w),
          //   margin: EdgeInsets.symmetric(vertical: 10.h),
          //   decoration: BoxDecoration(
          //       color: Colors.grey[300],
          //       borderRadius: BorderRadius.all(Radius.circular(10.w))),
          //   child: TextField(
          //     controller: searchController,
          //     decoration: const InputDecoration(
          //         icon: Icon(Icons.search),
          //         border: InputBorder.none,
          //         hintText: "Search"),
          //   ),
          // ),
          Expanded(
            child: StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('Rooms')
                  .where('members', arrayContains: widget.id)
                  .orderBy('lastMessageTime', descending: true)
                  .snapshots(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  if (snapshot.data!.docs.isEmpty) {
                    return Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        bottom: 200.h,
                      ),
                      child: Text(
                        'You do not have any messages yet!',
                        style:
                            TextStyle(fontSize: 20.sp, color: Colors.black54),
                        textAlign: TextAlign.center,
                      ),
                    );
                  }
                  listMessage = snapshot.data!.docs;
                  debugPrint("Messages incoming!!");
                  debugPrint(listMessage.toString());
                  return Container(
                    margin: EdgeInsets.only(top: 10.h),
                    alignment: Alignment.topCenter,
                    child: ListView(
                      children: snapshot.data!.docs
                          .where((element) => element.exists)
                          .map((e) => buildItem(e))
                          .toList(),
                    ),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

class RoomWidget extends StatelessWidget {
  const RoomWidget(
      {Key? key,
      required this.id,
      required this.peerName,
      required this.roomId,
      required this.peerId,
      required this.peerPhotoURL,
      required this.lastMessage,
      required this.numUnreadMessages,
      required this.timeString,
      required this.isOnline,
      required this.senderIsHost})
      : super(key: key);

  final String id;
  final String roomId;
  final String peerName;
  final String? peerPhotoURL;
  final String peerId;
  final String lastMessage;
  final String timeString;
  final int numUnreadMessages;
  final bool senderIsHost;
  final bool isOnline;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 20.h, left: 20.w, right: 20.w),
          child: Row(
            children: [
              Container(
                height: 10.w,
                width: 10.w,
                margin: EdgeInsets.only(right: 10.w),
                decoration: BoxDecoration(
                  color: isOnline ? primaryColor : Colors.grey,
                  shape: BoxShape.circle,
                ),
              ),
              Container(
                height: 45.w,
                width: 45.w,
                margin: EdgeInsets.only(right: 10.w),
                decoration: BoxDecoration(
                    color: Colors.grey[400],
                    shape: BoxShape.circle,
                    image: (peerPhotoURL == null || peerPhotoURL == '')
                        ? null
                        : DecorationImage(
                            image: CachedNetworkImageProvider(peerPhotoURL!))),
                child: (peerPhotoURL == null || peerPhotoURL == '')
                    ? const Padding(
                        padding: EdgeInsets.all(2.0),
                        child: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                      )
                    : null,
              ),
              Expanded(
                child: InkWell(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChatPage(
                          title: peerName,
                          peerId: peerId,
                          id: id,
                          roomId: roomId,
                          senderIsHost: senderIsHost),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 5.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                peerName,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.sp),
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.start,
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  timeString,
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 14.sp),
                                ),
                                SizedBox(width: 10.w),
                                Icon(Icons.arrow_forward_ios, size: 18.w)
                              ],
                            )
                          ],
                        ),
                        SizedBox(height: 3.h),
                        Container(
                          margin: EdgeInsets.only(left: 5.w),
                          child: Text(
                            lastMessage,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(color: Colors.black54),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10.h),
        Container(
          height: 1.h,
          color: Colors.grey,
          margin: EdgeInsets.only(bottom: 20.h),
        ),
      ],
    );
  }
}
