import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/utils/components.dart';

import '../../../services/database.dart';
import 'chat.dart';

class SelectHostPage extends StatefulWidget {
  const SelectHostPage({Key? key, required this.userID, required this.hostUID})
      : super(key: key);
  final String userID;
  final String hostUID;

  @override
  State<SelectHostPage> createState() => _SelectHostPageState();
}

class _SelectHostPageState extends State<SelectHostPage> {
  TextEditingController hostName = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return customScaffold(
      appBar: customAppbar(context, title: 'Send Message'),
      child: Column(
        children: [
          SizedBox(height: 70.h),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'You can only send and reply messages from your host',
              style: TextStyle(fontSize: 18.sp, color: Colors.black54),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 24.h),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20.w),
            child: Text(
              'Sending to:',
              style: TextStyle(fontSize: 16.sp, color: Colors.black54),
            ),
          ),
          SizedBox(height: 10.h),
          FutureBuilder(
              future: DatabaseService().getHostData(hostUID: widget.hostUID),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot<Object?>> snapshot) {
                if (!snapshot.hasData) {
                  return Container();
                }
                return InkWell(
                  onTap: () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChatPage(
                        title: snapshot.data!.get('fullName'),
                        peerId: widget.hostUID,
                        id: widget.userID,
                        roomId: widget.hostUID + '&' + widget.userID,
                        senderIsHost: false,
                      ),
                    ),
                  ),
                  child: roundedContainer(
                    buttonColor: Colors.black54,
                    child: Container(
                      padding: EdgeInsets.all(15.w),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        snapshot.data!.get('fullName'),
                        style:
                            TextStyle(fontSize: 18.sp, color: Colors.black54),
                      ),
                    ),
                  ),
                );
              })
        ],
      ),
    );
  }
}
