import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/pages/main/hosts/work_station/broadcast/broadcast.dart';
import 'package:isupport/services/database.dart';
import 'package:isupport/utils/components.dart';

class SelectUserPage extends StatefulWidget {
  const SelectUserPage(
      {Key? key, required this.hostUID, required this.hostSnapshot})
      : super(key: key);
  final String hostUID;
  final DocumentSnapshot hostSnapshot;

  @override
  State<SelectUserPage> createState() => _SelectUserPageState();
}

class _SelectUserPageState extends State<SelectUserPage> {
  String dropdownValue = 'Select User';
  Map<String, String> userNameToIDMap = {};
  final FocusNode focusNode = FocusNode();

  Widget filterDropdownButton() {
    return FutureBuilder(
        future: DatabaseService()
            .getUsersToNameMap(userList: widget.hostSnapshot.get('users')),
        builder: (BuildContext context, snapshot) {
          if (!snapshot.hasData) {
            // List<String> userIDs = await getSpecificHostData('users', hostUID: hostUID);
            return Container();
          }
          userNameToIDMap = (snapshot.data! as Map<String, String>);
          return Center(
            child: roundedContainer(
              buttonColor: Colors.black54,
              radius: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5.h),
                child: FittedBox(
                  child: DropdownButton<String>(
                    isDense: true,
                    // remove underline
                    underline: Container(),

                    // Initial Value
                    value: dropdownValue,

                    // Down Arrow Icon
                    icon: Padding(
                      padding: EdgeInsets.only(right: 10.w),
                      child: const Icon(CupertinoIcons.chevron_down),
                    ),

                    // Array list of items
                    items: [
                      // initial value should be contained in items
                      'Select User',
                      ...userNameToIDMap.keys
                    ].map((String item) {
                      return DropdownMenuItem(
                        value: item,
                        child: Container(
                          padding: EdgeInsets.only(
                              left: 30.w,
                              right: item == dropdownValue ? 70.w : 0),
                          child: FittedBox(
                            child: Text(item,
                                style: TextStyle(
                                    fontSize: 21.sp, color: Colors.black54)),
                          ),
                        ),
                      );
                    }).toList(),
                    // After selecting the desired option,it will
                    // change button value to selected value
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue!;
                      });
                    },
                  ),
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return customScaffold(
      appBar: customAppbar(context, title: 'Send Message'),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30.h),
              Text(
                'You can send and reply messages from your users',
                style: TextStyle(fontSize: 16.sp),
              ),
              SizedBox(height: 30.h),
              Row(
                children: [
                  Text(
                    'To send bulk SMS please use',
                    style: TextStyle(fontSize: 16.sp),
                  ),
                  InkWell(
                    onTap: () => Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            BroadcastPage(snapshot: widget.hostSnapshot),
                      ),
                    ),
                    child: Text(
                      ' broadcast',
                      style: TextStyle(fontSize: 16.sp, color: Colors.blue),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30.h),
              Text(
                'Sending to:',
                style: TextStyle(fontSize: 16.sp),
              ),
              SizedBox(height: 10.h),
              filterDropdownButton()
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Stack(
              children: [
                ChatInputWidget(
                  focusNode: focusNode,
                  id: widget.hostUID,
                  peerId: dropdownValue != 'Select User'
                      ? userNameToIDMap[dropdownValue]!
                      : dropdownValue,
                  senderIsHost: true,
                  roomId: dropdownValue != 'Select User'
                      ? widget.hostUID + '&' + userNameToIDMap[dropdownValue]!
                      : dropdownValue,
                  senderName: widget.hostSnapshot.get('fullName'),
                ),
                if (dropdownValue == 'Select User')
                  Positioned.fill(
                    child: InkWell(
                      child: Container(color: Colors.white30),
                      onTap: () =>
                          customScaffoldMessage('Select a user first', context),
                    ),
                  )
              ],
            ),
          )
        ],
      ),
    );
  }
}
