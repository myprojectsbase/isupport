import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:isupport/splash.dart';
import 'package:isupport/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int? colorCode = prefs.getInt('themeColorCode');
  if (colorCode != null) {
    setPrimaryColor(colorCode: colorCode);
  }
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyDrIhZB_ELD_lZyATe1fMH5Sjkd_MT_kgQ",
      authDomain: "isupport-c2b03.firebaseapp.com",
      projectId: "isupport-c2b03",
      storageBucket: "isupport-c2b03.appspot.com",
      messagingSenderId: "115155782609",
      appId: "1:115155782609:android:27af91111d060ebc552ef0",
      measurementId: "G-2DQ6H34BR5",
    ),
  );
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (value) => runApp(
      const AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          // systemNavigationBarColor: primaryColor,
        ),
        child: MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        minTextAdapt: true,
        designSize: const Size(414, 869),
        builder: (context, child) {
          return MaterialApp(
            // useInheritedMediaQuery: true,
            title: 'iSupport',
            theme: ThemeData(
              // primarySwatch: Colors.green,
              scaffoldBackgroundColor: Colors.white,
            ),
            home: child,
          );
        },
        child: const SplashScreen());
  }
}
