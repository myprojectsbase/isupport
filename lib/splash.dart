import 'dart:async';

import 'package:flutter/material.dart';
import 'package:isupport/pages/intro/intro.dart';
import 'package:isupport/pages/main/hosts/home.dart';
import 'package:isupport/pages/main/users/home.dart';
import 'package:isupport/pages/registry/choose.dart';
import 'package:isupport/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void pageChanger() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userFirestoreId = prefs.getString("uid");
    void handleTimeout() {
      if (userFirestoreId == null) {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (BuildContext context) => const IntroScreen(),
          ),
        );
      } else {
        print('USER ID IS NOT NULL: ' + userFirestoreId);
        String? userType = prefs.getString("userType");
        if (userType == null || userFirestoreId == '') {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => const RegisterOrLogin(),
            ),
          );
        } else {
          if (userType == 'host') {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    HostHomePage(uid: userFirestoreId),
              ),
            );
          } else if (userType == 'user') {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    UserHomePage(uid: userFirestoreId),
              ),
            );
          }
        }
      }
    }

    Timer(const Duration(seconds: 2), handleTimeout);
  }

  @override
  initState() {
    pageChanger();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF0CA404),
      child: Image.asset('assets/images/logo.png'),
    );
  }
}
