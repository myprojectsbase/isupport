import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
// const cors = require('cors')({ origin: true });


admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.paymentConfirmationData = functions.https.onRequest(async (req, res) => {
    if (req.method !== 'POST') {
        res.status(500).json({
            message: 'Not allowed'
        });
    }
    functions.logger.log(req.body);
    // var today = new Date();
    var email = req.body.email;
    // var confirmationKey = req.body.confirmationKey;
    // var amount = req.body.amount;
    // var date = today;
    // var data = { 'email': email, 'confirmationKey': confirmationKey, 'amount': amount, 'date': date };
    // res.send(data);

    await db
        .collection('Hosts')
        .where('email', '==', email)
        .limit(1)
        .get().then(async (documentQuery) => {

            if (documentQuery.docs.length > 0) {

                const documentSnapshot = documentQuery.docs[0];
                await db
                    .collection('Hosts')
                    .doc(documentSnapshot.id)
                    .update({
                        'expires': true,
                        'isActive': true
                    })
                    .then(() => {
                        res.status(200).json({
                            message: 'Success'
                        });
                    }).catch((error) => {
                        res.status(500).json({
                            message: 'Error occured'
                        });
                    });
            } else {
                res.status(500).json({
                    message: 'Error occured'
                });
            }
        }).catch((error) => {
            res.status(500).json({
                message: 'Error occured'
            });
        });

});

// handling notifications for messages sent
exports.messageNotificationFunction = functions.firestore
    .document("Rooms/{roomID}/messages/{messageID}")
    .onCreate(async (snap, context) => {
        let roomID = context.params.roomID;
        let hostUID = context.params.hostUID;
        // let userID = context.auth?.uid;
        let receiverID = snap.data().receiver_id;
        let message = snap.data().text;
        let senderName = snap.data().sender_name;
        let senderID = snap.data().sender_id;
        let messageHasImage = snap.data().msgPhoto != '';
        // if(message.length > 45){
        //     message  = message.substr(0, 45) + "...";
        // }
        if (messageHasImage) {
            if (message == '') {
                message = "Image  📷";
            } else {
                message += "  •  Image  📷";
            }
        }
        let collectionName = 'Hosts';
        // if message sender is host, then notification receiver has to be user
        if (senderID == hostUID) {
            collectionName = 'Users';
        }
        const tokensQuerySnapshot = await db
            .collection(collectionName)
            .doc(receiverID)
            .collection("tokens")
            .get();
        const tokens = tokensQuerySnapshot.docs.map(snap => snap.id);
        const payload = {
            notification: {
                title: senderName,
                body: "New message:  " + message,
                icon: "https://firebasestorage.googleapis.com/v0/b/grabba-31231.appspot.com/o/constants%2Fplaystore.png?alt=media&token=45bd24f1-d088-4385-94d0-dc2cee93ca78",
                clickAction: "FLUTTER_NOTIFICATION_CLICK"
            },
            data: {
                "senderName": senderName,
                "senderID": senderID,
                "roomID": roomID,
            }
        };

        return fcm.sendToDevice(tokens, payload);

    });